package deprecated;
//package core.population;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
//import core.individual.core.IIEMFTextBinary;
//import core.individual.core.IIndividual;
//
//public class PopulationEMFTextBinary implements IPopulation<IIEMFTextBinary>{
//
//	ArrayList<IIEMFTextBinary> individuals;
//	
//	public PopulationEMFTextBinary() {
//		individuals = new ArrayList<IIEMFTextBinary>();
//	}
//	
//	public IIEMFTextBinary get(int index) {
//		return individuals.get(index);
//	}
//
//	public int size() {
//		return individuals.size();
//	}
//	
//	public void sortPopulationDescendant() {
//		
//		Collections.sort(individuals, new Comparator<IIndividual>() {
//
//			@Override
//			public int compare(IIndividual first, IIndividual second) {
//				
//				return -Double.compare(first.getFitness(), second.getFitness());
//			}
//		});
//		
//	}
//	
//	public void removeFromBottom(int amount) {
//		
//		individuals.subList(individuals.size()-amount, individuals.size()).clear();
//		
//	}
//
//	@Override
//	public void addAll(List<IIEMFTextBinary> offspring) {
//		individuals.addAll(offspring);
//		
//	}
//
//	@Override
//	public void setInitialPopulation(List<IIEMFTextBinary> initialPopulation) {
//		
//		individuals.clear();
//		individuals.addAll(initialPopulation);
//		
//	}
//
//	@Override
//	public List<IIEMFTextBinary> getPopulation() {
//		// TODO Auto-generated method stub
//		return individuals;
//	}
//	
//	public String toString(){
//		
//		String s = "Population of "+individuals.size()+" individuals\r\n";
//		
//		for(IIndividual individual: individuals) {
//			s+= individuals.indexOf(individual)+" - "+individual.toString()+"\r\n";
//		}
//		
//		return s;
//	}
//	
//}
