//package usj.svit.approach.flimea.core.config;
//
//import java.util.List;
//
//import usj.svit.approach.flimea.core.fitness.IFitness;
//import usj.svit.approach.flimea.core.operator.crossover.ICrossover;
//import usj.svit.approach.flimea.core.operator.mutation.IMutation;
//import usj.svit.approach.flimea.core.operator.replace.IReplace;
//import usj.svit.approach.flimea.core.operator.selection.ISelection;
//import usj.svit.approach.flimea.core.population.IPopulation;
//import usj.svit.approach.flimea.core.populator.IPopulator;
//import usj.svit.approach.flimea.core.post.IPostProcessor;
//import usj.svit.approach.flimea.core.pre.IPreProcessor;
//import usj.svit.architecture.core.eof.EObjectFragmentable;
//import usj.svit.architecture.core.individual.IIndividualTextEMF;
//
//public abstract class BaseEAConfigTextEMF<T extends IIndividualTextEMF> {
//	
//	public EObjectFragmentable searchSpace;
//	
//	public static final int MAX_POPULATION = 10;
//	public int MAX_GENERATIONS = 10;
//	public float CROSSOVER_PROBABILITY=1;
//	public double MUTATION_PROBABILITY=1;
//	public static final int LSI_DIMENSIONS = 2;
//	
//	public IPopulation<T> population;
//	public IPopulator<T> populator;
//	
//	public ICrossover<T> crossover;
//	public IMutation<T> mutation;
//	public ISelection<T> parentSelection;
//	public IReplace<T> replacement;
//	
//	public IFitness<T> fitness;
//	
//	public abstract boolean stopConditionMeet();
//	public List<IPostProcessor<T>> postprocessors;
//	
//	public List<T> offspring;
//	
//	public int generations=0;
//	public List<IPreProcessor<T>> preprocessors;
//	
//	
//}
