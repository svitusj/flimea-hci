package usj.svit.flimea.api.encoding;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import usj.svit.flimea.api.individual.IndividualEMFTextBinary;
import usj.svit.flimea.core.encoding.IEncoding;
import usj.svit.flimea.core.individual.IIEMFTextBinary;

public class EncodingEMFtoBinary implements IEncoding<EObject,IIEMFTextBinary> {

	EMFToBinaryCODEC codec = null;
	
	public IIEMFTextBinary encode(EObject root) {
		
		if(codec == null)
				codec = new EMFToBinaryCODEC(root);
		
		return new IndividualEMFTextBinary(codec);
	}

	public EObject decode(IIEMFTextBinary individual) {
		
		IndividualEMFTextBinary auxIndividual = new IndividualEMFTextBinary(individual);
		
		EObject root = auxIndividual.getCodec().getRoot();
		TreeIterator<EObject> elements = EcoreUtil.getAllContents(root,false);
		
		ArrayList<EObject> toDelete = new ArrayList<EObject>();
		
		while (elements.hasNext()) {
			EObject object = elements.next();
			
			int index = individual.getCodec().getMapEObjectToID().get(object);
			if(!individual.getGenes().get(index)) {
				toDelete.add(object);
			}
		}
		
		for(EObject obj : toDelete) {
			EcoreUtil.delete(obj);
		}
		
		return auxIndividual.getCodec().getRoot();
	}
	
	


	private class EMFToBinaryCODEC  {
	
		private EObject root;	
		private HashMap<Integer, EObject> mapIDTOEOBject;
		private HashMap<EObject, Integer> mapEObjectToID;
		
		public EMFToBinaryCODEC(EObject root) {
			
			this.root = root;
			mapIDTOEOBject = new HashMap<Integer,EObject>();
			mapEObjectToID = new HashMap<EObject,Integer>();
			
			TreeIterator<EObject> elements = root.eAllContents();
			int index = 0;
			
			while (elements.hasNext()) {
				EObject object = elements.next();
				mapIDTOEOBject.put(index, object);
				
				mapEObjectToID.put(object, index);
				index++;
			}
		}
		
		public int getSize() {
			return mapIDTOEOBject.size();
		}
	
		public EObject getRoot() {
			return root;
		}
		
		public HashMap<Integer, EObject> getMapIDTOEOBject() {
			return mapIDTOEOBject;
		}
	
		public HashMap<EObject, Integer> getMapEObjectToID() {
			return mapEObjectToID;
		}
	}
}



