package usj.svit.approach.flimea.core.ea;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.oracle.TestCaseTextEMF;
import usj.svit.architecture.results.EMFResultCompletePopulation;
import usj.svit.architecture.results.IEMFResult;
import usj.svit.helper.perfomance.log.Logger;

public class FLiMEA extends BaseEATextEMF {

	static int id_counter=0;
	int id;
	
	public FLiMEA(EAConfigTextEMF config) {
		super(config);
		id = id_counter;
		id_counter++;
	}

	@Override
	public IEMFResult<IIndividualEMF> run(TestCaseTextEMF testCase) {
		
		if(Logger.ENABLED_EA) Logger.log(Logger.WHO_EA,getClass().getCanonicalName(),Logger.ACTION_START, Logger.WHAT_EA_TC_RUN+testCase.getID());
		
		this.config.setCurrentTestCase(testCase);
		
		this.preLoop();
		this.loop();
		
		if(Logger.ENABLED_EA) Logger.log(Logger.WHO_EA,getClass().getCanonicalName(),Logger.ACTION_FINISH, Logger.WHAT_EA_TC_RUN+testCase.getID());
		
		return new EMFResultCompletePopulation<IIndividualEMF>(config.getPopulation().getIndividuals());
//		return new SingleEMFResult<IIndividualEMF>(config.getPopulation().get(0));
		
	}

	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return "FLiMEA_"+id;
	}

}
