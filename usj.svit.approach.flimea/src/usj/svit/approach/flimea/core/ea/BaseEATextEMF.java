package usj.svit.approach.flimea.core.ea;

import java.util.List;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.pre.IPreProcessor;
import usj.svit.architecture.approach.IApproachTextEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.Query;
import usj.svit.architecture.results.TCPerformance;
import usj.svit.helper.workspace.helper.ProjectHelper;

public abstract class BaseEATextEMF implements IApproachTextEMF {

	protected EAConfigTextEMF config;

	public void persistWathever(ProjectHelper helper, String baseName) {
		
	}
	
	public BaseEATextEMF(EAConfigTextEMF config) {
		this.config = config;
	}

	/**
	 * copy tweak and optionally assess the individual provided
	 * 
	 * @param original
	 * @param shouldAsses
	 * @return
	 */
	protected IIndividualTextEMF copyAndTweak(IIndividualTextEMF original, boolean shouldAsses) {
		IIndividualTextEMF copy = original.createCopy();
		config.getMutation().forceMutation(copy);
		copy.updateTextsAfterRecombinations();
		if (shouldAsses)
			assess(copy);
		return copy;
	}

	protected IIndividualTextEMF perturb(IIndividualTextEMF home) {

		IIndividualTextEMF randomParent = config.getPopulator().generateIndividual(config);

		List<IIndividualTextEMF> offspring = config.getCrossover().crossover(home, randomParent);

		IIndividualTextEMF perturb = offspring.get(0);

		perturb.updateTextsAfterRecombinations();
		assess(perturb);

		return perturb;
	}

	/**
	 * generates a random individual and optionally assess it
	 * 
	 * @param shouldAssess
	 * @return
	 */
	protected IIndividualTextEMF generateRandomFragment(boolean shouldAssess) {
		IIndividualTextEMF random = config.getPopulator().generateIndividual(config);

		if (shouldAssess)
			assess(random);

		return random;
	}

	/**
	 * Assess the individual provided, only possible if the Fitness allows to do
	 * it
	 * 
	 * @param individual
	 */
	protected void assess(IIndividualTextEMF individual) {
		config.getFitness().assess(individual, config);
	}

	/**
	 * Generate initial population and execute preprocesors
	 */
	public void preLoop() {
		config.getPopulation().setPopulation(config.getPopulator().generateInitialPopulation(config));

		for (IPreProcessor<IIndividualTextEMF> preprocessor : config.getPreprocessors()) {
			preprocessor.process(config);
		}
	}

	public void loop() {

		while (true) {

			for (IIndividualTextEMF element : config.getPopulation().getIndividuals()) {
				element.updateTextsAfterRecombinations();
			}

			config.getFitness().assess(config);
			config.getPopulation().sortPopulationDescendant();

			if (config.stopConditionMeet()) {

				return;
			}

			config.getSelection().selectParents(config);
			config.getCrossover().crossover(config);
			config.getMutation().mutate(config);
			config.getReplace().replace(config);

			config.increaseCurrentGeneration();
		}
		// loop();
	}

	public TCPerformance getTCPerformance(){
		
		TCPerformance performance = new TCPerformance();
		
		performance.crossoverAffected = config.getCrossover().getAndResetAffected();
		performance.crossoverCalls = config.getCrossover().getAndResetCalls();
		performance.fitnessAffected = config.getFitness().getAndResetAffected();
		performance.fitnessCalls = config.getFitness().getAndResetCalls();
		performance.mutationAffected = config.getMutation().getAndResetAffected();
		performance.mutationCalls = config.getMutation().getAndResetCalls();
		performance.populationAffected = config.getPopulation().getAndResetAffected();
		performance.populationCalls = config.getPopulation().getAndResetCalls();
		performance.populatorAffected = config.getPopulator().getAndResetAffected();
		performance.populatorCalls = config.getPopulator().getAndResetCalls();
		performance.replaceAffected = config.getReplace().getAndResetAffected();
		performance.replaceCalls = config.getReplace().getAndResetCalls();
		performance.selectionAffected = config.getSelection().getAndResetAffected();
		performance.selectionCalls = config.getSelection().getAndResetCalls();
		
		return performance;	
	}
	
	public String getDescription() {
		StringBuilder sb = new StringBuilder();

		sb.append("Type;Name;Calls;Affected+\r\n");

		sb.append("Populator:;" + config.getPopulator().getDescription());
		sb.append("Fitness:;" + config.getFitness().getDescription());
		sb.append("Replace:;" + config.getReplace().getDescription());
		sb.append("Selection:;" + config.getSelection().getDescription());
		sb.append("Mutation:;" + config.getMutation().getDescription());
		sb.append("Crossover:;" + config.getCrossover().getDescription());
		sb.append("Preprocessors:;");
		for (IPreProcessor<IIndividualTextEMF> pre : config.getPreprocessors()) {
			sb.append(pre.getClass().getSimpleName() + ";");
		}
		sb.append("\r\n");

		sb.append("Generations:;" + this.config.MAX_GENERATIONS + "\r\n");

		return sb.toString();
	}

	@Override
	public Query getLastQuery() {
		// TODO Auto-generated method stub
		return config.getQuery();
	}

	@Override
	public int getLastGenerations() {
		// TODO Auto-generated method stub
		//System.out.println("Last gen is: " + config.getCurrentGeneration());
		return config.getCurrentGeneration();
	}
}
