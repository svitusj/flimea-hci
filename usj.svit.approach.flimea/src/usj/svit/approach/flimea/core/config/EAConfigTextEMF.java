package usj.svit.approach.flimea.core.config;

import java.util.ArrayList;
import java.util.List;

import usj.svit.approach.flimea.core.fitness.IFitness;
import usj.svit.approach.flimea.core.operator.crossover.ICrossover;
import usj.svit.approach.flimea.core.operator.mutation.IMutation;
import usj.svit.approach.flimea.core.operator.replace.IReplace;
import usj.svit.approach.flimea.core.operator.selection.ISelection;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.approach.flimea.core.populator.IPopulator;
import usj.svit.approach.flimea.core.post.IPostProcessor;
import usj.svit.approach.flimea.core.pre.IPreProcessor;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.IndividualEMF;
import usj.svit.architecture.individual.impl.Query;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;
import usj.svit.architecture.oracle.TestCaseTextEMF;

public class EAConfigTextEMF implements IEAConfigTextEMF<IIndividualTextEMF> {

	private IPopulation<IIndividualTextEMF> population;
	private IPopulator<IIndividualTextEMF> populator;
	private ICrossover<IIndividualTextEMF> crossover;
	private IMutation<IIndividualTextEMF> mutation;
	private ISelection<IIndividualTextEMF> parentSelection;
	private IReplace<IIndividualTextEMF> replacement;
	private IFitness<IIndividualTextEMF> fitness;
	private List<IPreProcessor<IIndividualTextEMF>> preprocessors;
	private List<IPostProcessor<IIndividualTextEMF>> postprocessors;
	
	private List<IIndividualTextEMF> offspring;
	private EObjectFragmentable searchSpace;
	
	public int MAX_POPULATION = 100;
	public int MAX_GENERATIONS = 3000;
	public float CROSSOVER_PROBABILITY=0.75f;
	public double MUTATION_PROBABILITY=1;
	public int LSI_DIMENSIONS = 2;
	
	private int currentGenegeneration;
	private Query query;
	
	private TestCaseTextEMF currentTestCase;
	public long MAX_TIME;
	
	private long initialTime;

	public EAConfigTextEMF(IPopulation<IIndividualTextEMF> population, IPopulator<IIndividualTextEMF> populator, ICrossover<IIndividualTextEMF> crossover,
			IMutation<IIndividualTextEMF> mutation, ISelection<IIndividualTextEMF> parentSelection, IReplace<IIndividualTextEMF> replacement, IFitness<IIndividualTextEMF> fitness,
			List<IPreProcessor<IIndividualTextEMF>> preprocessors,List<IPostProcessor<IIndividualTextEMF>> postprocessors) {
		super();
		
		this.population = population;
		this.populator = populator;
		this.crossover = crossover;
		this.mutation = mutation;
		this.parentSelection = parentSelection;
		this.replacement = replacement;
		this.fitness = fitness;
		this.preprocessors = preprocessors;
		this.postprocessors = postprocessors;
		
		MAX_GENERATIONS = 100;
	}

	public void resetInitialTime(){
		initialTime = System.currentTimeMillis();
	}
	
	public long getElapsedTime() {
		return System.currentTimeMillis() - initialTime;
	}
	
	@Override
	public int getCurrentGeneration(){
		return this.currentGenegeneration;
	}
	
	@Override
	public boolean stopConditionMeet() {
		return currentGenegeneration>=MAX_GENERATIONS;
	}

	@Override
	public IPopulator<IIndividualTextEMF> getPopulator() {
		return populator;
	}

	@Override
	public IFitness<IIndividualTextEMF> getFitness() {
		return fitness;
	}

	@Override
	public IReplace<IIndividualTextEMF> getReplace() {
		return replacement;
	}

	@Override
	public ISelection<IIndividualTextEMF> getSelection() {
		return parentSelection;
	}

	@Override
	public IMutation<IIndividualTextEMF> getMutation() {
		return mutation;
	}

	@Override
	public ICrossover<IIndividualTextEMF> getCrossover() {
		return crossover;
	}

	@Override
	public List<IPreProcessor<IIndividualTextEMF>> getPreprocessors() {
		return preprocessors;
	}

	@Override
	public List<IPostProcessor<IIndividualTextEMF>> getPostprocessors() {
		return postprocessors;
	}

	@Override
	public IPopulation<IIndividualTextEMF> getPopulation() {
		return population;
	}

	private void reset() {
		currentGenegeneration = 0;
	}

	@Override
	public void increaseCurrentGeneration() {
		currentGenegeneration++;
	}

	@Override
	public Query getQuery() {
		return query ;
	}

	public double getCrossoverProbability() {
		return CROSSOVER_PROBABILITY;
	}

	public List<IIndividualTextEMF> getOffSpring() {
		return offspring;
	}

	@Override
	public void setOffSpring(ArrayList<IIndividualTextEMF> selection) {
		this.offspring = selection;
		
	}

	@Override
	public EObjectFragmentable getSearchSpace() {
		return searchSpace;
	}
	/**
	 * Sets the current Query to be "found"
	 * @param query
	 */
	private void setQuery(Query query) {
		this.query = query;
		
	}
	
	/**
	 * Sets the current SearchSpace to be explored
	 * @param searchSpace
	 */
	private void setSearchSpace(EObjectFragmentable searchSpace) {
		this.searchSpace = searchSpace;
	}

	public int getLSIDimension() {
		return LSI_DIMENSIONS;
	}
	
	public void setCurrentTestCase(TestCaseTextEMF tc){
		this.resetInitialTime();
		this.currentTestCase = tc;
		this.setQuery(new Query(tc.getQuestion()));
		this.setSearchSpace(tc.getSearchSpace());
		this.reset();
	}
	 public String getTestCaseId(){
		return this.currentTestCase.getID();
	 }
	
	public IndividualEMF getCurrentTestCaseAnswer(){
		return this.currentTestCase.getAnswer();
	}
}
