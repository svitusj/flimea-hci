package usj.svit.approach.flimea.core.config;

import java.util.ArrayList;
import java.util.List;

import usj.svit.approach.flimea.core.fitness.IFitness;
import usj.svit.approach.flimea.core.operator.crossover.ICrossover;
import usj.svit.approach.flimea.core.operator.mutation.IMutation;
import usj.svit.approach.flimea.core.operator.replace.IReplace;
import usj.svit.approach.flimea.core.operator.selection.ISelection;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.approach.flimea.core.populator.IPopulator;
import usj.svit.approach.flimea.core.post.IPostProcessor;
import usj.svit.approach.flimea.core.pre.IPreProcessor;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.Query;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;

public interface IEAConfigTextEMF<T extends IIndividualTextEMF> {

	IPopulator<T> getPopulator();

	IFitness<T> getFitness();

	IReplace<T> getReplace();

	ISelection<T> getSelection();

	IMutation<T> getMutation();

	ICrossover<T> getCrossover();

	List<IPreProcessor<T>> getPreprocessors();

	List<IPostProcessor<T>> getPostprocessors();
	
	IPopulation<T> getPopulation();

	boolean stopConditionMeet();

	int getCurrentGeneration();

	void increaseCurrentGeneration();

	Query getQuery();

	void setOffSpring(ArrayList<IIndividualTextEMF> selection);

	EObjectFragmentable getSearchSpace();


}
