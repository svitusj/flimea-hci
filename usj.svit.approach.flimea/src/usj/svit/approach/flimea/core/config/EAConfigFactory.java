package usj.svit.approach.flimea.core.config;

import java.util.ArrayList;

import usj.svit.approach.flimea.core.fitness.FitnessLSI;
import usj.svit.approach.flimea.core.fitness.FitnessPerfect;
import usj.svit.approach.flimea.core.fitness.FitnessRandom;
import usj.svit.approach.flimea.core.fitness.IFitness;
import usj.svit.approach.flimea.core.operator.crossover.CrossoverMask;
import usj.svit.approach.flimea.core.operator.mutation.MutationRandomEachGen;
import usj.svit.approach.flimea.core.operator.mutation.MutationRandomGen;
import usj.svit.approach.flimea.core.operator.replace.ReplaceElitism;
import usj.svit.approach.flimea.core.operator.selection.SelectionRouletteWheel;
import usj.svit.approach.flimea.core.population.Population;
import usj.svit.approach.flimea.core.populator.PopulatorMixed;
import usj.svit.approach.flimea.core.populator.PopulatorRandom;
import usj.svit.approach.flimea.core.populator.PopulatorRandomSizeDistributed;
import usj.svit.approach.flimea.core.pre.IPreProcessor;
import usj.svit.approach.flimea.core.pre.PreprocessorAllowOnlyNounsFilter;
import usj.svit.approach.flimea.core.pre.PreprocessorAllowOnlyNumbersFilter;
import usj.svit.approach.flimea.core.pre.PreprocessorEnglishStemmer;
import usj.svit.approach.flimea.core.pre.PreprocessorLowerCase;
import usj.svit.approach.flimea.core.pre.PreprocessorPosTagger;
import usj.svit.approach.flimea.core.pre.PreprocessorUnderscoreTokenizer;
import usj.svit.approach.flimea.core.pre.PreprocessorWhiteSpaceTokenizer;
import usj.svit.architecture.individual.IIndividualTextEMF;

public class EAConfigFactory {

	public static final int PRE_WT = 1;
	public static final int PRE_LC = 2;
	public static final int PRE_PT = 3;
	public static final int PRE_NO = 4;
	public static final int PRE_ES = 5;
	public static final int PRE_UT = 6;

	public static final int FIT_LSI = 7;
	public static final int FIT_RNG = 8;
	public static final int FIT_PER = 9;
	public static final int STOP_GEN = 10;

	public static EAConfigTextEMF getConfig(int... options) {

		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		IFitness<IIndividualTextEMF> fitness = null;

		boolean stopGenereations = false;

		for (int i : options) {
			switch (i) {
			case PRE_WT:
				preprocessors.add(new PreprocessorWhiteSpaceTokenizer<IIndividualTextEMF>());
				break;
			case PRE_LC:
				preprocessors.add(new PreprocessorLowerCase<IIndividualTextEMF>());
				break;
			case PRE_PT:
				preprocessors.add(PreprocessorPosTagger.getInstance(PreprocessorPosTagger.en));
				break;
			case PRE_NO:
				preprocessors.add(new PreprocessorAllowOnlyNounsFilter<IIndividualTextEMF>());
				break;
			case PRE_ES:
				preprocessors.add(PreprocessorEnglishStemmer.getInstance());
				break;
			case PRE_UT:
				preprocessors.add(new PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());
				break;
			case FIT_LSI:
				fitness = new FitnessLSI();
				break;
			case FIT_RNG:
				fitness = new FitnessRandom();
				break;
			case FIT_PER:
				fitness = new FitnessPerfect();
				break;
			case STOP_GEN:
				stopGenereations = true;
				break;
			}
		}

		if (fitness == null)
			fitness = new FitnessLSI();

		EAConfigTextEMF config;
		
		if (stopGenereations) {
			config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
					new PopulatorMixed<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
					new SelectionRouletteWheel(), new ReplaceElitism(), fitness, preprocessors, new ArrayList()){
				public boolean stopConditionMeet() {
					if(getCurrentGeneration()>=MAX_GENERATIONS)
						return true;
					if(getPopulation().getIndividuals().get(0).getFitness() == 1.0){
						//System.out.println("Found the perfect one in Gen "+getCurrentGeneration());
						return true;
					}
					return false;
				};
			};
		} else {
			config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
					new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
					new SelectionRouletteWheel(), new ReplaceElitism(), fitness, preprocessors, new ArrayList());
		}
		return config;

	}

	public static EAConfigTextEMF getConfigForRandomFragments() {

		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		preprocessors.add(new PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorWhiteSpaceTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorLowerCase<IIndividualTextEMF>());
		preprocessors.add(PreprocessorPosTagger.getInstance(PreprocessorPosTagger.en));
		// preprocessors.add(new
		// PreprocessorAllowOnlyNounsFilter<IIndividualTextEMF>());
		preprocessors.add(PreprocessorEnglishStemmer.getInstance());

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandomSizeDistributed<IIndividualTextEMF>(), new CrossoverMask(),
				new MutationRandomEachGen(), new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSI(),
				preprocessors, new ArrayList());

		config.LSI_DIMENSIONS = 2;
		config.MAX_GENERATIONS = 1;
		config.MAX_POPULATION = 200;
		config.MUTATION_PROBABILITY = 10;

		return config;
	}

	public static EAConfigTextEMF getC1Config() {

		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		preprocessors.add(new PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorWhiteSpaceTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorLowerCase<IIndividualTextEMF>());
		preprocessors.add(PreprocessorPosTagger.getInstance(PreprocessorPosTagger.en));
		// preprocessors.add(new
		// PreprocessorAllowOnlyNounsFilter<IIndividualTextEMF>());
		preprocessors.add(PreprocessorEnglishStemmer.getInstance());

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomEachGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSI(), preprocessors, new ArrayList());

		config.LSI_DIMENSIONS = 2;
		config.MAX_GENERATIONS = 1000;
		config.MAX_POPULATION = 200;
		config.MUTATION_PROBABILITY = 10;

		return config;
	}

	public static EAConfigTextEMF getC2Config() {

		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		preprocessors.add(new PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorLowerCase<IIndividualTextEMF>());
		preprocessors.add(PreprocessorPosTagger.getInstance(PreprocessorPosTagger.en));
		preprocessors.add(new PreprocessorAllowOnlyNounsFilter<IIndividualTextEMF>());
		preprocessors.add(PreprocessorEnglishStemmer.getInstance());

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSI(), preprocessors, new ArrayList());

		return config;
	}

	public static EAConfigTextEMF getC3Config() {

		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSI(), preprocessors, new ArrayList());

		return config;
	}

	public static EAConfigTextEMF getC4Config() {

		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		preprocessors.add(new PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSI(), preprocessors, new ArrayList());

		return config;
	}

	public static EAConfigTextEMF getC5Config() {

		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		preprocessors.add(new PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorLowerCase<IIndividualTextEMF>());

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSI(), preprocessors, new ArrayList());

		return config;
	}

	public static EAConfigTextEMF getC6Config() {

		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		preprocessors.add(new PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorWhiteSpaceTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorLowerCase<IIndividualTextEMF>());

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSI(), preprocessors, new ArrayList());

		return config;
	}

	public static EAConfigTextEMF getRandomFitnessConfig() {
		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessRandom(), preprocessors,
				new ArrayList());

		return config;
	}

	public static EAConfigTextEMF getOnlyNumbersConfig() {
		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		preprocessors.add(new PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorWhiteSpaceTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorLowerCase<IIndividualTextEMF>());
		preprocessors.add(PreprocessorPosTagger.getInstance(PreprocessorPosTagger.en));
		preprocessors.add(new PreprocessorAllowOnlyNumbersFilter<IIndividualTextEMF>());

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSI(), preprocessors, new ArrayList());

		config.LSI_DIMENSIONS = 5;

		return config;
	}
}
