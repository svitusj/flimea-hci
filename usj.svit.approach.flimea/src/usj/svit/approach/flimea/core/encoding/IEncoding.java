package usj.svit.approach.flimea.core.encoding;

public interface IEncoding<T,U> {
	
	public U encode();
	
	public T decode(U individual);
	
}
