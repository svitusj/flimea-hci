package usj.svit.approach.flimea.core.post;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;

public interface IPostProcessor<T extends IIndividualTextEMF> {

	public void process(EAConfigTextEMF context);
	
}
