package usj.svit.approach.flimea.core.post;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;

public class PostProcessLogger<T extends IIndividualTextEMF> implements IPostProcessor<T> {

	@Override
	public void process(EAConfigTextEMF context) {
		
		for(IIndividualTextEMF individual : context.getPopulation().getIndividuals()) {	
		}
		
	}

}
