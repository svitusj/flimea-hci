package usj.svit.approach.flimea.core.population;

import java.util.List;

import usj.svit.approach.flimea.core.operator.ICountableOperation;
import usj.svit.architecture.individual.IIndividual;

public interface IPopulation<T extends IIndividual> extends ICountableOperation {


	public void removeFromBottom(int amount);
	public void sortPopulationDescendant();
	public T getBestIndividual();
	public void addAll(List<T> offspring);
	public void setPopulation(List<T> population);
	public void setPopulation(T element);
	public int size();
	public T get(int i);
	public List<T> getIndividuals();
	
	public String toString();
	
	/**
	 * return the number of calls to the sortPopulation operation since the last reset and resets the value to 0.
	 * each sort operation may trigger the sort of 0 to POPULATION_SIZE individuals
	 * @return
	 */
	public long getAndResetCalls();
	
	/**
	 * return the number of elements sorted trough the sort operation
	 */
	public long getAndResetAffected();
	
}
