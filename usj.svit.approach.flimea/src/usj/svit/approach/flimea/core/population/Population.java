package usj.svit.approach.flimea.core.population;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.architecture.individual.IIndividual;

public class Population<T extends IIndividual> extends BaseOperation implements IPopulation<T>{

	ArrayList<T> individuals;
	
	public Population() {
		individuals = new ArrayList<T>();
	}
	
	public T get(int index) {
		return individuals.get(index);
	}

	public int size() {
		return individuals.size();
	}
	
	public void sortPopulationDescendant() {
		
		countCalls +=1;
		countAffected += individuals.size();
		
		Collections.sort(individuals, new Comparator<T>() {

			@Override
			public int compare(T first, T second) {
				
				return -Double.compare(first.getFitness(), second.getFitness());
			}
		});
	}
	
	public void removeFromBottom(int amount) {
		
		individuals.subList(individuals.size()-amount, individuals.size()).clear();
		
	}
	
	public String toString(){
		
		String s = "Population of "+individuals.size()+" individuals\r\n";
		
		for(IIndividual individual: individuals) {
			s+= individuals.indexOf(individual)+" - "+individual.getFitness()+"\r\n";
		}
		
		return s;
	}

	@Override
	public List<T> getIndividuals() {
		// TODO Auto-generated method stub
		return individuals;
	}

	@Override
	public void addAll(List<T> offspring) {
		individuals.addAll(offspring);
		
	}

	@Override
	public void setPopulation(List<T> initialPopulation) {
		individuals.clear();
		individuals.addAll(initialPopulation);
		
	}
	
	@Override
	public void setPopulation(T element) {
		individuals.clear();
		individuals.add(element);
		
	}

	@Override
	public T getBestIndividual() {
		return Collections.max(individuals, new Comparator<T>() {

			@Override
			public int compare(T first, T second) {
				
				return Double.compare(first.getFitness(), second.getFitness());
			}
		});
	}
}
