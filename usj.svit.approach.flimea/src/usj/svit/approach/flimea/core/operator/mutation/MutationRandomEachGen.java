package usj.svit.approach.flimea.core.operator.mutation;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.utils.MathUtils;

public class MutationRandomEachGen extends BaseOperation implements IMutation<IIndividualTextEMF> {
	
	@Override
	public void mutate(EAConfigTextEMF context) {
		
		for(IIndividualTextEMF individual : context.getOffSpring()) {
			mutate(individual,context);
		}
		countCalls++;
	}
	/**
	 * randomly mutates a bit of the individual
	 */
	@Override
	public void forceMutation(IIndividualTextEMF individual) {
		int index = MathUtils.getRandomInt(individual.getParentModelSize());
		individual.flipGen(index);
		countCalls++;
		countAffected++;
	}

	/**
	 * same probability for each model element of being added/removed
	 * 
	 * @param BinaryIndividual
	 * @return Individual
	 */
	private void mutate(IIndividualTextEMF individual,EAConfigTextEMF context) {
		for(int i=0;i<individual.getParentModelSize();i++)
			if(Math.random() <= context.MUTATION_PROBABILITY/individual.getParentModelSize())
				individual.flipGen(i);
			countAffected++;
	}
}
