package usj.svit.approach.flimea.core.operator.replace;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;

public class ReplaceElitism extends BaseOperation implements IReplace<IIndividualTextEMF> {

	
	long countCalls = 0;
	long countAffected = 0;
	
	@Override
	public IPopulation<IIndividualTextEMF> replace(EAConfigTextEMF context) {
		
		context.getPopulation().removeFromBottom(context.getOffSpring().size());
		context.getPopulation().addAll(context.getOffSpring());

		countAffected += context.getOffSpring().size();
		countCalls += 1;
		
		return context.getPopulation();
	}

	@Override
	public long getAndResetCalls() {
		long aux = countCalls;
		countCalls=0;
		return aux;
	}
	
	@Override
	public long getAndResetAffected() {
		long aux = countAffected;
		countAffected=0;
		return aux;
	}

}
