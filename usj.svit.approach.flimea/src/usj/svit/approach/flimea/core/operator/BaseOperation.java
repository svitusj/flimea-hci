package usj.svit.approach.flimea.core.operator;

public class BaseOperation implements ICountableOperation {

	protected long countCalls;
	protected long countAffected;

	@Override
	public long getAndResetCalls() {
		long aux = countCalls;
		countCalls=0;
		return aux;
	}
	
	@Override
	public long getAndResetAffected() {
		long aux = countAffected;
		countAffected=0;
		return aux;
	}
	
	public String getDescription(){
		StringBuilder sb = new StringBuilder(getClass().getSimpleName()+";"+getAndResetCalls()+";"+getAndResetAffected()+"\r\n");
		return sb.toString();
	}
}
