package usj.svit.approach.flimea.core.operator.selection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.utils.MathUtils;

public class SelectionRouletteWheel extends BaseOperation implements ISelection<IIndividualTextEMF> {
	
	@Override
	public List<IIndividualTextEMF> selectParents(EAConfigTextEMF context) {
		
		int size=2;
		
		double[] cumulativeFitnesses = new double[context.getPopulation().size()];
	     cumulativeFitnesses[0] = context.getPopulation().get(0).getFitness()+1;
		
	    for (int i = 1; i < context.getPopulation().size(); i++)
        {
            double fitness = context.getPopulation().get(i).getFitness()+1;
            cumulativeFitnesses[i] = cumulativeFitnesses[i - 1] + fitness;
        }
	     
	    ArrayList<IIndividualTextEMF> selection = new ArrayList<IIndividualTextEMF>(size);
        for (int i = 0; i < size; i++)
        {
            double randomFitness = MathUtils.getRandomDouble() * cumulativeFitnesses[cumulativeFitnesses.length - 1];
            int index = Arrays.binarySearch(cumulativeFitnesses, randomFitness);
            if (index < 0)
            {
                index = Math.abs(index + 1);
            }
            
            if(index>=context.getPopulation().size())
            	index--;
            selection.add(context.getPopulation().get(index));
        }
        countAffected += 2;
        countCalls +=1;
        context.setOffSpring(selection);
        return selection;
	}
	
}
