package usj.svit.approach.flimea.core.operator.mutation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.Chromosome;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;
import usj.svit.architecture.utils.MathUtils;

public class ReformulationExpansion extends Reformulation implements IMutation<IIndividualTextEMF> {
	//modification of MutationRandomEachGen
	
	private ArrayList<String> queryKeywords;
	private ArrayList<IIndividualTextEMF> corpusIndividuals= new ArrayList<IIndividualTextEMF>();
	private ArrayList<HashMap<String, Integer>> relevantDocs=new ArrayList<HashMap<String, Integer>>();
	private ArrayList<HashMap<String, Integer>> corpusDocs=new ArrayList<HashMap<String, Integer>>();
	private List<IIndividualTextEMF> relevantIndividuals=new ArrayList<IIndividualTextEMF>();
	private int termsToExpand=10;
	ArrayList<String> includedTerms=new ArrayList<String>();
	PrintWriter writer;
	
	
	@Override
	public void mutate(EAConfigTextEMF context) {
		
		IPopulation<IIndividualTextEMF> population=context.getPopulation();
		List<IIndividualTextEMF> individualsList=population.getIndividuals();
		
		
		
		int i=0;
		
		/* form the individualsOrderedByFitness the relevant and corpus individuals are initialized
		 * top N are relevant. All individuals are added to the corpus
		 */
		
		
		
		/*try {
			writer = new PrintWriter("/Users/paqui/Desktop/termsCopus-all.txt", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		//relevantDocs.add(getTermsAndFrequenciesFromQuery(context.getQuery().getText(), result));
		
		for(IIndividualTextEMF individual:population.getIndividuals())
		{
			HashMap<String,Integer> result=new HashMap<String,Integer>();
			result=individual.getFilteredTextsAndFrequencies();
			
			if(i<numberRelevantIndividuals)
			{
			//relevantIndividuals.add(individual);
			relevantDocs.add(result);
			i++;
			}

			corpusDocs.add(result);
			
			
			
			
			//individual.getText();
		}
		//writer.close();
		
		//removeDuplicateTermsInFile("/Users/paqui/Desktop/termsCopus-all.txt");
		
		//copying the offspring to do not modify the selected parents
		List<IIndividualTextEMF> offspring=context.getOffSpring();
		
		List<IIndividualTextEMF> mutatedOffspring = new ArrayList<IIndividualTextEMF>();
		Chromosome parent1 = offspring.get(0).getCopyOfGenes();
		Chromosome parent2 = offspring.get(1).getCopyOfGenes();
		IndividualTextEMF indiv1=new IndividualTextEMF(parent1,offspring.get(0).getParent());
		indiv1.setText(offspring.get(0).getText());
		
		IndividualTextEMF indiv2=new IndividualTextEMF(parent2,offspring.get(1).getParent());
		indiv2.setText(offspring.get(1).getText());
		
		mutatedOffspring.add(indiv1);
		mutatedOffspring.add(indiv2);
		
		context.getOffSpring().clear();
			
		for(IIndividualTextEMF individual : mutatedOffspring) {
			//mutate(individual,context);
			queryKeywords= getFilteredText(individual,individual.getParent().getSize());
			ArrayList<String> termsToExpandMF=runExpansionRocchio(queryKeywords);
			
			/* terms to expand the MF are in the parent but not in the individual. 
			 * The position in the parent will be found to flip the gen.
			 * The first element that includes the term will be added in the MF.
			 */
			EObjectFragmentable parent=individual.getParent();
			//String text;
			String [] text;
			int parentSize=parent.getSize();
			int pos=0;
			while(pos<parentSize && !termsToExpandMF.isEmpty())
			{
				if(!individual.getGen(pos))
				{ 
					//since this is an expansion, only gens set to 0 are checked to flip them.
					//text=parent.getTextForPosition(pos);  this obtains the text without filtering
					text=parent.getFilteredTextForPosition(pos);
					for(int t=0;t<text.length;t++)
					{
						if(termsToExpandMF.contains(text[t])) 
						{
						  //individual.flipGen(pos);
						   individual.getGenes().set(pos, true);
						   System.out.println("Flipped gen "+pos+" due to the term: "+text[t]);
						   termsToExpandMF.remove(text[t]);
						   countAffected++;
						   break;
						}
					}
				}
				pos++;
			}
				
			
		}
		countCalls++;
		
		//the offspring is updated with the mutated individuals
		context.getOffSpring().addAll(mutatedOffspring);
	}
	/**
	 * randomly mutates a bit of the individual
	 */
	@Override
	public void forceMutation(IIndividualTextEMF individual) {
		int index = MathUtils.getRandomInt(individual.getParentModelSize());
		individual.flipGen(index);
		countCalls++;
		countAffected++;
	}

	/*private HashMap<String,Integer> getTermsAndFrequenciesFromIndividual(IIndividualTextEMF individual, HashMap<String,Integer> result)
	{
		//String [] text=individual.getText();
		//to avoid getting the meta-model text
		int parentSize=individual.getParentModelSize();
		ArrayList<String> individualText=getFilteredText(individual,parentSize);
		
		
		
		
		for(String term:individualText)
		{
			if(result.containsKey(term)) result.replace(term, result.get(term)+1);
			else 
			{
				result.put(term,1);
				//writer.println(term);
			}
			
		
		}
		return result;
	}*/
	
	/**
	 * same probability for each model element of being added/removed
	 * 
	 * @param BinaryIndividual
	 * @return Individual
	 */
	
	private ArrayList<String> runExpansionRocchio(ArrayList<String>queryKeywords)
	{
		ArrayList<String> result=new ArrayList<String>();
		
		ArrayList<HashMap<String, Double>> resultTfxIdfValuesInDocs=getTfxIdfValues(relevantDocs, corpusDocs);
		
		HashMap <String, Double> resultWeights=new HashMap<String, Double>();
	
		for (HashMap<String,Double> doc: resultTfxIdfValuesInDocs)
		{
						
			for (HashMap.Entry<String, Double> entry:doc.entrySet())
			{
				String word = entry.getKey();
			    double tfxIdfValue = entry.getValue();
			    
			    if(resultWeights.containsKey(word)) resultWeights.replace(word, resultWeights.get(word)+tfxIdfValue);
			    else resultWeights.put(word, (double) tfxIdfValue);
			}
		}
		
	
		Map<String, Double> sortedTermWeights=sortByComparator(resultWeights, false);
		
		
		result = expandQuery(termsToExpand, queryKeywords, sortedTermWeights);
		
		return result;
		
		
	}
	
	private ArrayList<String> expandQuery(int termsToExpand, ArrayList<String> queryKeywords, Map<String, Double> sortedTermWeights) {
		//it returns the terms to expand the MF
		
		ArrayList<String> result=new ArrayList<String>();
		for (Entry<String, Double> entry : sortedTermWeights.entrySet())
		{
			
			String word=entry.getKey();
			if(termsToExpand==0) break;
			else if(!queryKeywords.contains(word) && entry.getValue()>0) {
			//else if(entry.getValue()>0) {
				result.add(word);
				termsToExpand--;
			}
			//value>0 to ensure that an expanded term has some weight
			
		}
		return result;
	}
	
	private int getNumDocsHasAWord(ArrayList<HashMap<String, Integer>> documents, String word)
	{
		int result=0;
		//number of times that a term appears in a document
		for (HashMap<String,Integer> doc: documents)
		{
			if(doc.containsKey(word)) result++;
		}
	
		return result;
	}
	
	
	private ArrayList<HashMap<String, Double>> getTfxIdfValues(ArrayList<HashMap<String, Integer>> relevantDocs, ArrayList<HashMap<String, Integer>> corpusDocs)
	{
		ArrayList<HashMap<String, Double>> resultTfxIdfValuesInDocs=new ArrayList<HashMap<String, Double>>();
		for (HashMap<String,Integer> doc: relevantDocs)
		{
			HashMap<String,Double> resultDoc= new HashMap<String,Double> ();
			
			for (HashMap.Entry<String, Integer> entry:doc.entrySet())
			{
				String word = entry.getKey();
			    int frequencyInDoc = entry.getValue();
			    int numCorpusDocsHasTheWord=getNumDocsHasAWord(corpusDocs, word);
			    double idf=1.0/numCorpusDocsHasTheWord;
			    double value=frequencyInDoc*idf;
			    
			    resultDoc.put(word, value);
			}
			resultTfxIdfValuesInDocs.add(resultDoc);
		
		}
		return resultTfxIdfValuesInDocs;
	}
	
	
	private void removeDuplicateTermsInFile(String fileRute)
	{
		List<String> terms=new ArrayList<String>();
		File file = new File(fileRute); 
		  
		  BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));
			String st; 
			  while ((st = br.readLine()) != null) {
			    if(!terms.contains(st)) terms.add(st);
			  } 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		try {
			writer = new PrintWriter(fileRute, "UTF-8");
			for(String term:terms)
			{
			  writer.println(term);
			}
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}