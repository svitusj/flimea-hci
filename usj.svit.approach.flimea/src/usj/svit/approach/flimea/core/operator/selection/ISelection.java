package usj.svit.approach.flimea.core.operator.selection;

import java.util.List;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.ICountableOperation;
import usj.svit.architecture.individual.IIndividualTextEMF;

public interface ISelection<T extends IIndividualTextEMF> extends ICountableOperation {

	public List<T> selectParents(EAConfigTextEMF config);
	
	
	/**
	 * return the number of selected elements by the selection operation since the last reset and resets the value to 0.
	 * each selection operation may trigger the selection of 0 to POPULATION_SIZE individuals
	 * @return
	 */
	public long getAndResetAffected();
	
	/**
	 * return the number of calls to the selection operation since the last reset and resets the value to 0.
	 * @return
	 */
	public long getAndResetCalls();
}
