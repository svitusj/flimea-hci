package usj.svit.approach.flimea.core.operator.mutation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.Chromosome;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;
import usj.svit.architecture.utils.MathUtils;

public class ReformulationReduction extends Reformulation implements IMutation<IIndividualTextEMF> {
	//modification of MutationRandomEachGen
	
	private ArrayList<String> queryKeywords;
	private final int reductionValue=25;
	
	ArrayList<String> includedTerms=new ArrayList<String>();

	
	
	@Override
	public void mutate(EAConfigTextEMF context) {
		
		IPopulation<IIndividualTextEMF> population=context.getPopulation();
	//	List<IIndividualTextEMF> individualsList=population.getIndividuals();
		
		
		
		initializeRelevantDocsAndCorpus(population.getIndividuals());
		
		//removeDuplicateTermsInFile("/Users/paqui/Desktop/termsCopus-all.txt");
		
		//copying the offspring to do not modify the selected parents
		List<IIndividualTextEMF> offspring=context.getOffSpring();
		
		List<IIndividualTextEMF> mutatedOffspring = new ArrayList<IIndividualTextEMF>();
		Chromosome parent1 = offspring.get(0).getCopyOfGenes();
		Chromosome parent2 = offspring.get(1).getCopyOfGenes();
		IndividualTextEMF indiv1=new IndividualTextEMF(parent1,offspring.get(0).getParent());
		indiv1.setText(offspring.get(0).getText());
		
		IndividualTextEMF indiv2=new IndividualTextEMF(parent2,offspring.get(1).getParent());
		indiv2.setText(offspring.get(1).getText());
		
		mutatedOffspring.add(indiv1);
		mutatedOffspring.add(indiv2);
		
		context.getOffSpring().clear();
			
		for(IIndividualTextEMF individual : mutatedOffspring) {
			//mutate(individual,context);
			queryKeywords= getFilteredText(individual,individual.getParent().getSize());
			//int querySize=queryKeywords.size();
			ArrayList<String> termsToReduce=runReduction(corpusDocs, queryKeywords);
			//int termsSize=termsToReplace.size();
			
			/* terms to expand the MF are in the parent but not in the individual. 
			 * The position in the parent will be found to flip the gen.
			 * The first element that includes the term will be added in the MF.
			 */
			EObjectFragmentable parent=individual.getParent();
			//String text;
			String [] text;
			int parentSize=parent.getSize();
			int pos=0;
			
			int flippedToNegative=0;
			
			while(pos<parentSize && !termsToReduce.isEmpty())
			{
				//since this is a reduction, only gens set to 1 are checked to flip them.
				
				if(individual.getGen(pos))
				{
					text=parent.getFilteredTextForPosition(pos);
					for(int t=0;t<text.length;t++)
					{
						boolean genContainsTerms=termsToReduce.contains(text[t]);
						
						if(genContainsTerms) 
						{
						   termsToReduce.remove(text[t]);
						   
						  // break;
						}
						if(individual.getGen(pos) && genContainsTerms)
						{
							individual.flipGen(pos);
							System.out.println("Flipped gen "+pos+" to 0 reduce the term: "+text[t]);
							countAffected++;
							flippedToNegative++;
						}
					}
				}
				
				pos++;
			}	
		
			System.out.println("\n ** Gens flipped to 0: "+flippedToNegative);
		}
		countCalls++;
		
		//the offspring is updated with the mutated individuals
		context.getOffSpring().addAll(mutatedOffspring);
	}
	/**
	 * randomly mutates a bit of the individual
	 */
	@Override
	public void forceMutation(IIndividualTextEMF individual) {
		int index = MathUtils.getRandomInt(individual.getParentModelSize());
		individual.flipGen(index);
		countCalls++;
		countAffected++;
	}

	
	
	/**
	 * same probability for each model element of being added/removed
	 * 
	 * @param BinaryIndividual
	 * @return Individual
	 */
	
	private ArrayList<String> runReduction(ArrayList<HashMap<String,Integer>> corpusDocs, ArrayList<String> queryKeywords)
	{
		/*returns the list of terms to be reduced from the query. 
		 * Those terms appear in more than 25% of the documents in the corpus.
		 */
		ArrayList<String> result=new ArrayList<String>();
		ArrayList<String> termsToReduce=new ArrayList<String>();
		int totalCorpusDocs=corpusDocs.size();
		
		for (String term:queryKeywords)
		{
		    int appearance = getNumDocs(corpusDocs,term);
		    int appearanceValue=(appearance*100)/totalCorpusDocs;
		    if(appearanceValue>reductionValue)
		    {
		    	//term will be removed
		    	result.add(term);
		    }
		}

		termsToReduce=removeDuplicatesFromQuery(result);
		return termsToReduce;
	}
	
	private int getNumDocs(ArrayList<HashMap<String,Integer>> documents, String word)
	{
		//it gets the number of docs that a term appears
		int result=0;
		//number of times that a term appears in a document
		for (HashMap<String,Integer> doc: documents)
		{
			if(doc.containsKey(word)) result++;
		}
	
		return result;
	}
	
}