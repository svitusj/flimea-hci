package usj.svit.approach.flimea.core.operator;

public interface ICountableOperation {

	/**
	 * return the number of calls to the operation
	 * @return
	 */
	public long getAndResetCalls();
	
	/**
	 * return the number of elements affected by the operation
	 * @return
	 */
	public long getAndResetAffected();
	
	/**
	 * return a string describing the operator and its usage since last reset
	 */
	public String getDescription();
}
