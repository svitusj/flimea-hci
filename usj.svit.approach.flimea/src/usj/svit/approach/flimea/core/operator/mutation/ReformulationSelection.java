package usj.svit.approach.flimea.core.operator.mutation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.Chromosome;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;
import usj.svit.architecture.utils.MathUtils;

public class ReformulationSelection extends Reformulation implements IMutation<IIndividualTextEMF> {
	//modification of MutationRandomEachGen
	
	private ArrayList<String> queryKeywords;
	
	ArrayList<String> includedTerms=new ArrayList<String>();
	//a default value for the parameters is set
	private double TRdefault=0.25;
	private int numIterations=100;
	private int topTerms=300;
	private double dumpingFactor=0.85;
	
	public void setQuerySelectionValues(double TRdefault, int numIterations, int topTerms, double dumpingFactor)
	{
		this.TRdefault=TRdefault;
		this.numIterations=numIterations;
		this.topTerms=topTerms;
		this.dumpingFactor=dumpingFactor;
	}
	
	
	@Override
	public void mutate(EAConfigTextEMF context) {
		
		IPopulation<IIndividualTextEMF> population=context.getPopulation();
	//	List<IIndividualTextEMF> individualsList=population.getIndividuals();
		
		
		
		initializeRelevantDocsInList(population.getIndividuals());
		
		
		//copying the offspring to do not modify the selected parents
		List<IIndividualTextEMF> offspring=context.getOffSpring();
		
		List<IIndividualTextEMF> mutatedOffspring = new ArrayList<IIndividualTextEMF>();
		Chromosome parent1 = offspring.get(0).getCopyOfGenes();
		Chromosome parent2 = offspring.get(1).getCopyOfGenes();
		IndividualTextEMF indiv1=new IndividualTextEMF(parent1,offspring.get(0).getParent());
		indiv1.setText(offspring.get(0).getText());
		
		IndividualTextEMF indiv2=new IndividualTextEMF(parent2,offspring.get(1).getParent());
		indiv2.setText(offspring.get(1).getText());
		
		mutatedOffspring.add(indiv1);
		mutatedOffspring.add(indiv2);
		
		context.getOffSpring().clear();
			
		for(IIndividualTextEMF individual : mutatedOffspring) {
			//mutate(individual,context);
			queryKeywords= getFilteredText(individual,individual.getParent().getSize());
			//int querySize=queryKeywords.size();
			ArrayList<ArrayList<String>> documents= new ArrayList<ArrayList<String>>();
			documents.add(queryKeywords);
			
			String [] featureDescriptionTerms=context.getQuery().getText();
			ArrayList<String> list = new ArrayList<String>(featureDescriptionTerms.length);
			Collections.addAll(list, featureDescriptionTerms);
			documents.add(list);
			
			documents.addAll(relevantDocsInList);
			
			ArrayList<String> selectedTerms=runSelection(documents);
			//int termsSize=termsToReplace.size();
			
			/* The gens that include selected terms are set to 1. Otherwise, the gens are set to 0.
			 */
			EObjectFragmentable parent=individual.getParent();
			//String text;
			String [] text;
			int parentSize=parent.getSize();
			int pos=0;
			int flippedToPositive=0;
			int flippedToNegative=0;
		
			while(pos<parentSize)
			{
				
					text=parent.getFilteredTextForPosition(pos);
					boolean genContainsTerms=false;
					boolean positiveGen=individual.getGen(pos);
					
					for(int t=0;t<text.length;t++)
					{
						genContainsTerms=selectedTerms.contains(text[t]);						
						
					}
					
					if(genContainsTerms && !positiveGen) 
					{
					   individual.flipGen(pos);
					   System.out.println("Flipped gen "+pos+" to 1");
					   countAffected++;
					   flippedToPositive++;
					   break;
					}
					else if (!genContainsTerms && positiveGen)
					{
						individual.flipGen(pos);
						System.out.println("-- Flipped gen "+pos+" to 0");
						countAffected++;
						flippedToNegative++;
					}
				
				pos++;
			}
		
			System.out.println("\n ** Gens flipped to 1: "+flippedToPositive+", flipped to 0: "+flippedToNegative);	
		}
		countCalls++;
		
		//the offspring is updated with the mutated individuals
		context.getOffSpring().addAll(mutatedOffspring);
	}
	/**
	 * randomly mutates a bit of the individual
	 */
	@Override
	public void forceMutation(IIndividualTextEMF individual) {
		int index = MathUtils.getRandomInt(individual.getParentModelSize());
		individual.flipGen(index);
		countCalls++;
		countAffected++;
	}

	
	
	/**
	 * same probability for each model element of being added/removed
	 * 
	 * @param BinaryIndividual
	 * @return Individual
	 */
	
	private ArrayList<String> runSelection(ArrayList<ArrayList<String>> documents)
	{
		//obtains the different terms from the documents (no repetitions to represent the graph)
				ArrayList<String> termsWithoutRepetitions=getTermsFromDocumentsWithoutRepetitions(documents);
				
				//a TR for each different term is initializated to the default value
				HashMap <String, Double> TRValues=new HashMap <String, Double>();
				
				for(String term:termsWithoutRepetitions)
				{
					TRValues.put(term, TRdefault);
				}
				
				/*a column/row for each term in the graph. Each term is a vertex. 
				The number of the vertex is the index of the term in the arrayList*/
				int [][] graph=new int[termsWithoutRepetitions.size()][termsWithoutRepetitions.size()];
				//graph is initialized without arcs
				for(int i=0;i<graph.length;i++)
				{
					for(int j=0;j<graph.length;j++)
					{
					  graph[i][j]=0;
					}
				}
				
				//arcs are initialized from contiguous terms in a document. The graph is bidirectional.
				for(ArrayList<String> doc:documents)
				{
					
					String[] docArray = new String[doc.size()];

					for (int i = 0; i < doc.size(); i++) {
						docArray[i] = doc.get(i);
					}
					
					for(int i=0;i<docArray.length-1;i++)
					{
						String vertexAterm=docArray[i];
						String vertexBterm=docArray[i+1];
						
						//the number of each vertex is obtained
						int vertexAnumber=termsWithoutRepetitions.indexOf(vertexAterm);
						int vertexBnumber=termsWithoutRepetitions.indexOf(vertexBterm);
						
						//an arc is created between vertexA and vertexB and vice versa. The number of arcs is counted.
						graph[vertexAnumber][vertexBnumber]++;
						graph[vertexBnumber][vertexAnumber]++;
					}		
					
				}
				
				//TR values are obtained for each vertex according to the formula and number of iterations
				for(int i=0;i<numIterations;i++)
				{
				
					for(String term:termsWithoutRepetitions)
					{
						double TRsummation=0;
						int adjacents=0;
						ArrayList<Double> adjacentsTRValues=new ArrayList<Double>();
						
						//TR summation of the node list conected to the node of the term
						
						int vertexAnumber=termsWithoutRepetitions.indexOf(term);
						
						//getting adjacent vertexs for vertexAnumber
						for(int t=0;t<termsWithoutRepetitions.size();t++)
						{
							if(graph[vertexAnumber][t]>0)
							{
								//if >0 an arc has been set
								adjacents++;
								//term is obtained from the index
								String vertexB=termsWithoutRepetitions.get(t);
								double TRvalueVertexB=TRValues.get(vertexB);
								//TR value is added 
								adjacentsTRValues.add(TRvalueVertexB);
								
							}
						}
						
						if(adjacents>0)
						{
							for(double tRvalue: adjacentsTRValues)
							{
								TRsummation=TRsummation+(tRvalue/adjacents);
							}
						}
						
						
						
						double newTRvalue=(1-dumpingFactor)+ dumpingFactor * TRsummation;
						TRValues.replace(term, newTRvalue);
						
					}
					
					
				}
				//returns the top terms as result
				return getTopTerms(TRValues);
	}
	
	private ArrayList<String> getTopTerms(Map<String, Double> terms)
	{
		
		ArrayList<String> result=new ArrayList<String>();
		
		Map<String, Double> sortedTermsByTR=sortByComparator(terms, false);
		
		for (Entry<String, Double> entry : sortedTermsByTR.entrySet())
		{
			//entry contains each term and its TR value
		String term=entry.getKey();
		if(topTerms<=0) break;
		else if(!result.contains(term)) 
			{
			topTerms--;
			result.add(term);
			}
		}
	
		
		return result;
		
	}
	
	
}