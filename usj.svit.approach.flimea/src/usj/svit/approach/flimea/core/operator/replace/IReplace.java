package usj.svit.approach.flimea.core.operator.replace;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.ICountableOperation;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;

public interface IReplace<T extends IIndividualTextEMF> extends ICountableOperation {

	public IPopulation<T> replace(EAConfigTextEMF config);
	
	/**
	 * return the number of times the replace operation has been called since the last reset and resets the value to 0.
	 * @return
	 */
	public long getAndResetCalls();
	
	/**
	 * return the number of replaced elements by the replace operation since the last reset and resets the value to 0.
	 * each replace operation may trigger the replace of 0 to POPULATION_SIZE individuals
	 * @return
	 */
	public long getAndResetAffected();
}
