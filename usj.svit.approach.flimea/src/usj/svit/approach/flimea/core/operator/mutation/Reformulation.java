package usj.svit.approach.flimea.core.operator.mutation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.architecture.individual.IIndividualTextEMF;

public class Reformulation extends BaseOperation{

	protected int numberRelevantIndividuals=5;
	protected ArrayList<IIndividualTextEMF> corpusIndividuals= new ArrayList<IIndividualTextEMF>();
	protected  ArrayList<HashMap<String, Integer>> relevantDocs=new ArrayList<HashMap<String, Integer>>();
	protected ArrayList<ArrayList<String>> relevantDocsInList=new ArrayList<ArrayList<String>>();
	protected  ArrayList<HashMap<String, Integer>> corpusDocs=new ArrayList<HashMap<String, Integer>>();
	protected  List<IIndividualTextEMF> relevantIndividuals=new ArrayList<IIndividualTextEMF>();
	protected int termsToExpand=10;
	
	protected void initializeRelevantDocsAndCorpus(List<IIndividualTextEMF> individuals)
	{
		int i=0;
		for(IIndividualTextEMF individual:individuals)
		{
			HashMap<String,Integer> result=new HashMap<String,Integer>();
			result=individual.getFilteredTextsAndFrequencies();
			
			if(i<numberRelevantIndividuals)
			{
			relevantDocs.add(result);
			i++;
			}

			corpusDocs.add(result);
			
		}
	}
	protected void initializeRelevantDocsInList(List<IIndividualTextEMF> individuals)
	{
		//to maintain the order of the terms. Important in selection
		int i=0;
		for(IIndividualTextEMF individual:individuals)
		{
			if(i<numberRelevantIndividuals)
			{
			//with term repetitions
			relevantDocsInList.add(getFilteredText(individual, individual.getParentModelSize()));
			i++;
			}
			
		}
	}
	protected ArrayList<String> getFilteredText(IIndividualTextEMF individual,int parentSize) {
		// TODO Auto-generated method stub
		ArrayList<String> individualText=new ArrayList<String>();
		
		for (int i = 0; i < parentSize; i++) {
			if (individual.getGen(i)) {
				//individualText.addAll(Arrays.asList(individual.getParent().getTextForPosition(i)));
				individualText.addAll(Arrays.asList(individual.getParent().getFilteredTextForPosition(i)));
				//individualText.addAll(Arrays.asList(individual.getParent().getMetamodelTextForPosition(i)));
			}
		}
		return individualText;
	}
	protected HashMap<String,Integer> getTermsAndFrequenciesFromQuery(String[] text, HashMap<String,Integer> result)
	{
		
		for(String term:text)
		{
			if(result.containsKey(term)) result.replace(term, result.get(term)+1);
			else 
				{
				result.put(term,1);
				//writer.println(term);
				}
		}
		
		return result;
	}
	protected static Map<String, Double> sortByComparator(Map<String, Double> unsortMap, final boolean order)
	{
		//order attribute
		//	public static boolean ASC = true;
		//  public static boolean DESC = false;
	
	    List<Entry<String, Double>> list = new LinkedList<Entry<String, Double>>(unsortMap.entrySet());
	
	    // Sorting the list based on values
	    Collections.sort(list, new Comparator<Entry<String, Double>>()
	    {
	        public int compare(Entry<String, Double> o1,
	                Entry<String, Double> o2)
	        {
	            if (order)
	            {
	                return o1.getValue().compareTo(o2.getValue());
	            }
	            else
	            {
	                return o2.getValue().compareTo(o1.getValue());
	
	            }
	        }
	    });
	
	    // Maintaining insertion order with the help of LinkedList
	    Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
	    for (Entry<String, Double> entry : list)
	    {
	        sortedMap.put(entry.getKey(), entry.getValue());
	    }
	
	    return sortedMap;
	}
	protected ArrayList<String> removeDuplicatesFromQuery(ArrayList<String> query)
	{
		ArrayList<String> result=new ArrayList<String>();
		
		for (String term:query)
		{
		  if(!result.contains(term)) result.add(term);
		}
		return result;
	}
	protected ArrayList<String> getTermsFromDocumentsWithoutRepetitions(ArrayList<ArrayList<String>> documents)
	{
		ArrayList<String> terms=new ArrayList<String>();
	 
		for(ArrayList<String> doc:documents)
		{
			for(String t:doc)
			{
				if(!terms.contains(t)) terms.add(t);
			}
		}
		return terms;
	}
}
