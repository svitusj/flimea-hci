package usj.svit.approach.flimea.core.operator.crossover;

import java.util.List;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.ICountableOperation;
import usj.svit.architecture.individual.IIndividualTextEMF;

public interface ICrossover<T extends IIndividualTextEMF> extends ICountableOperation {

	/**
	 * This method performs a crossover of the two individuals provided in the {@link BaseEAConfigTextEMF#offspring} and stores them in the same place. It also returns the newly created elements for testing purposes.
	 * @param config A configuration holding the parameters that may be needed for the crossover.
	 * @return A list holding the new individuals (usually 2) or null if the crossover is not performed (usually because of the probability)
	 */
	List<T> crossover(EAConfigTextEMF config);
	
	/**
	 * This method performs two crossovers (mask positive and mask negative) of the two individuals provided as input and returns them.
	 * @param parent1 first parent
	 * @param parent2 second parent
	 * @return a list with the two new individuals
	 */
	List<T> crossover(T parent1, T parent2);
	
	/**
	 * return the number of times that the crossover operation has been executed since the last reset and resets the value to 0.
	 * @return
	 */
	public long getAndResetCalls();
	
	/**
	 * return the number of new individuals created since the last reset and resets the value to 0.
	 * each crossover operation generates 2 new individuals (possitive and negative crossover)
	 * @return
	 */
	public long getAndResetAffected();
}
