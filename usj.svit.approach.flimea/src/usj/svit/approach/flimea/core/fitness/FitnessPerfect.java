package usj.svit.approach.flimea.core.fitness;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.Chromosome;

public class FitnessPerfect extends BaseOperation implements IFitness<IIndividualTextEMF> {

	@Override
	public IPopulation<IIndividualTextEMF> assess(EAConfigTextEMF context) {

		for (IIndividualEMF individual : context.getPopulation().getIndividuals()) {
			individual.setFitness(assess(individual, context.getCurrentTestCaseAnswer()));
		}
		
		//System.out.println("Gen "+context.getCurrentGeneration()+" avg fitness: "+fitness/(double)amount);
		
		return context.getPopulation();
	}

	private double assess(IIndividualEMF individual, IIndividualEMF oracle){
		
		Chromosome copyIndividual = individual.getCopyOfGenes();
		//xor operation will put 1 in "incorrect" bits and 0 in "correct" ones.
		copyIndividual.xor(oracle.getGenes());
		int possitive = copyIndividual.getSize() - copyIndividual.cardinality();
		int negative = copyIndividual.cardinality();
		int result =  possitive - negative;
		//System.out.println("Fitness Perfect: "+result+" / "+copyIndividual.getSize()+" = "+((double)result/copyIndividual.getSize())+" (POS: "+possitive+" - NEG: "+negative+")");
		if(negative == 0)
			System.out.println("Fitness Perfect: "+result+" / "+copyIndividual.getSize()+" = "+((double)result/copyIndividual.getSize())+" (POS: "+possitive+" - NEG: "+negative+")");
		return ((double)result/copyIndividual.getSize());
	}

	@Override
	public void setBoundaries(EAConfigTextEMF config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IIndividualTextEMF assess(IIndividualTextEMF individual,EAConfigTextEMF config) {
		individual.setFitness(assess(individual, config.getCurrentTestCaseAnswer()));
		return individual;
	}
}
