package usj.svit.approach.flimea.core.fitness;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.utils.MathUtils;

public class FitnessRandom extends BaseOperation implements IFitness<IIndividualTextEMF> {

	@Override
	public IPopulation<IIndividualTextEMF> assess(EAConfigTextEMF context) {
		for(IIndividualTextEMF individual : context.getPopulation().getIndividuals()) {
			assess(individual,context);
		}
		return context.getPopulation();
	}

	@Override
	public void setBoundaries(EAConfigTextEMF config) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IIndividualTextEMF assess(IIndividualTextEMF individual,EAConfigTextEMF config) {
		individual.setFitness(MathUtils.getRandomFloat());
		return individual;
	}

}
