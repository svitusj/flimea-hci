package usj.svit.approach.flimea.core.fitness;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.ICountableOperation;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;

public interface IFitness<T extends IIndividualTextEMF> extends ICountableOperation {

	/**
	 * Modifies the population updating the fitness for each individual
	 * @param config of the EA
	 * @return the population modified for testing purposes (context.population has been modified)
	 */
	public IPopulation<T> assess(EAConfigTextEMF config);
	
	/**
	 * Gives the opportunity to preprocess the boundaries for the current
	 * TestCase, should be called before using fitness and after preprocessing the query
	 */
	public void setBoundaries(EAConfigTextEMF config);
	
	/**
	 * Not always possible, depends on the Fitness
	 * @param individual to be assessed and returned after
	 */
	public T assess(T individual,EAConfigTextEMF config);
}
