package usj.svit.approach.flimea.core.fitness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ejml.alg.dense.mult.VectorVectorMult;
import org.ejml.simple.SimpleMatrix;
import org.ejml.simple.SimpleSVD;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividual;
import usj.svit.architecture.individual.IIndividualText;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;
import usj.svit.architecture.individual.impl.Query;

/**
 * http://www1.se.cuhk.edu.hk/~seem5680/lecture/LSI-Eg.pdf
 * @author usuario
 *
 */
public class FitnessLSI extends BaseOperation implements IFitness<IIndividualTextEMF> {
	
	private SimpleMatrix termByDocumentCoOccurrenceMatrix;
	private SimpleMatrix queryMatrix;
	//private int dimension = 2;
	
	public FitnessLSI() {
	}

	@Override
	public IPopulation<IIndividualTextEMF> assess(EAConfigTextEMF context) {
		
		// TODO: Resolver el problema de la dimensi�n (controlar dimensiones m�ximas, etc)
		
		
		// Retrieve the terms from the population and the query
		Set<String> terms = new HashSet<String>();
		
		for(IIndividual individual : context.getPopulation().getIndividuals()) {
			if(individual instanceof IIndividualText) {
				terms.addAll(Arrays.asList(((IIndividualText)individual).getText()));
			}			
		}		
		terms.addAll(Arrays.asList(context.getQuery().getText()));
		
//		if(Logger.enabled)
//		Logger.log(this.getClass().getCanonicalName(), "List of terms: "+terms.toString());
//		
		// Build the Term-by-Document Co-Ocurrence Matrix
		buildTermByDocumentCoOccurrenceMatrix(terms, context.getPopulation().getIndividuals());
		
		// Build the Query Matrix
		buildQueryMatrix(terms, context.getQuery());
		
		// Calculate the similitude values between documents and query
		calculateLSISimilitudeValues(context.getPopulation(), context.getLSIDimension());
		
		return context.getPopulation();
	}
	
	/* LSI Methods -> Build matrix, build query, calculate LSI similitude values, produce ranking */
	
	private void buildTermByDocumentCoOccurrenceMatrix(Set<String> terms, List<IIndividualTextEMF> individuals)
	{
		/* What needs to be done is count the term occurrences in each document */
		/* For each term, count the occurrences in a document, then go to the next document */
		// Start at row 0 (first term)
		int rowControl = 0;

		// The matrix has to have dimensions [number of keywords][number of documents]
		double[][] termByDocumentCoOccurrenceMatrixData = new double[terms.size()][individuals.size()];
				
		// For each term in the list of terms
		for(String term : terms)
		{
			// Restart the column count at the beginning (we are at the beginning of a term calculations, we have to go through all the documents)
			int colControl = 0;
			
			// For each individual in the list of individuals
			for(IIndividual individual : individuals)
			{				
				// Count the number of occurrences of the term in the processed document
				int termOccurrences = count(term, (IIndividualText)individual);
				
				// Set the value on the matrix
				termByDocumentCoOccurrenceMatrixData[rowControl][colControl] = termOccurrences;
				
				// Increase the column (we go to the next document)
				colControl++;
			}
			
			// Once the term occurrences have been calculated for all the documents, go to the next row (next term)
			rowControl++;
		}
		
		// When all values have been calculated, then fill the matrix data
		termByDocumentCoOccurrenceMatrix = new SimpleMatrix(termByDocumentCoOccurrenceMatrixData);
//		if(Logger.enabled) Logger.log(getClass().getCanonicalName(), "CoOc matrix:");
//		if(Logger.enabled) Logger.log(getClass().getCanonicalName(), termByDocumentCoOccurrenceMatrix);
	}
	
	private void buildQueryMatrix(Set<String> terms, Query query)
	{
		/* What needs to be done is count term occurrences in the query for each term */

		// Start at row 0 (first term)
		int rowControl = 0;
		
		// The query is just one column
		double[][] queryMatrixData = new double[terms.size()][1];
				
		// For each term in the list of terms
		for(String term : terms)
		{
	
			// Count the number of occurrences of the term in the query
			int termOccurrences = count(term, query);

			// Set the value on the matrix
			queryMatrixData[rowControl][0] = termOccurrences;

			// Once the term occurrences have been calculated the query, go to the next row (next term)
			rowControl++;
		}
		
		// When all values have been calculated, then fill the matrix data
		queryMatrix = new SimpleMatrix(queryMatrixData);
//		if(Logger.enabled) Logger.log(getClass().getCanonicalName(), "Query matrix");
//		if(Logger.enabled) Logger.log(getClass().getCanonicalName(), queryMatrix);
	}

	private void calculateLSISimilitudeValues(IPopulation<IIndividualTextEMF> population, int dimension)
	{
		// Get the SVD decomposition of the Term-by-Document Co-Occurrence Matrix
		SimpleSVD<SimpleMatrix> matrixSVD = termByDocumentCoOccurrenceMatrix.svd(true);
		
		// Get the three matrices that compose the SVD
		SimpleMatrix u = matrixSVD.getU();
		SimpleMatrix w = matrixSVD.getW();
		SimpleMatrix v = matrixSVD.getV();
		
		/* Reduce the matrices to work with a certain number of dimensions */
		SimpleMatrix reducedU = new SimpleMatrix(u.numRows(),dimension);
		SimpleMatrix reducedW = new SimpleMatrix(dimension,dimension);
		SimpleMatrix reducedV = new SimpleMatrix(v.numRows(),dimension);
		
		/* Fill the reduced matrices */
		// Fill reducedU
		for(int row = 0; row < u.numRows(); row++)
		{
			for(int col = 0; col < dimension; col++)
				reducedU.set(row, col, u.get(row,col));
		}
		// Fill reducedW
		for(int row = 0; row < dimension; row++)
		{
			for(int col = 0; col < dimension; col++)
				reducedW.set(row, col, w.get(row,col));
		}
		// Fill reducedV
		for(int row = 0; row < v.numRows(); row++)
		{
			for(int col = 0; col < dimension; col++)
				reducedV.set(row, col, v.get(row,col));
		}
					
		// The vectors of the documents are stored in the V matrix rows
		// Create a list of vectors and add the V matrix rows recursively
		List<SimpleMatrix> documentVectors = new ArrayList<SimpleMatrix>();
		for(int i = 0; i < reducedV.numRows(); i++)
		{
			documentVectors.add(reducedV.extractVector(true, i));
		}
	
		// Obtain the vector associated to the query
		SimpleMatrix transposedQuery = queryMatrix.transpose();
		SimpleMatrix invertedW = reducedW.invert();
		SimpleMatrix queryVector = transposedQuery.mult(reducedU).mult(invertedW);
		
		// Calculate the cosine similitude for each of the documents
		int documentIndex = 0;
		for(SimpleMatrix vector : documentVectors)
		{
			// Calculate the dot product between the document vector and the query vector, and the norms of both vectors
			double dotProd = VectorVectorMult.innerProd(queryVector.getMatrix(), vector.getMatrix());
			double queryNorm = queryVector.normF();
			double vectorNorm = vector.normF();
			
			// Perform the similitude operation
			double similitude = dotProd / (queryNorm * vectorNorm);
			
			// Store the similitude value as fitness
			population.get(documentIndex).setFitness(similitude);
			
			documentIndex++;
		}		
	}
	
	
	/* Auxiliary methods */
	
	private static int count(String term, IText individual)
	{	
		return Collections.frequency(Arrays.asList(individual.getText()), term);
	}
	
	@Override
	public void setBoundaries(EAConfigTextEMF config) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Cannot work! returns null
	 */
	@Override
	public IIndividualTextEMF assess(IIndividualTextEMF individual,EAConfigTextEMF config) {
		// TODO Auto-generated method stub
		return null;
	}

}
