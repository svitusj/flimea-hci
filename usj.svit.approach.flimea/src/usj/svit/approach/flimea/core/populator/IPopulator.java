package usj.svit.approach.flimea.core.populator;

import java.util.List;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.ICountableOperation;
import usj.svit.architecture.individual.IIndividualEMF;

public interface IPopulator<T extends IIndividualEMF> extends ICountableOperation {

	/**
	 * generates a list of individuals following a specific strategy.
	 * @param context
	 * @return
	 */
	List<T> generateInitialPopulation(EAConfigTextEMF context);
	
	/**
	 * generates a single individual (FTP)
	 * @param context
	 * @return
	 */
	T generateIndividual(EAConfigTextEMF context);

	/**
	 * return the number of calls to the generate*  operation since the last reset and resets the value to 0.
	 * each sort operation may trigger the sort of 0 to POPULATION_SIZE individuals
	 * @return
	 */
	public long getAndResetCalls();
	
	/**
	 * return the number of elements generated trough this populator
	 */
	public long getAndResetAffected();
	
}
