package usj.svit.approach.flimea.core.populator;

import java.util.ArrayList;
import java.util.List;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.utils.MathUtils;

public class PopulatorMixed <T extends IIndividualTextEMF> extends BaseOperation implements IPopulator<T> {

	@Override
	public List<T> generateInitialPopulation(EAConfigTextEMF context) {
		
		ArrayList<IIndividualTextEMF> lista = new ArrayList<IIndividualTextEMF>();
		for(int i=0;i<context.MAX_POPULATION;i++) {
			
			//individual.randomizeGenes();
			lista.add(generateIndividual(context));
			countCalls -= 1;
		}
		countCalls += 1;
		return (List<T>) lista;
	}

	@Override
	public T generateIndividual(EAConfigTextEMF context) {
		IndividualTextEMF individual = new IndividualTextEMF(context.getSearchSpace());
		
		if(MathUtils.getRandomDouble()>0.5)
			individual.getGenes().negate();
		countCalls += 1;
		countAffected += 1;
		return (T) individual;
	}
	
}
