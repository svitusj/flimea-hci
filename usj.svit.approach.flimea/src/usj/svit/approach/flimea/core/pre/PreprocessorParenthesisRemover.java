package usj.svit.approach.flimea.core.pre;

import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

public class PreprocessorParenthesisRemover<T extends IIndividualTextEMF> extends BaseTextIndividualAndQueryPreprocessor<T> {

	@Override
	public String[] process(IText individual) {
		for(int i=0;i<individual.getText().length;i++){
			
			individual.getText()[i] = individual.getText()[i].replaceAll("[\\(\\)]", "");
		}
		return individual.getText();
	}

}