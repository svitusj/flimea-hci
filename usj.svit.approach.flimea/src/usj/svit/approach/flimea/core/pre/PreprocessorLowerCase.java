package usj.svit.approach.flimea.core.pre;

import java.util.Locale;

import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

public class PreprocessorLowerCase<T extends IIndividualTextEMF> extends BaseTextIndividualAndQueryPreprocessor<T> {

	@Override
	public String[] process(IText individual) {
		for(int i=0;i<individual.getText().length;i++){
			individual.getText()[i] = individual.getText()[i].toLowerCase(Locale.ROOT);
		}
		return individual.getText();
	}

}
