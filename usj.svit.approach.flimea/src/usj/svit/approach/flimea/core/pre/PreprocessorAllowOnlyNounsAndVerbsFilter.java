package usj.svit.approach.flimea.core.pre;

import java.util.ArrayList;
import java.util.stream.Stream;

import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

public class PreprocessorAllowOnlyNounsAndVerbsFilter<T extends IIndividualTextEMF> extends BaseTextIndividualAndQueryPreprocessor<T> {
	
	public enum Locale {
		EN, ES
	} Locale locale;
	
	public PreprocessorAllowOnlyNounsAndVerbsFilter(Locale locale) {
		this.locale = locale;
	}
	
	@Override
	public String[] process(IText individual) {
		final String[] typesOf = getLocales();
		
		ArrayList<String> auxText = new ArrayList<String>();
		ArrayList<String> auxTags = new ArrayList<String>();
		
		for (int i = 0; i < individual.getTags().length; i++) {
			for (int j = 0; j < typesOf.length; j++) {
				if (individual.getTags()[i].equals(typesOf[j])) {
					auxText.add(individual.getText()[i]);
					auxTags.add(individual.getTags()[i]);
				}
			}
		}
		
		individual.setText(auxText.toArray(new String[auxText.size()]));
		individual.setTags(auxTags.toArray(new String[auxTags.size()]));
		
		return individual.getText();
	}
	
	private String[] getLocales() {
		return Stream.of(getLocaleNouns(), getLocaleVerbs())
				.flatMap(Stream::of)
				.toArray(String[]::new);
	}
	
	private String[] getLocaleNouns() {
		switch (locale) {
		case ES:
			return new String[] {"NC"};
		case EN:
			return new String[] {"NN", "NNS", "NNP", "NNPS"};
		default:
			return new String[] {"NN"};
		}
	}
	
	private String[] getLocaleVerbs() {
		switch (locale) {
		case ES:
			return new String[] {"VMN", "VMS", "CS"};
		case EN:
		default:
			return new String[] {"VB", "VBD", "VBG", "VBN", "VBP", "VBZ"};
		}
	}

}
