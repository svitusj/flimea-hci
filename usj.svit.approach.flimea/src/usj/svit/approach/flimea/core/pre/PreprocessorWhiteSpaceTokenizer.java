package usj.svit.approach.flimea.core.pre;

import java.util.ArrayList;
import java.util.Arrays;

import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

/**
 * Changes the size of the array
 * @author jfont
 *
 */
public class PreprocessorWhiteSpaceTokenizer<T extends IIndividualTextEMF> extends BaseTextIndividualAndQueryPreprocessor<T> {
	
	public String[] process(IText individual) {
		
		ArrayList<String> newProcessedText = new ArrayList<String>();
		
		for(String string : individual.getText()) {
			newProcessedText.addAll(Arrays.asList(process(string)));
		}
		return newProcessedText.toArray(new String[newProcessedText.size()]);
	}
	
	private String [] process(String text) {
		
		return text.split(" ");
	}
}
