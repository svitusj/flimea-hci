package usj.svit.approach.flimea.core.pre;

import java.util.ArrayList;
import java.util.List;

import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

public class PreprocessorTokenRemover<T extends IIndividualTextEMF> extends BaseTextIndividualAndQueryPreprocessor<T> {
	
	private String[] tokenList;
	
	public PreprocessorTokenRemover(List<String> tokenList) {
		this.tokenList = tokenList.toArray(new String[tokenList.size()]);
	}
	
	@Override
	public String[] process(IText individual) {		
		ArrayList<String> auxText = new ArrayList<String>();
		
		boolean found;
		for (int i = 0; i < individual.getText().length; i++) {
			found = false;
			for (int j = 0; j < tokenList.length; j++) {
				if (individual.getText()[i].equals(tokenList[j]) && !found) {
					found = true;
				}
			}
			if (!found) {
				auxText.add(individual.getText()[i]);
			}
		}
		
		individual.setText(auxText.toArray(new String[auxText.size()]));
		
		return individual.getText();
	}
	
}
