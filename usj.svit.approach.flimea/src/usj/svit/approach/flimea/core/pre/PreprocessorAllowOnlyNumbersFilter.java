package usj.svit.approach.flimea.core.pre;

import java.util.ArrayList;

import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

public class PreprocessorAllowOnlyNumbersFilter<T extends IIndividualTextEMF> extends BaseTextIndividualAndQueryPreprocessor<T> {

	@Override
	public String[] process(IText text) {
		
		ArrayList<String> auxText = new ArrayList<String>();
		ArrayList<String> auxTags = new ArrayList<String>();
		
		for(int i=0;i<text.getTags().length;i++) {
			if(text.getTags()[i].equals("CD")) {
				auxText.add(text.getText()[i]);
				auxTags.add(text.getTags()[i]);
			}
		}
		
		text.setText(auxText.toArray(new String[auxText.size()]));
		text.setTags(auxTags.toArray(new String[auxTags.size()]));
		return text.getText();
	}
	

}

