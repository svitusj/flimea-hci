package usj.svit.approach.flimea.core.pre;

import java.text.Normalizer;

import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

public class PreprocessorAccentRemover<T extends IIndividualTextEMF> extends BaseTextIndividualAndQueryPreprocessor<T> {

	@Override
	public String[] process(IText individual) {
		for (int i = 0; i < individual.getText().length; i++) {
			individual.getText()[i] = Normalizer.normalize(individual.getText()[i], Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		}
		
		return individual.getText();
	}

}