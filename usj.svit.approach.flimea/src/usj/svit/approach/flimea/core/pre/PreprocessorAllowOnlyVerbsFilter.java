package usj.svit.approach.flimea.core.pre;

import java.util.ArrayList;

import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

public class PreprocessorAllowOnlyVerbsFilter<T extends IIndividualTextEMF> extends BaseTextIndividualAndQueryPreprocessor<T> {
	private final String[] typesOfVerbs = {"VB", "VBD", "VBG", "VBN", "VBP", "VBZ"};
	
	@Override
	public String[] process(IText query) {
		ArrayList<String> auxText = new ArrayList<String>();
		ArrayList<String> auxTags = new ArrayList<String>();
		
		for (int i = 0; i < query.getTags().length; i++) {
			for (int j = 0; j < typesOfVerbs.length; j++) {
				if (query.getTags()[i].equals(typesOfVerbs[j])) {
					auxText.add(query.getText()[i]);
					auxTags.add(query.getTags()[i]);
				}
			}
		}
		
		query.setText(auxText.toArray(new String[auxText.size()]));
		query.setTags(auxTags.toArray(new String[auxTags.size()]));
		
		return query.getText();
	}
}
