package usj.svit.approach.flimea.core.pre;

import java.io.IOException;
import java.io.InputStream;

import opennlp.tools.lemmatizer.DictionaryLemmatizer;
import usj.svit.architecture.individual.IText;

public class PreprocessorLemmatizer extends BaseTextIndividualAndQueryPreprocessor {

	static PreprocessorLemmatizer instance;
	private final String dictRoute = "/resources/en-lemmatizer.dict";

	private DictionaryLemmatizer lemmatizer;

	public static PreprocessorLemmatizer getInstance() {

		if (instance == null)
			instance = new PreprocessorLemmatizer();

		return instance;
	}

	private PreprocessorLemmatizer() {

		try {
			InputStream is = this.getClass().getResourceAsStream(dictRoute);
			lemmatizer = new DictionaryLemmatizer(is);
			is.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public String[] process(IText text) {
		
		String[] lemma = lemmatizer.lemmatize(text.getText(), text.getTags());
		
//		if(Logger.enabled) Logger.log(getClass().getCanonicalName(), Arrays.toString(text.getText())+" - "+Arrays.toString(text.getTags())+" -> "+Arrays.toString(lemma));
		
		text.setText(lemma);
		return text.getText();
	}

}
