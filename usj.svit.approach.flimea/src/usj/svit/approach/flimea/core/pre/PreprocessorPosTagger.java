package usj.svit.approach.flimea.core.pre;

import java.io.IOException;
import java.io.InputStream;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.util.InvalidFormatException;
import usj.svit.architecture.individual.IText;

public class PreprocessorPosTagger extends BaseTextIndividualAndQueryTaggerPreprocessor {

	public final static int en = 0;	
	public final static int es = 1;
	private final String[] posModelRoutes = {"/resources/en-pos-maxent.bin","/resources/es-pos-maxent.bin"};
	
	private static PreprocessorPosTagger instance;

	private POSTaggerME tagger;
	
	public static PreprocessorPosTagger getInstance(int language) {
		if(instance== null)
			instance = new PreprocessorPosTagger(language);
		
		return instance;
	}

	private PreprocessorPosTagger(int posModel) {
		
		try {
			InputStream is = this.getClass().getResourceAsStream(posModelRoutes[posModel]);
			POSModel model = new POSModel(is);
			tagger = new POSTaggerME(model);
			is.close();	
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String[] process(IText text) {
		return tagger.tag(text.getText());
	}

}
