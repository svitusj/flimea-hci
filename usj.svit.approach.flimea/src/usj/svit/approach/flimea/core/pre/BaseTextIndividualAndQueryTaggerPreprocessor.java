package usj.svit.approach.flimea.core.pre;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

public abstract class BaseTextIndividualAndQueryTaggerPreprocessor<T extends IIndividualTextEMF> implements IPreProcessor<T> {

	@Override
	/**
	 * Checks if the individuals are IITexts, if so sets the text to the processed ones.
	 */
	public void process(EAConfigTextEMF context) {
		
//		if (Logger.ENABLED_EA_PREPROCESSOR)
//			Logger.log(Logger.WHO_EA_PREPROCESSOR, getClass().getCanonicalName(), Logger.ACTION_LOG, this.getClass().getCanonicalName());

		for(IText node : context.getSearchSpace().getIndex().getFilteredElements()) {
			node.setTags(process(node));
	}
		context.getQuery().setTags(process(context.getQuery()));
	}

	public abstract String[] process(IText text);


	

}
