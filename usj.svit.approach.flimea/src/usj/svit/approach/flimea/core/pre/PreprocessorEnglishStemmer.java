package usj.svit.approach.flimea.core.pre;

import java.util.ArrayList;

import org.tartarus.snowball.ext.englishStemmer;

import usj.svit.architecture.individual.IText;

public class PreprocessorEnglishStemmer extends BaseTextIndividualAndQueryPreprocessor {

	private englishStemmer stemmer;
	
	private static PreprocessorEnglishStemmer instance;
	
	private PreprocessorEnglishStemmer(){
		stemmer = new englishStemmer();
	}
	
	public static PreprocessorEnglishStemmer getInstance() {
		if(instance == null)
			instance = new PreprocessorEnglishStemmer();
		return instance;
	}
	
	@Override
	public String[] process(IText individual) {
		
		ArrayList<String> newProcessedText = new ArrayList<String>();
		
		for(String string : individual.getText()) {
			newProcessedText.add(process(string));
		}
		return newProcessedText.toArray(new String[newProcessedText.size()]);
		
	}
	
	private String process(String text) {
		
		stemmer.setCurrent(text);
		stemmer.stem();
		return stemmer.getCurrent();
	}

}
