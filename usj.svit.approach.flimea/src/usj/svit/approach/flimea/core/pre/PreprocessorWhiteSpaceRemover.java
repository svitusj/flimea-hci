package usj.svit.approach.flimea.core.pre;

import java.util.ArrayList;

import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

/**
 * removes "empty" words
 * @author usuario
 *
 * @param <T>
 */
public class PreprocessorWhiteSpaceRemover<T extends IIndividualTextEMF> extends BaseTextIndividualAndQueryPreprocessor<T> {

	@Override
	public String[] process(IText individual) {
		
		ArrayList<String> newProcessedText = new ArrayList<String>();
		
		for(int i=0;i<individual.getText().length;i++){
			String trimedText = individual.getText()[i].trim();
			
			if(trimedText.length() > 0)
				newProcessedText.add(trimedText);
		}
		return newProcessedText.toArray(new String[newProcessedText.size()]);
	}

}
