package usj.svit.approach.flimea.core.pre;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

public abstract class BaseTextIndividualAndQueryPreprocessor<T extends IIndividualTextEMF> implements IPreProcessor<T> {

	
	@Override
	/**
	 * Checks if the individuals are IITexts, if so sets the text to the processed ones.
	 */
	public void process(EAConfigTextEMF context) {
		
//		if (Logger.ENABLED_EA_PREPROCESSOR)
//			Logger.log(Logger.WHO_EA_PREPROCESSOR, getClass().getCanonicalName(), Logger.ACTION_LOG, this.getClass().getCanonicalName());
		int i=0;
		for(IText node : context.getSearchSpace().getIndex().getFilteredElements()) {
			
			String [] processed = process(node);
			
//			if (Logger.ENABLED_EA_PREPROCESSOR)
//				Logger.log(Logger.WHO_EA_PREPROCESSOR, getClass().getCanonicalName(), Logger.ACTION_LOG, i+" - "+Arrays.toString(node.getText())+" - "+Arrays.toString(node.getTags())+" -> "+Arrays.toString(processed));
//			
			
			node.setText(processed);
			i++;
		}
		
		//System.out.println("Query before: "+Arrays.toString(context.getQuery().getText()));
		
		context.getQuery().setText(process(context.getQuery()));
		

		//System.out.println("Query After: "+Arrays.toString(context.getQuery().getText()));
	}

	public abstract String[] process(IText individual);


	

}
