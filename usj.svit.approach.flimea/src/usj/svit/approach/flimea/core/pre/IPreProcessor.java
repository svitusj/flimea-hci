package usj.svit.approach.flimea.core.pre;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.IText;

public interface IPreProcessor<T extends IIndividualTextEMF> {

	public void process(EAConfigTextEMF config);
	public String[] process(IText text);

}
