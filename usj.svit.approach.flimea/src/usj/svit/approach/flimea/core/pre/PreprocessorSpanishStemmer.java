package usj.svit.approach.flimea.core.pre;

import java.util.ArrayList;

import org.tartarus.snowball.ext.spanishStemmer;

import usj.svit.architecture.individual.IText;

public class PreprocessorSpanishStemmer extends BaseTextIndividualAndQueryPreprocessor {

	private spanishStemmer stemmer;
	
	private static PreprocessorSpanishStemmer instance;
	
	private PreprocessorSpanishStemmer(){
		stemmer = new spanishStemmer();
	}
	
	public static PreprocessorSpanishStemmer getInstance() {
		if(instance == null)
			instance = new PreprocessorSpanishStemmer();
		return instance;
	}
	
	@Override
	public String[] process(IText individual) {
		
		ArrayList<String> newProcessedText = new ArrayList<String>();
		
		for(String string : individual.getText()) {
			newProcessedText.add(process(string));
		}
		return newProcessedText.toArray(new String[newProcessedText.size()]);
		
	}
	
	private String process(String text) {
		
		stemmer.setCurrent(text);
		stemmer.stem();
		return stemmer.getCurrent();
	}

}
