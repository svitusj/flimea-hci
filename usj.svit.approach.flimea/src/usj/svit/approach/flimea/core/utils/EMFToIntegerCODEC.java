package usj.svit.approach.flimea.core.utils;

import java.util.HashMap;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

public class EMFToIntegerCODEC  {

	private EObject root;	
	private HashMap<Integer, EObject> mapIDTOEOBject;
	private HashMap<EObject, Integer> mapEObjectToID;
	
	public EMFToIntegerCODEC(EObject root) {
		
		this.root = root;
		mapIDTOEOBject = new HashMap<Integer,EObject>();
		mapEObjectToID = new HashMap<EObject,Integer>();
		
		TreeIterator<EObject> elements = root.eAllContents();
		int index = 0;
		
		while (elements.hasNext()) {
			EObject object = elements.next();
			mapIDTOEOBject.put(index, object);
			
			mapEObjectToID.put(object, index);
		//	if(Logger.enabled) Logger.log(getClass().getCanonicalName(), object);
			index++;
		}
	}
	
	public int getSize() {
		return mapIDTOEOBject.size();
	}

	public EObject getRoot() {
		return root;
	}
	
	public HashMap<Integer, EObject> getMapIDTOEOBject() {
		return mapIDTOEOBject;
	}

	public HashMap<EObject, Integer> getMapEObjectToID() {
		return mapEObjectToID;
	}
	
	public String toString() {
	
		StringBuilder sb = new StringBuilder();
		TreeIterator<EObject> elements = getRoot().eAllContents();
		
		sb.append("Codec: ROOT -> "+defaultToString(root)+"\r\n");
		
		sb.append("EObject -> ID\r\n");
		while (elements.hasNext()) {
			EObject object = elements.next();
			int id = getMapEObjectToID().get(object);
			sb.append(object+" -> "+id+"\r\n");
		}
		
		sb.append("ID -> EObject\r\n");
		
		for(int i : getMapIDTOEOBject().keySet()){
			EObject obj = getMapIDTOEOBject().get(i);
			sb.append(i+" -> "+obj+"\r\n");
		}
		sb.append("\r\n");
		return sb.toString();
	}
	
	public static String defaultToString(Object o) {
	     return o.getClass().getName() + "@" + 
	            Integer.toHexString(System.identityHashCode(o));
	}
}
