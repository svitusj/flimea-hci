package usj.svit.approach.flimea.core.utils;

import java.util.ArrayList;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import usj.svit.approach.flimea.core.encoding.IEncoding;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;

public class EncodingEMF implements IEncoding<EObject,IIndividualTextEMF> {

	EObjectFragmentable parent;
	
	public EncodingEMF(EObject root) {
		//this.parent = new EObjectFragmentable(root,new IndexBuilder(root));
	}
	
	public IIndividualTextEMF encode() {
		return new IndividualTextEMF(parent);
	}

	public EObject decode(IIndividualTextEMF individual) {
		
		EObject root = EcoreUtil.copy(parent.getEObject());
		EMFToIntegerCODEC auxCODEC = new EMFToIntegerCODEC(root);
		
		TreeIterator<EObject> elements = EcoreUtil.getAllContents(auxCODEC.getRoot(),false);
		
		ArrayList<EObject> toDelete = new ArrayList<EObject>();
		
		while (elements.hasNext()) {
			EObject object = elements.next();
			
			int index = auxCODEC.getMapEObjectToID().get(object);
			if(!individual.getGen(index)) {
				toDelete.add(object);
			}
		}
		
		for(EObject obj : toDelete) {
			EcoreUtil.delete(obj);
		}
		
		return auxCODEC.getRoot();
	}
	
}


