package usj.svit.approach.flimea.core.utils;

import java.util.List;

import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.results.IEMFResult;

public class EMFTextResults implements IEMFResult<IIndividualTextEMF> {

	IPopulation<IIndividualTextEMF> population;
	
	public EMFTextResults(IPopulation<IIndividualTextEMF> population) {
		this.population = population;
	}
	
	@Override
	public List<IIndividualTextEMF> getResults() {
		population.sortPopulationDescendant();
		return population.getIndividuals();
	}

}
