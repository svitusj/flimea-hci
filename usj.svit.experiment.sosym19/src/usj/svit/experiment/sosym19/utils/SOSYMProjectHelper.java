package usj.svit.experiment.sosym19.utils;

import usj.svit.helper.workspace.helper.ProjectHelper;

public class SOSYMProjectHelper {
	
	private SOSYMProjectHelper() {
		if(projectHelper == null) 
			projectHelper = new ProjectHelper(PROJECT_NAME);
	}
	
	private static final String PROJECT_NAME = "usj.svit.experiment.sosym19";
	
	private static ProjectHelper projectHelper = null;
	
	public static void createFile(String path,String content){
		if(projectHelper == null) 
			projectHelper = new ProjectHelper(PROJECT_NAME);
		
		projectHelper.createFile(path, content);
	}

	public static ProjectHelper getProjectHelper() {
		if(projectHelper == null) 
			projectHelper = new ProjectHelper(PROJECT_NAME);
		return projectHelper;
	}
}
