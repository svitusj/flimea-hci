package usj.svit.experiment.sosym19.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.crossover.CrossoverMask;
import usj.svit.approach.flimea.core.operator.mutation.MutationRandomGen;
import usj.svit.approach.flimea.core.operator.replace.ReplaceElitism;
import usj.svit.approach.flimea.core.operator.selection.SelectionRouletteWheel;
import usj.svit.approach.flimea.core.population.Population;
import usj.svit.approach.flimea.core.populator.PopulatorMixed;
import usj.svit.approach.flimea.core.populator.PopulatorRandom;
import usj.svit.approach.flimea.core.pre.IPreProcessor;
import usj.svit.approach.flimea.core.pre.PreprocessorAccentRemover;
import usj.svit.approach.flimea.core.pre.PreprocessorLowerCase;
import usj.svit.approach.flimea.core.pre.PreprocessorParenthesisRemover;
import usj.svit.approach.flimea.core.pre.PreprocessorPosTagger;
import usj.svit.approach.flimea.core.pre.PreprocessorSpanishStemmer;
import usj.svit.approach.flimea.core.pre.PreprocessorTokenRemover;
import usj.svit.approach.flimea.core.pre.PreprocessorWhiteSpaceRemover;
import usj.svit.approach.flimea.core.pre.PreprocessorWhiteSpaceTokenizer;
import usj.svit.approach.flimea.lsifitness.FitnessLSISimple;
import usj.svit.approach.flimea.lsifitness.FitnessLSISimpleWithClusters;
import usj.svit.architecture.individual.IIndividualTextEMF;

public class SoSyMConfigs {

	private static final long TIME_PER_TC = 2000;
	public static int POPULATION = 20;
	public static int GENERATIONS = 30000;

	private static ArrayList<IPreProcessor<IIndividualTextEMF>> getPreprocessorsCAF() {
		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		// preprocessors.add(new
		// PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorAccentRemover<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorParenthesisRemover<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorWhiteSpaceTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorLowerCase<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorWhiteSpaceRemover<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorTokenRemover<IIndividualTextEMF>(Arrays.asList("normalstate","fragmentsource","parentstate","transition","trigger","conditions","effect","triggeralias","conditionsalias","effectalias","triggertime","conditionstime","effecttime","triggertypeoftime","conditionstypeoftime","effecttypeoftime","fragmentsource","start","end","null","equipment","name","bool","type","value","hasproperties","hasequipments","hasorders","property")));
		
//		preprocessors.add(PreprocessorPosTagger.getInstance(PreprocessorPosTagger.es));
		// preprocessors.add(new
		// PreprocessorAllowOnlyNounsFilter<IIndividualTextEMF>());
//		preprocessors.add(PreprocessorSpanishStemmer.getInstance());
		return preprocessors;
	}

	private static ArrayList<IPreProcessor<IIndividualTextEMF>> getPreprocessorsBSH() {
		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();
		// preprocessors.add(new
		// PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorAccentRemover<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorParenthesisRemover<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorWhiteSpaceTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorLowerCase<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorWhiteSpaceRemover<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorTokenRemover<IIndividualTextEMF>(Collections.singletonList("null")));

		//preprocessors.add(PreprocessorPosTagger.getInstance(PreprocessorPosTagger.en));
		// preprocessors.add(new
		// PreprocessorAllowOnlyNounsFilter<IIndividualTextEMF>());
		//preprocessors.add(PreprocessorSpanishStemmer.getInstance());
		return preprocessors;
	}
	
	private static ArrayList<IPreProcessor<IIndividualTextEMF>> getPreprocessorsOracleChecker() {
		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();
		// preprocessors.add(new
		// PreprocessorUnderscoreTokenizer<IIndividualTextEMF>());
		// preprocessors.add(new PreprocessorAccentRemover<IIndividualTextEMF>());
		// preprocessors.add(new PreprocessorParenthesisRemover<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorWhiteSpaceTokenizer<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorLowerCase<IIndividualTextEMF>());
		preprocessors.add(new PreprocessorWhiteSpaceRemover<IIndividualTextEMF>());

	//	preprocessors.add(PreprocessorPosTagger.getInstance(PreprocessorPosTagger.en));
		// preprocessors.add(new
		// PreprocessorAllowOnlyNounsFilter<IIndividualTextEMF>());
	//	preprocessors.add(PreprocessorSpanishStemmer.getInstance());
		return preprocessors;
	}
	
	public static EAConfigTextEMF getOracleCheckerConfig(){
		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSISimple(), getPreprocessorsOracleChecker(),
				Collections.emptyList()) {

			public boolean stopConditionMeet() {
				if (getCurrentGeneration() >= MAX_GENERATIONS)
					return true;
				return false;
			};
		};

		config.MAX_GENERATIONS = GENERATIONS;
		config.MAX_POPULATION = POPULATION;

		return config;
	}

	public static EAConfigTextEMF getBSH_RS_Config(){
		
		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSISimple(), getPreprocessorsBSH(),
				Collections.emptyList()) {

			public boolean stopConditionMeet() {
				if (getCurrentGeneration() >= MAX_GENERATIONS)
					return true;
				return false;
			};
		};

		config.MAX_GENERATIONS = GENERATIONS;
		config.MAX_POPULATION = POPULATION;

		return config;
	}
	
	public static EAConfigTextEMF getCAF_LSISimple_Config() {

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSISimple(), getPreprocessorsCAF(),
				Collections.emptyList()) {

			public boolean stopConditionMeet() {
				if (getCurrentGeneration() >= MAX_GENERATIONS)
					return true;
				return false;
			};
		};

		config.MAX_GENERATIONS = GENERATIONS;
		config.MAX_POPULATION = POPULATION;

		return config;
	}

	public static EAConfigTextEMF getBSH_LSISimple_Timed_Config(long time) {

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSISimpleWithClusters(), getPreprocessorsBSH(),
				Collections.emptyList()) {

			public boolean stopConditionMeet() {
				if (getElapsedTime() >= MAX_TIME)
					return true;
				return false;
			};
		};

		config.MAX_GENERATIONS = GENERATIONS;
		config.MAX_POPULATION = POPULATION;
		config.MAX_TIME = time;
		

		return config;
	}
	
	public static EAConfigTextEMF getCAF_LSISimple_Timed_Config(long time) {

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSISimple(), getPreprocessorsCAF(),
				Collections.emptyList()) {

			public boolean stopConditionMeet() {
				if (getElapsedTime() >= MAX_TIME)
					return true;
				return false;
			};
		};

		config.MAX_GENERATIONS = GENERATIONS;
		config.MAX_POPULATION = POPULATION;
		config.MAX_TIME = time;
		
		return config;
	}
	
	public static EAConfigTextEMF getBSH_LSISimple_Config() {

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSISimple(), getPreprocessorsBSH(),
				Collections.emptyList()) {

			public boolean stopConditionMeet() {
				if (getCurrentGeneration() >= MAX_GENERATIONS)
					return true;
				return false;
			};
		};

		config.MAX_GENERATIONS = GENERATIONS;
		config.MAX_POPULATION = POPULATION;
		

		return config;
	}

	public static EAConfigTextEMF getLSINoText() {

		ArrayList<IPreProcessor<IIndividualTextEMF>> preprocessors = new ArrayList<IPreProcessor<IIndividualTextEMF>>();

		EAConfigTextEMF config = new EAConfigTextEMF(new Population<IIndividualTextEMF>(),
				new PopulatorRandom<IIndividualTextEMF>(), new CrossoverMask(), new MutationRandomGen(),
				new SelectionRouletteWheel(), new ReplaceElitism(), new FitnessLSISimple(), preprocessors,
				new ArrayList()) {

			public boolean stopConditionMeet() {
				if (getCurrentGeneration() >= MAX_GENERATIONS)
					return true;
				if (getPopulation().getIndividuals().get(0).getFitness() == 1.0) {
					// System.out.println("Found the perfect one in Gen
					// "+getCurrentGeneration());
					return true;
				}
				return false;
			};
		};

		config.LSI_DIMENSIONS = 20;
		// config.MAX_GENERATIONS = 30000;
		// config.MAX_POPULATION = 100;
		// config.MUTATION_PROBABILITY = 10;

		return config;
	}

}
