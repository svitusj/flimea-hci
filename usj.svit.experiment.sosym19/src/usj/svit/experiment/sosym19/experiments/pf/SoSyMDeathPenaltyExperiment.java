package usj.svit.experiment.sosym19.experiments.pf;

import java.util.ArrayList;
import java.util.Collections;

import usj.svit.approach.flimea.core.config.EAConfigFactory;
import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.architecture.approach.IApproachTextEMF;
import usj.svit.architecture.experiment.ConfigFactory;
import usj.svit.architecture.experiment.ExperimentEMFTextTimed;
import usj.svit.architecture.oracle.BaseOracleEMFText;
import usj.svit.architecture.views.ExperimentViewEntry;
import usj.svit.experiment.sosym19.approaches.SoSyMEA;
import usj.svit.experiment.sosym19.utils.SOSYMProjectHelper;

public class SoSyMDeathPenaltyExperiment extends ExperimentViewEntry {

	@Override
	public void run() {
		ArrayList<BaseOracleEMFText> oracles = new ArrayList<BaseOracleEMFText>();

		//Add Suitable oracles
		//oracles.add(new IH62OracleEMFText());
		//oracles.add(new TCMLOracleEMFText());
		
		ArrayList<IApproachTextEMF> approaches = new ArrayList<IApproachTextEMF>();
		EAConfigTextEMF baselineConfig = EAConfigFactory.getConfig(EAConfigFactory.FIT_PER,EAConfigFactory.STOP_GEN);
		baselineConfig.MAX_GENERATIONS = 30000;
		approaches.add(new SoSyMEA(baselineConfig,SoSyMEA.MODE_DEATH_PENALTY,Collections.emptyList()));
		
		ExperimentEMFTextTimed experimentBSH = new ExperimentEMFTextTimed(oracles,approaches,SOSYMProjectHelper.getProjectHelper(),ConfigFactory.getBaseConfig());
		
		long t0 = System.currentTimeMillis();
		experimentBSH.executeExperiment();
		long t1 = System.currentTimeMillis();
		System.out.println("Time executing: "+ (t1-t0));
			
		experimentBSH.postExperiment();
		
		long t2 = System.currentTimeMillis();
		
		System.out.println("Time processing logs: "+ (t2-t1));
		
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return "SoSyM18 - Death Penalty";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "SoSyM18 - Death Penalty";
	}

}
