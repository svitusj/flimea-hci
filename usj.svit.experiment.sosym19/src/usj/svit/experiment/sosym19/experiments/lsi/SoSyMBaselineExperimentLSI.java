package usj.svit.experiment.sosym19.experiments.lsi;

import java.util.ArrayList;
import java.util.Collections;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.architecture.approach.IApproachTextEMF;
import usj.svit.architecture.experiment.ConfigFactory;
import usj.svit.architecture.experiment.ExperimentEMFTextTimed;
import usj.svit.architecture.oracle.BaseOracleEMFText;
import usj.svit.architecture.oracle.json.Filter;
import usj.svit.architecture.oracle.json.JsonTestCase;
import usj.svit.architecture.views.ExperimentViewEntry;
import usj.svit.experiment.sosym19.approaches.SoSyMEA;
import usj.svit.experiment.sosym19.utils.SOSYMProjectHelper;
import usj.svit.experiment.sosym19.utils.SoSyMConfigs;
import usj.svit.oracle.tcml.mock.oracle.TCMLOracleEMFText;

public class SoSyMBaselineExperimentLSI extends ExperimentViewEntry {

	private static final int TIME_PER_TC = 30000;
	
	@Override
	public void run() {		
		
		
		ArrayList<BaseOracleEMFText> oracles = new ArrayList<BaseOracleEMFText>();
		
		ArrayList<Filter> filters = new ArrayList<Filter>();
		//Add Suitable oracles
		filters.add(new Filter<JsonTestCase>(JsonTestCase.class, Filter.Attributes.id, "151", Filter.Operation.LESS));
		//oracles.add(new IH62OracleEMFText());
		oracles.add(new TCMLOracleEMFText(filters));
		
		ArrayList<IApproachTextEMF> approaches = new ArrayList<IApproachTextEMF>();
		EAConfigTextEMF baselineConfig = SoSyMConfigs.getCAF_LSISimple_Timed_Config(TIME_PER_TC);
		
		approaches.add(new SoSyMEA(baselineConfig,SoSyMEA.MODE_BASELINE,Collections.emptyList()));
		
		ExperimentEMFTextTimed experimentBSH = new ExperimentEMFTextTimed(oracles,approaches,SOSYMProjectHelper.getProjectHelper(),ConfigFactory.getBaseConfig());
		
		long t0 = System.currentTimeMillis();
		experimentBSH.executeExperiment();
		long t1 = System.currentTimeMillis();
		System.out.println("Time executing: "+ (t1-t0));
			
		experimentBSH.postExperiment();
		
		long t2 = System.currentTimeMillis();
		
		System.out.println("Time processing logs: "+ (t2-t1));
		
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return "SoSyM18 - Baseline BSH - LSI";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "SoSyM18 - Baseline BSH - LSI";
	}

}
