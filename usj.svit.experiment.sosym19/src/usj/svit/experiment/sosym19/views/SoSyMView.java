package usj.svit.experiment.sosym19.views;

import java.util.ArrayList;
import java.util.List;

import usj.svit.architecture.views.ExperimentViewEntry;
import usj.svit.architecture.views.ExperimentsView;
import usj.svit.experiment.sosym19.experiments.lsi.SoSyMBaselineExperimentLSI;
import usj.svit.experiment.sosym19.experiments.lsi.SoSyMClosedOperationsExperimentLSI;
import usj.svit.experiment.sosym19.experiments.lsi.SoSyMDeathPenaltyExperimentLSI;
import usj.svit.experiment.sosym19.experiments.lsi.SoSyMDynamicDegreePenaltyExperimentLSI;
import usj.svit.experiment.sosym19.experiments.lsi.SoSyMDynamicPenaltyExperimentLSI;
import usj.svit.experiment.sosym19.experiments.lsi.SoSyMRepairAddExperimentLSI;
import usj.svit.experiment.sosym19.experiments.lsi.SoSyMRepairRemoveExperimentLSI;
import usj.svit.experiment.sosym19.experiments.lsi.SoSyMStaticDegreePenaltyExperimentLSI;
import usj.svit.experiment.sosym19.experiments.lsi.SoSyMStaticPenaltyExperimentLSI;
import usj.svit.experiment.sosym19.experiments.lsi.SoSyMStrongEncodingExperimentLSI;
import usj.svit.experiment.sosym19.experiments.pf.SoSyMBaselineExperiment;
import usj.svit.experiment.sosym19.experiments.pf.SoSyMClosedOperationsExperiment;
import usj.svit.experiment.sosym19.experiments.pf.SoSyMDeathPenaltyExperiment;
import usj.svit.experiment.sosym19.experiments.pf.SoSyMDynamicDegreePenaltyExperiment;
import usj.svit.experiment.sosym19.experiments.pf.SoSyMDynamicPenaltyExperiment;
import usj.svit.experiment.sosym19.experiments.pf.SoSyMRepairAddExperiment;
import usj.svit.experiment.sosym19.experiments.pf.SoSyMRepairRemoveExperiment;
import usj.svit.experiment.sosym19.experiments.pf.SoSyMStaticDegreePenaltyExperiment;
import usj.svit.experiment.sosym19.experiments.pf.SoSyMStaticPenaltyExperiment;
import usj.svit.experiment.sosym19.experiments.pf.SoSyMStrongEncodingExperiment;

public class SoSyMView extends ExperimentsView {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "usj.svit.experiment.sosym19.views.SOSYM19View";

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 * @return 
	 */	
	@Override
	public List<ExperimentViewEntry> populateWithExperiments() {		
		List<ExperimentViewEntry> demo = new ArrayList<ExperimentViewEntry>();
		demo.add(new SoSyMBaselineExperiment());
		demo.add(new SoSyMBaselineExperimentLSI());
		demo.add(new SoSyMStaticPenaltyExperiment());
		demo.add(new SoSyMStaticPenaltyExperimentLSI());
		demo.add(new SoSyMStaticDegreePenaltyExperiment());
		demo.add(new SoSyMStaticDegreePenaltyExperimentLSI());
		demo.add(new SoSyMDynamicPenaltyExperiment());
		demo.add(new SoSyMDynamicPenaltyExperimentLSI());
		demo.add(new SoSyMDynamicDegreePenaltyExperiment());
		demo.add(new SoSyMDynamicDegreePenaltyExperimentLSI());
		demo.add(new SoSyMDeathPenaltyExperiment());
		demo.add(new SoSyMDeathPenaltyExperimentLSI());
		demo.add(new SoSyMStrongEncodingExperiment());
		demo.add(new SoSyMStrongEncodingExperimentLSI());
		demo.add(new SoSyMClosedOperationsExperiment());
		demo.add(new SoSyMClosedOperationsExperimentLSI());
		demo.add(new SoSyMRepairAddExperiment());
		demo.add(new SoSyMRepairAddExperimentLSI());
		demo.add(new SoSyMRepairRemoveExperiment());
		demo.add(new SoSyMRepairRemoveExperimentLSI());
		return demo;
	}
}
