package usj.svit.experiment.sosym19.approaches;

import java.util.List;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.ea.BaseEATextEMF;
import usj.svit.approach.flimea.core.pre.IPreProcessor;
import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.oracle.TestCaseTextEMF;
import usj.svit.architecture.results.EMFResultSingle;
import usj.svit.architecture.results.IEMFResult;

public class SoSyMEA extends BaseEATextEMF {

	static int id_counter=0;
	int id;
	
	
	public static final int MODE_BASELINE = 0;
	public static final int MODE_STATIC_PENALTY = 1;
	public static final int MODE_STATIC_DEGREE_PENALTY = 2;
	public static final int MODE_DINAMIC_PENALTY = 3;
	public static final int MODE_DYNAMIC_DEGREE_PENALTY = 4;
	public static final int MODE_DEATH_PENALTY = 5;
	public static final int MODE_STRONG_ENCODING = 6;
	public static final int MODE_CLOSED_OPERATIONS = 7;
	public static final int MODE_REPAIR_ADD = 8;
	public static final int MODE_REPAIR_REMOVE = 9;
	
	private int mode;
	private List<Double> values;
	private FeasibilityChecker checker;
	
	private void handleUnfeasible(){
		if(mode == MODE_STATIC_PENALTY){
			for(IIndividualTextEMF individual : config.getPopulation().getIndividuals()){
				if(checker.isFeasible(individual)!= 0){
					individual.setFitness(individual.getFitness()-values.get(0));
				}
			}
		}
		else if(mode == MODE_STATIC_DEGREE_PENALTY) {
			for(IIndividualTextEMF individual : config.getPopulation().getIndividuals()){
				
				individual.setFitness(individual.getFitness()-values.get(0)*checker.isFeasible(individual));
				
			}
		}
		else if(mode == MODE_DINAMIC_PENALTY) {
			for(IIndividualTextEMF individual : config.getPopulation().getIndividuals()){
				if(checker.isFeasible(individual)!= 0){
					individual.setFitness(individual.getFitness()-values.get(0)*config.getCurrentGeneration());
				}
			}
		}
		else if(mode == MODE_DYNAMIC_DEGREE_PENALTY) {
			for(IIndividualTextEMF individual : config.getPopulation().getIndividuals()){
				
				individual.setFitness(individual.getFitness()-values.get(0)*config.getCurrentGeneration()*checker.isFeasible(individual));
				
			}
		}
		else if(mode == MODE_DEATH_PENALTY) {
			for(IIndividualTextEMF individual : config.getPopulation().getIndividuals()){
				if(checker.isFeasible(individual)!= 0){
					config.getPopulation().getIndividuals().set(config.getPopulation().getIndividuals().indexOf(individual), new IndividualTextEMF(this.config.getSearchSpace()));
					//individual.setFitness(individual.getFitness()-values.get(0)*config.getCurrentGeneration());
					//System.out.println("Killed!");
				}
			}
		}
		else if(mode == MODE_REPAIR_ADD) {
			for(IIndividualTextEMF individual : config.getPopulation().getIndividuals()){
				checker.repairAdd(individual);
			}
			
		}
		else if(mode == MODE_REPAIR_REMOVE) {
			for(IIndividualTextEMF individual : config.getPopulation().getIndividuals()){
				checker.repairRemove(individual);
			}
		}
		
	}
	
	public SoSyMEA(EAConfigTextEMF config, int mode, List<Double> values) {
		super(config);
		id = id_counter;
		id_counter++;
		this.mode = mode;
		this.values = values;
	}

	@Override
	public IEMFResult<IIndividualEMF> run(TestCaseTextEMF testCase) {
		
		if(mode != 0){
			checker = new FeasibilityChecker(testCase.getSearchSpace(),testCase.getAnswer());
		}
		//System.out.println("Missplaced :"+checker.isFeasible(testCase.getAnswer())+" / "+testCase.getAnswer().getNumberOfPossitiveGenes());

		this.config.setCurrentTestCase(testCase);
		
		this.config.getPopulation().setPopulation(config.getPopulator().generateInitialPopulation(config));
		
		for (IPreProcessor<IIndividualTextEMF> preprocessor : config.getPreprocessors()) {
			preprocessor.process(config);
		}
		
		for (IIndividualTextEMF element : config.getPopulation().getIndividuals()) {
			element.updateTextsAfterRecombinations();
		}
		
		this.config.getFitness().setBoundaries(this.config);
		
		this.loop();
		System.out.println(this.getClass().getSimpleName()+" - TC: "+testCase.getID()+" - "+config.getPopulation().get(0).getFitness());
		return new EMFResultSingle<IIndividualEMF>(config.getPopulation().get(0));
		
	}
	
	@Override
	public void loop() {

		while (true) {

			config.getFitness().assess(config);
			
			handleUnfeasible();
			
			
			config.getPopulation().sortPopulationDescendant();
			
			if (config.stopConditionMeet()) {
				
				return;
			}

			config.getSelection().selectParents(config);
			config.getCrossover().crossover(config);
			config.getMutation().mutate(config);
			
			for (IIndividualTextEMF element : config.getOffSpring()) {
				element.updateTextsAfterRecombinations();
			}
			
			config.getReplace().replace(config);
			
			config.increaseCurrentGeneration();
		}
	}

	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return "SoSyMEA_"+id;
	}

}

