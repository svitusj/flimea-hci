package usj.svit.experiment.sosym19.approaches;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.IndividualEMF;
import usj.svit.architecture.individual.impl.eof.BaseNode;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;
import usj.svit.architecture.individual.impl.eof.NodeEAttribute;
import usj.svit.architecture.individual.impl.eof.NodeEObject;
import usj.svit.architecture.individual.impl.eof.NodeEReference;

public class FeasibilityChecker {

	EObjectFragmentable parent;
	
	HashMap<Integer,List<Integer>> feasibilityMap = new HashMap<Integer, List<Integer>>();
	
	public FeasibilityChecker(EObjectFragmentable parent,IndividualEMF solution) {
		this.parent = parent;
		
		buildFeasibilityMap();
		fixFM(solution);
		
	}
	
	private void fixFM(IndividualEMF individual) {
		for(int position = 0; position < individual.getParent().getSize(); position++){
			if(individual.getGen(position)){
				if(feasibilityMap.get(position) != null){	
					for(Integer req : feasibilityMap.get(position)){
						if(!individual.getGen(req)){
							//List<Integer> auxList = feasibilityMap.get(position);
							//auxList.remove(req);
							//if(auxList.size()== 0) {
								feasibilityMap.remove(position);
								break;
//							}
//							else
//								feasibilityMap.replace(position, auxList);		
						}
					}
				}
			}
		}
		
	}

	private ArrayList<Integer> getIdsList(ArrayList<? extends BaseNode> lista){
		ArrayList<Integer> ids = new ArrayList<Integer>();
		
		for(BaseNode node : lista){
			ids.add(Integer.parseInt(node.getId()));
		}
		
		return ids;
	}
	
	private void buildFeasibilityMap() {
		
		for(NodeEObject parentNode : parent.getIndex().getAllEObjects()){
			
			
		//	System.out.println(parentNode.getId()+" - "+parentNode.getEobject());
			for(NodeEAttribute attribute : parentNode.getAttributes()){
				//System.out.println("\t"+attribute.getId()+" - "+attribute.getEobject());
				feasibilityMap.put(Integer.parseInt(attribute.getId()), Arrays.asList(Integer.parseInt(parentNode.getId())));
			}
			for(NodeEReference reference : parentNode.getReferences()){
				
				//System.out.println("\t"+reference.getId()+" - "+reference.getEobject());
				
				List<Integer> lista = new ArrayList<Integer>();
				lista.addAll(getIdsList(reference.getContents()));
				lista.add(Integer.parseInt(parentNode.getId()));
				
				feasibilityMap.put(Integer.parseInt(reference.getId()), lista);
				
			}
		}
		
		for(int key : feasibilityMap.keySet()){
	//		System.out.println(key+" -> "+feasibilityMap.get(key));
			
		}
		
	}
	
	public int isFeasible(IIndividualEMF individual){
		int misplaced = 0;
		for(int position = 0; position < individual.getParent().getSize(); position++){
			if(individual.getGen(position)){
				misplaced += fullfillsConstraints(position,individual);
			}
		}
		return misplaced;
	}
	
	private int fullfillsConstraints(int id,IIndividualEMF individual){
		if(feasibilityMap.get(id) == null)
			return 0;
		int misplaced = 0;
		for(Integer req : feasibilityMap.get(id)){
			if(!individual.getGen(req)){
//				System.out.println(id+" - is broken, requires ("+req+"):"+feasibilityMap.get(id)+" - "+parent.getIndex().getNodeForPosition(id).getEobject());
				misplaced++;;
			}
		}
		return misplaced;	
	}

	public void repairAdd(IIndividualTextEMF individual) {
		for(int position = 0; position < individual.getParent().getSize(); position++){
			if(individual.getGen(position)){
				if(feasibilityMap.get(position) != null){	
					for(Integer req : feasibilityMap.get(position)){
						if(!individual.getGen(req)){
							individual.flipGen(req);
						}
					}
				}
			}
		}
		
	}

	public void repairRemove(IIndividualTextEMF individual) {
		for(int position = 0; position < individual.getParent().getSize(); position++){
			if(individual.getGen(position)){
				if(feasibilityMap.get(position) != null){	
					for(Integer req : feasibilityMap.get(position)){
						if(!individual.getGen(req)){
							individual.flipGen(position);
							break;
						}
					}
				}
			}
		}
		
	}
	
}
