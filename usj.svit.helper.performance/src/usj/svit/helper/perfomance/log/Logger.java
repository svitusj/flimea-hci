package usj.svit.helper.perfomance.log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import com.google.gson.GsonBuilder;

import usj.svit.helper.workspace.helper.ProjectHelper;

public class Logger {

	private static final boolean minified = true;

	private static ArrayList<String> classNames = new ArrayList<String>();

	public static final String WHO_EXPERIMENT = "EXPERIMENT";
	public static final String WHO_ORACLE = "ORACLE";
	public static final String WHO_APPROACH = "APP";
	public static final String WHO_TC = "TC";
	public static final String WHO_EA = "EA";

	public static final String WHAT_EXPERIMENT_EXECUTE = "RUN";
	public static final Object WHAT_EA_EXECUTE = "RUN";
	public static final boolean ENABLED_TC = false;

	public static final Object WHAT_TC_EXECUTE = "RUN";
	public static final boolean ENABLED_APPROACH = true;

	public static final Object WHAT_APPROACH_EXECUTE = "RUN";
	public static final Object WHAT_APPROACH_PERSIST = "SAVE RESULTS";

	public static final String WHAT_NOTHING = "";
	// Oracle Loading

	public static final boolean ENABLED_ORACLE = true;

	public static final boolean ENABLED_EA_SORT = false;

	public static final String WHO_EA_SORT = "SORT";
	
	// FLiMEA

	public static String WHO_EA_PREPROCESSOR = "Preprocessor";
	public static String WHO_EA_FITNESS = "Fitness";
	public static String WHO_EA_MUTATION = "Mutation";
	public static String WHO_EA_CROSSOVER = "Crossover";
	public static String WHO_EA_SELECTION = "Selection";
	public static String WHO_EA_REPLACE = "Replace";

	public static boolean ENABLED_EXPERIMENT = true;
	public static boolean ENABLED_EA = false;
	public static boolean ENABLED_EA_FITNESS = false;
	public static boolean ENABLED_EA_MUTATION = false;
	public static boolean ENABLED_EA_CROSSOVER = false;
	public static boolean ENABLED_EA_SELECT = false;
	public static boolean ENABLED_EA_REPLACE = false;
	public static boolean ENABLED_EA_PREPROCESSOR = false;

	public static String ACTION_START = "S";
	public static String ACTION_FINISH = "F";

	public static String WHAT_EA_TC_RUN = "Solve TC:";

	public static String WHAT_ORACLE_LOAD = "Load Oracle:";
	public static String WHAT_ORACLE_EXECUTE = "Execute TC:";

	public static String WHAT_EA_GENERATION = "Gen:";

	// IH62 generator
	public static String WHO_IH62_GENERATOR = "IH62 Generator";
	public static boolean WHO_IH62_GENERATOR_ENABLED = false;

	// IH62 generator
	public static String WHO_TCML_GENERATOR = "TCML Generator";
	public static boolean WHO_TCML_GENERATOR_ENABLED = false;

	// Query reformulation
	public static boolean WHO_QUERY_REFORMULATION_ENABLED = false;
	public static String WHO_QUERY_REFORMULATOR = "QUERY REFORMULATOR";

	public static boolean enabled = false;

	private static ArrayList<PrintStream> outters = new ArrayList<PrintStream>();

	static File logFile;
	static File timeFile;
	static ProjectHelper helper;
	static String baseName;
	
	public static void setLogFiles(ProjectHelper helper,String baseName) {
		Logger.helper = helper;
		Logger.baseName = baseName;
		
		outters.clear();
		helper.createFile(baseName+"/log.txt", "");
		logFile = helper.getFile(baseName+"/log.txt");
		try {
			outters.add(new PrintStream(logFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		timeFile = helper.getFile(baseName+"/time.json");
	}

	public static void log(String who, String className, String what, Object msg) {

		if (minified) {
			minilog(who, what, msg);
			return;
		}
		long now = System.currentTimeMillis();
		int cn = classNames.indexOf(className);
		if (cn == -1) {
			classNames.add(className);
			cn = classNames.size() - 1;
		}

		// double time = (now-then);
		if (what.equals(ACTION_FINISH))
			level--;

		for (PrintStream out : outters) {
			indent(out);
			out.println(who + " - " + now + " - " + cn + " - " + what + " - " + msg);
		}

		if (what.equals(ACTION_START))
			level++;

		// then = now;
	}

	private static void minilog(String who, String what, Object msg) {
		long now = System.currentTimeMillis();

		if (what.equals(ACTION_FINISH))
			level--;

		for (PrintStream out : outters) {
			indent(out);
			out.println(who + " - " + now + " -  " + msg);
		}

		if (what.equals(ACTION_START))
			level++;
	}

	private static int level = 0;

	private static final String[] indent = new String[] { "", " ", "  ", "   ", "    ", "     ", "      ", "       ",
			"        ", "         ", "          " };

	private static void indent(PrintStream out) {
		if (level > 0 && level < indent.length)
			out.print(indent[level]);
	}

	public static TimedElement processLogAndReport() {

		try {
			
			TimedElement baseElement = processLog();

			reportTimes(baseElement);
			
			return baseElement;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static void reportTimes(TimedElement baseElement) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Experiment Total Time:;"+baseElement.time+"\r\n");
		sb.append("# Oracles:;"+baseElement.contents.size()+"\r\n");
		
		for(TimedElement oracle: baseElement.contents){
			
			sb.append(";Oracle Total Time:;"+oracle.time+"\r\n");
			
			for(TimedElement app: oracle.contents){
				
				sb.append(";;APP Total Time:;"+app.time+"\r\n");
				
				for(TimedElement tc: app.contents){
					
					sb.append(";;;"+tc.msg+":;"+tc.time+"\r\n");
					
					for(TimedElement ea : tc.contents){
						
						sb.append(";;;;"+ea.msg+":;"+ea.time+"\r\n");
						
						for(TimedElement gens : ea.contents){
							
							sb.append(";;;;;"+gens.msg+":;"+gens.time+"\r\n");
							
//							for(TimedElement ops : gens.contents) {
//								
//								sb.append(";;;;;;"+ops.msg+":;"+ops.time+"\r\n");
//							}
							sb.append(";;;;;;"+gens.contents.get(0).msg+":;"+gens.contents.get(0).time+"\r\n");
							sb.append(";;;;;;;"+gens.contents.get(1).msg+":;"+gens.contents.get(1).time+"\r\n");
							
							if(gens.contents.size()>2){
								sb.append(";;;;;;;;"+gens.contents.get(2).msg+":;"+gens.contents.get(2).time+"\r\n");
								sb.append(";;;;;;;;;"+gens.contents.get(3).msg+":;"+gens.contents.get(3).time+"\r\n");
								sb.append(";;;;;;;;;;"+gens.contents.get(4).msg+":;"+gens.contents.get(4).time+"\r\n");
								sb.append(";;;;;;;;;;;"+gens.contents.get(5).msg+":;"+gens.contents.get(5).time+"\r\n");
							}
							
							
						}
						
					}
					
				}
				
			}
			
		}
		
		helper.createFile(baseName+"/time.csv", sb.toString());
	}

	private static TimedElement processLog() throws IOException {

		int currentLevel = 0;
		TimedElement[] openElements = new TimedElement[indent.length];
		
		BufferedReader br = new BufferedReader(new FileReader(logFile));

		String currentLine = br.readLine();

		String[] words = currentLine.split(" - ");

		TimedElement baseElement = new TimedElement(words[0], Long.parseLong(words[1]), words[2]);

		openElements[currentLevel] = baseElement;

		while ((currentLine = br.readLine()) != null) {
			
			int elementLevel = getIndentLevel(currentLine);
			currentLine = currentLine.substring(elementLevel);
			words = currentLine.split(" - ");

			if (elementLevel > currentLevel) {

				TimedElement element = new TimedElement(words[0], Long.parseLong(words[1]), words[2]);
				openElements[currentLevel].contents.add(element);
				currentLevel = elementLevel;
				openElements[currentLevel] = element;

			} else {

				openElements[currentLevel].time = Long.parseLong(words[1]) - openElements[currentLevel].time;
				
				if(currentLevel>0)
					currentLevel--;
			}
		}
		
		br.close();
		new PrintStream(timeFile).println(new GsonBuilder().setPrettyPrinting().create().toJson(baseElement));
		return baseElement;
	}

	private static int getIndentLevel(String line) {

		int i = 0;
		char c = line.charAt(i);
		while (c == ' ') {
			i++;
			c = line.charAt(i);
		}
		
		if(i == -1)
			System.out.println("WRONG!!!");
		
		return i;
	}

	public static void log(String who, Object msg) {

		long when = System.nanoTime();

		for (PrintStream out : outters) {
			out.println(when + " - " + who + " - " + msg);
		}

	}

}
