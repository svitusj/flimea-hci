package usj.svit.helper.perfomance.log;

import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

public class NamedStopwatch {

	Stopwatch stopwatch;
	String name;
	ArrayList<Lap> laps;
	public long elapsedFromLastSplit;
	
	public static NamedStopwatch createWatchStarted(String name) {
		return new NamedStopwatch(name);
	}
	
	protected NamedStopwatch(String name){
		
		this.name = name;
		laps = new ArrayList<Lap>();
		elapsedFromLastSplit = 0;
		stopwatch = Stopwatch.createStarted();
	}
	
	public void start() {
		stopwatch.start();
	}
	
	public void stop() {
		stopwatch.stop();
	}
	
	public void split(String description) {
		if(!isRunning())
			return;
		long elapsed = stopwatch.elapsed(TimeUnit.NANOSECONDS);
		laps.add(new Lap(elapsed - elapsedFromLastSplit, description));
		elapsedFromLastSplit = elapsed;
	}
	
	public boolean isRunning() {
		return stopwatch.isRunning();
	}
	public long elapsedNanos() {
		return stopwatch.elapsed(TimeUnit.NANOSECONDS);
	}
	
	public String toString() {
		
		return name+": "+stopwatch.toString() +"\r\n Laps: "+laps;
	}
	
	class Lap {
		
		Duration d;
		
		long elapsed;
		String description;
		
		Lap(long elapsed) {
			this.elapsed = elapsed;
			
		}
		
		Lap(long elapsed,String description){
			this(elapsed);
			this.description = description;
		}
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return description + " - "+ elapsed /1000000 +" ms";
		}
		
	}
}
