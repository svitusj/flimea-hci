package usj.svit.helper.perfomance.log;

import java.util.ArrayList;

public class TimedElement {

	String name;
	public long time;
	String msg;
	ArrayList<TimedElement> contents;
	
	public TimedElement(String name, long time, String msg) {
		super();
		this.name = name;
		this.time = time;
		this.msg = msg;
		contents = new ArrayList<TimedElement>();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Name: "+this.name+" - ");
		sb.append("MSG: "+this.msg+" - ");
		sb.append("Time: "+this.time+" ms\r\n");
		if(contents.size()>0)
		sb.append(" "+contents);
		
		return sb.toString();
	}
}
