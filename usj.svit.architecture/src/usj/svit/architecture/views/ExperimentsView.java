package usj.svit.architecture.views;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;


/**
 * Base class to provide a framework for different experiments
 * It enables browsing of experiments and its descriptions and launching them.
 * @author svit
 *
 */
public abstract class ExperimentsView extends ViewPart {
	
	private List<ExperimentViewEntry> experiments;
	private Text experimentLabel;
	private Combo experimentCombo;
	
	private Composite panel;

	
	public abstract List<ExperimentViewEntry> populateWithExperiments();
	
	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		GridData gridData;
		
		experiments = populateWithExperiments();
		
		panel = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		panel.setLayout(layout);
		
		
		//combo for experiemnts;
		experimentCombo = new Combo(panel, SWT.NONE);
		experimentCombo.setItems(getExperimenttitles());
		experimentCombo.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				experimentLabel.setText(experiments.get(experimentCombo.getSelectionIndex()).getDescription());
				panel.layout();
			}
		});
		gridData = new GridData();
		gridData.widthHint = 300;
		gridData.verticalAlignment = SWT.FILL;
		experimentCombo.setLayoutData(gridData);
		
		
		//Launch button
		Button b = new Button(panel, SWT.NONE);
		b.setText("Execute");
		b.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				experiments.get(experimentCombo.getSelectionIndex()).run();
			}
		});	
		gridData = new GridData();
		gridData.horizontalIndent = 10;
		gridData.widthHint = 100;
		gridData.verticalAlignment = SWT.FILL;
		b.setLayoutData(gridData);
		
		//Experiment label
		experimentLabel = new Text(panel, SWT.NONE);
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessVerticalSpace = true;
		experimentLabel.setLayoutData(gridData);
	}
	
	private String[] getExperimenttitles() {
		
		String[] titles = new String[experiments.size()];
		for(int i = 0; i < experiments.size(); i++)
			titles[i] = experiments.get(i).getTitle();
		
		return titles;
	}
	
	public void setFocus() {

	}
}
