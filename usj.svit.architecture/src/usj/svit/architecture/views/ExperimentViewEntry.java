package usj.svit.architecture.views;

public abstract class ExperimentViewEntry {
	String title = "";
	String description = "";
	
	
	public abstract void run();
	public String getTitle(){
		return this.getClass().getSimpleName();
	}
	public String getDescription(){
		return this.getClass().getCanonicalName();
	}
	
	public ExperimentViewEntry(){
		
	}
	
	public ExperimentViewEntry(String newTitle, String newDescription)
	{
		title = newTitle;
		description = newDescription;
	}
	
	public void setTitle(String newTitle)
	{
		title = newTitle;
	}
	public void setDescription(String newDescription)
	{
		description = newDescription;
	}
	
}
