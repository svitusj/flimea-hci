package usj.svit.architecture.approach;


import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.individual.impl.Query;
import usj.svit.architecture.oracle.ITestCase;
import usj.svit.architecture.results.IEMFResult;


/**
 * To be considered an approach and run through the experiment framework you need to implement this approach.
 * 
 * @author usuario
 *
 * @param <T>
 * @param <R>
 */
public interface IApproach<T extends ITestCase, R extends IIndividualEMF> {
	
	public IEMFResult<R> run(T testCase);
	public String getID();
	public String getDescription();
	public Query getLastQuery();
	public int getLastGenerations();
}
