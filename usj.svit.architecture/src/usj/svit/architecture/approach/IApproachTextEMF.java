package usj.svit.architecture.approach;

import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.oracle.TestCaseTextEMF;
import usj.svit.architecture.results.TCPerformance;
import usj.svit.helper.workspace.helper.ProjectHelper;

public interface IApproachTextEMF extends IApproach<TestCaseTextEMF, IIndividualEMF> {

	TCPerformance getTCPerformance();

	/**
	 * Called at the end of the execution to store any extra data needed by the approach
	 * @param helper
	 * @param baseName
	 */
	void persistWathever(ProjectHelper helper, String baseName);

}
