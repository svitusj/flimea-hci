package usj.svit.architecture.individual;

public interface IText {

	// Text storage
	public String[] getText();
	public void setText(String[] texts);
	
	public String[] getTags();
	public void setTags(String[] texts);

}
