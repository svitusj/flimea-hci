package usj.svit.architecture.individual.impl.eof;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

public class IndexBuilder extends BaseIndexBuilder{
	
	public IndexBuilder(EObject root) {
		super(root);
	}

	protected String getReferenceText(EObject model, EReference reference) {
		return "";
	}

	protected String getAttributeValue(EObject model, EAttribute attribute) {
		
		Object value = model.eGet(attribute);
		return "" + value;
	}

	protected String getMMText(EObject object) {
		
		if(object instanceof EReference) {
			return ((EReference)object).getName();
		}
		else if(object instanceof EStructuralFeature){
			return ((EStructuralFeature)object).getName();
		}
		else{
			String [] terms = object.eClass().getInstanceClassName().split("\\.");
			if(terms.length==0)
				return "";
			return terms[terms.length-1];
		}	
	}
	

}
