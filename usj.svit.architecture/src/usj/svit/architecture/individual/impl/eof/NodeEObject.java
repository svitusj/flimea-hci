package usj.svit.architecture.individual.impl.eof;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;

public class NodeEObject extends BaseNode{
	
	ArrayList<NodeEAttribute> attributes;
	ArrayList<NodeEReference> references;
	
	
	public NodeEObject(String id, String textMM, EObject eobject) {
		super(id, textMM, eobject, "");
		references = new ArrayList<NodeEReference>();
		attributes = new ArrayList<NodeEAttribute>();
	}
	
	public void addAttribute(NodeEAttribute attribute) {
		attributes.add(attribute);
	}

	public ArrayList<NodeEAttribute> getAttributes() {
		return attributes;
	}

	public void removeAttribute(int i) {
		attributes.remove(i);		
	}

	public void addReference(NodeEReference reference) {
		references.add(reference);
	}

	public ArrayList<NodeEReference> getReferences() {
		return references;
	}

	public void removeReference(int i) {
		references.remove(i);		
	}

}
