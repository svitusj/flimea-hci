package usj.svit.architecture.individual.impl.eof;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;

public class NodeEReference extends BaseNode{
	
	private boolean containment;
	private ArrayList<NodeEObject> contents;
	
	public NodeEReference(String id, String textMM, EObject eobject, boolean containment) {
		super(id, textMM, eobject, "");
		this.containment = containment;
		contents = new ArrayList<NodeEObject>();	
	}
	
	public NodeEReference(String id, String textMM, EObject eobject, String value) {
		this(id,textMM,eobject,false);
		this.originalText = value;
		
	}

	public void addContent(NodeEObject child) {
		contents.add(child);
	}

	public boolean isContainment() {
		return containment;
	}

	public ArrayList<NodeEObject> getContents() {
		return contents;
	}

	
	
	
}
