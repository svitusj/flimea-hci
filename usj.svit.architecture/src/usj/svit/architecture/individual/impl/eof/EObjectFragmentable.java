package usj.svit.architecture.individual.impl.eof;

import java.util.HashMap;

import org.eclipse.emf.ecore.EObject;

import usj.svit.architecture.oracle.BaseOracleEMFText.Granularity;

public class EObjectFragmentable {

	private EObject originalEObject;
	private Index index;

	private static HashMap<CacheKey, EObjectFragmentable> cacheEOF;
	private static int llamadas = 0;

	static {
		cacheEOF = new HashMap<CacheKey, EObjectFragmentable>();
	}

	private EObjectFragmentable(EObject eobject, Granularity granularity) {
		this.originalEObject = eobject;
		this.index = new IndexBuilder(eobject).generateFragmentIndex(granularity);
	}

	public static EObjectFragmentable createOrFindEOF(EObject eobject, Granularity granularity) {
		CacheKey key = new CacheKey(eobject, granularity);
		if (cacheEOF.containsKey(key))
			return cacheEOF.get(key);

		EObjectFragmentable instance = new EObjectFragmentable(eobject, granularity);
		cacheEOF.put(key, instance);
		llamadas++;

		return instance;
	}

	public static EObjectFragmentable createOrFindEOF(EObject eobject) {
		return createOrFindEOF(eobject, Granularity.TOTAL);
	}

	public EObject getEObject() {
		return originalEObject;
	}

	public void setEObject(EObject eobject) {
		this.originalEObject = eobject;
	}

	public Index getIndex() {
		return index;
	}

	public void setIndex(Index index) {
		this.index = index;
	}

	public int getSize() {
		return index.getFilteredSize();
	}

	public String getTextForPosition(int position) {
		return index.getTextForPosition(position);
	}

	public String getMetamodelTextForPosition(int position) {
		return index.getMetamodelTextForPosition(position);
	}

	static class CacheKey {
		EObject eobject;
		Granularity granularity;

		public CacheKey(EObject eobject, Granularity granularity) {
			this.eobject = eobject;
			this.granularity = granularity;
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof CacheKey))
				return super.equals(obj);
			CacheKey other = (CacheKey) obj;
			return (other.granularity == this.granularity && other.eobject == this.eobject);

		}

		@Override
		public int hashCode() {
			return this.eobject.hashCode();
		}
	}

	public String[] getFilteredTextForPosition(int position) {

		return index.getFilteredTextForPosition(position);
	}

}
