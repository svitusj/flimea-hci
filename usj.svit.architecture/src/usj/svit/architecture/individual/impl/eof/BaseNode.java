package usj.svit.architecture.individual.impl.eof;

import org.eclipse.emf.ecore.EObject;

import usj.svit.architecture.individual.IText;

public abstract class BaseNode implements IText {
	
	private static final String SEPARATOR = " ";

	protected String id;
	protected String originalMMtext;
	protected transient EObject eobject;
	protected String originalText;
	
	protected transient String[] textP;
	protected transient String[] tagsP;

	public BaseNode(String id, String textMM, EObject eobject,String textM) {
		this.id=id;
		this.originalMMtext = textMM;
		this.eobject = eobject;
		this.originalText = textM;
	}	
	
	public void resetTextForProcessing(){
		this.textP = new String[1];
		this.textP[0] = originalMMtext + SEPARATOR + originalText;
	}
	
	public String getId() {
		return id;
	}

	public String getTextMetamodel() {
		return originalMMtext;
	}

	public EObject getEobject() {
		return eobject;
	}

	public String getOriginalText() {
		return originalText;
	}

	public void appendToOriginalMMText(String text) {
		this.originalMMtext += SEPARATOR+text;
	}
	
	public void appendToOriginalText(String text) {
		this.originalText += SEPARATOR+text;
	}

	@Override
	public String[] getText() {
		
		return textP;
	}

	@Override
	public void setText(String[] texts) {
		this.textP = texts;
		
	}

	@Override
	public String[] getTags() {
		// TODO Auto-generated method stub
		return tagsP;
	}

	@Override
	public void setTags(String[] texts) {
		this.tagsP = texts;
		
	}
}
