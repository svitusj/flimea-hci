package usj.svit.architecture.individual.impl.eof;

import java.util.ArrayList;

import usj.svit.architecture.oracle.BaseOracleEMFText.Granularity;

public class Index {

	NodeEObject root;
	int size;
	ArrayList<BaseNode> filteredElements;
	private Granularity granularity;
	
	
	private ArrayList<NodeEObject> allEObjects = new ArrayList<NodeEObject>();
	private ArrayList<NodeEAttribute> allEAttributes = new ArrayList<NodeEAttribute>();
	private ArrayList<NodeEReference> allEReferences = new ArrayList<NodeEReference>();

	private Index(Granularity granularity) {
		allEObjects = new ArrayList<NodeEObject>();
		allEAttributes = new ArrayList<NodeEAttribute>();
		allEReferences = new ArrayList<NodeEReference>();
		filteredElements = new ArrayList<BaseNode>();
		this.granularity = granularity;
	}
	
	public Index(NodeEObject eObjectFragmentabletElement,Granularity granularity) {
		this(granularity);
		this.root = eObjectFragmentabletElement;
		processNextEObject(this.root);
		computeSize();
		initTextsForProcessing();
	}

	private void initTextsForProcessing() {
		for(BaseNode node: filteredElements) {
			node.resetTextForProcessing();
		}
		
	}

	private void computeSize() {
		this.size = allEObjects.size()+allEAttributes.size()+allEReferences.size();
	}

	public int getUnfilteredSize() {
		return size;
	}
	
	public int getFilteredSize(){
		return filteredElements.size();
	}
	public BaseNode getRoot() {
		return root;
	}

	public ArrayList<NodeEObject> getAllEObjects() {
		return allEObjects;
	}

	public ArrayList<NodeEAttribute> getAllEAttributes() {
		return allEAttributes;
	}

	public ArrayList<NodeEReference> getAllEReferences() {
		return allEReferences;
	}

	private void processNextEObject(NodeEObject node){
		
		allEObjects.add(node);
		filteredElements.add(node);
		
		for(NodeEAttribute attribute : node.getAttributes()) {
			allEAttributes.add(attribute);
			
			if (Granularity.TOTAL.ordinal() < granularity.ordinal())
			{
				node.appendToOriginalText(attribute.getOriginalText());
				node.appendToOriginalMMText(attribute.getTextMetamodel());
			}
			else
			{
				filteredElements.add(attribute);
			}
		}
		for(NodeEReference reference : node.getReferences()){
			allEReferences.add(reference);
			
			if (Granularity.DropAttributes.ordinal() < granularity.ordinal())
			{
				node.appendToOriginalText(reference.getOriginalText());
				node.appendToOriginalMMText(reference.getTextMetamodel());
			}
			else
			{
				filteredElements.add(reference);
			}
			
			if(reference.isContainment()) {
				
				for(NodeEObject child : reference.getContents()) {
					processNextEObject(child);
				}	
			}	
		}
	}

	public String getTextForPosition(int position) {	
		return filteredElements.get(position).getOriginalText();
	}

	public String getMetamodelTextForPosition(int position) {
		return filteredElements.get(position).getTextMetamodel();
	}
	
	public String[] getFilteredTextForPosition(int position) {
		
		return filteredElements.get(position).getText();
	}

	public ArrayList<BaseNode> getFilteredElements()
	{
		return filteredElements;
	}

	public BaseNode getNodeForPosition(int position){
		return filteredElements.get(position);
	}
	
	public NodeEObject getParentEObjectFor(BaseNode searchAttribute) {
		
		for(NodeEObject object : allEObjects) {
			
			for(BaseNode attribute : object.attributes) {
				
				if(attribute == searchAttribute)
					return object;
				
			}
			
		}
		
		return null;
	}

	public ArrayList<Integer> getIndexesForElementsUnder(NodeEObject root) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		
		addIndexes(root,ids);
		
		return ids;
	}

	private void addIndexes(NodeEObject root, ArrayList<Integer> ids) {
		ids.add(new Integer(root.id));
		
		for(BaseNode attribute : root.attributes) {
			ids.add(new Integer(attribute.getId()));
		}
		
		for(NodeEReference reference : root.references) {
			ids.add(new Integer(reference.getId()));
			
			if(reference.isContainment()) {
				
				for(NodeEObject child : reference.getContents()){
					
					addIndexes(child, ids);
					
				}	
			}
		}
	}
	
}
