package usj.svit.architecture.individual.impl.eof;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import usj.svit.architecture.oracle.BaseOracleEMFText.Granularity;

public abstract class BaseIndexBuilder {

	int id=1;
	EObject root;
	private static Gson gson;
	
	
	public BaseIndexBuilder(EObject root) {
		if(gson == null)
			gson = new GsonBuilder().setPrettyPrinting().create();
		this.root= root;
	}
	
	public Index generateFragmentIndex(Granularity granularity) {	
		id=1;
		NodeEObject rootElement = generateElementJSON(root);
		Index index = new Index(rootElement,granularity);
		return index;
	}
	
	public String getJSON() {
		return gson.toJson(generateFragmentIndex(Granularity.TOTAL));
	}
	
	private NodeEObject generateElementJSON(EObject model) {

		// if(Logger.enabled) Logger.log(getClass().getCanonicalName(), tag+"EOBject:"+model);
		NodeEObject node = new NodeEObject(id+"", getMMText(model), model);
		id++;
		

		for (EAttribute attribute : model.eClass().getEAllAttributes()) {
			// if(Logger.enabled) Logger.log(getClass().getCanonicalName(), tag+"EAttribute: "+attribute);
			node.addAttribute(new NodeEAttribute(id + "",getMMText(attribute), attribute, getAttributeValue(model, attribute)));
			id++;
		}

		for (EReference reference : model.eClass().getEAllReferences()) {
			if (!reference.isContainment()) {
				
				node.addReference(new NodeEReference(id + "",getMMText(reference),reference,getReferenceText(model,reference)));
				id++;
			}
			else if(reference.isContainment()) {
				NodeEReference ref = new NodeEReference(id+"", getMMText(reference), reference, true);
				id++;
				
				if(reference.isMany() && model.eIsSet(reference)) {
					for(EObject child : (EList<EObject>)model.eGet(reference)) {
						ref.addContent(generateElementJSON(child));
					}
				}
				else if (model.eIsSet(reference)){
					EObject child = (EObject) model.eGet(reference);
					ref.addContent(generateElementJSON(child));
				}
				node.addReference(ref);
				
				
			}
		}

		return node;

	}

	protected abstract String getMMText(EObject model);

	protected abstract String getAttributeValue(EObject model, EAttribute attribute);

	protected abstract String getReferenceText(EObject model, EReference reference);
}
