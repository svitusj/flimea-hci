package usj.svit.architecture.individual.impl;

import usj.svit.architecture.individual.IText;

public class Query implements IText {

	private String originalQuery;
	private String[] text;
	private String[] tags;
	
	public Query(String query) {
		text = new String[] {query};
		originalQuery = query;
	}
	public String[] getText() {
		return text;
	}
	@Override
	public void setText(String[] texts) {
		text = texts;
		
	}
	@Override
	public String[] getTags() {
		return tags;
	}
	@Override
	public void setTags(String[] texts) {
		this.tags = texts;
		
	}
	
	public String getOriginalText(){
		return this.originalQuery;
	}
	
}
