package usj.svit.architecture.individual.impl;

import java.math.BigInteger;
import java.util.BitSet;

public class Chromosome {
	
	private BitSet bitset;
	private int size;
	
	/**
	 * Creates a chromosome of the given size where all genes are set to true
	 * @param size
	 */
	public Chromosome(int size) {
		bitset = new BitSet(size);
		this.size=size;
		negate();
	}

	public Chromosome(String genes) {
		bitset = new BitSet(genes.length());
		this.size = genes.length();
	    for (int i = 0; i < genes.length(); i++) {
	        if (genes.charAt(i) == '1') {
	            bitset.set(i);
	        }
	    }
	}
	
	public Chromosome(BigInteger bi,int size) {
		bitset = new BitSet(size);
		this.size = size;
		 for (int i = 0; i < bi.bitLength(); i++) {
			 	bitset.set(i, bi.testBit(i));
		    }
	}
	
	public void negate() {
		bitset.flip(0,size);
		
	}
	
	public Chromosome clone() {
		Chromosome g = new Chromosome(size);
		g.bitset = (BitSet) this.bitset.clone();
		return g;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("");
        for( int i = 0; i<size;  i++ )
        {
        	if(bitset.get(i))
        		sb.append("1");
        	else
        		sb.append("0");
        }
        return sb.toString();
	}
	
	public int getSize()
	{
		return this.size;
	}

	public void flip(int i) {
		bitset.flip(i);
		
	}

	public int cardinality() {
		
		return bitset.cardinality();
	}

	public boolean get(int i) {
		
		return bitset.get(i);
	}

	public void and(Chromosome mask) {
		bitset.and(mask.bitset);
		
	}

	public void or(Chromosome mask) {
		bitset.or(mask.bitset);	
	}
	
	public void xor(Chromosome mask) {
		bitset.xor(mask.bitset);	
	}

	public void randomize() {
		
		for(int i=0;i<size;i++){
			if(Math.random()>0.5)
				bitset.set(i);
			else
				bitset.clear(i);
		}
		
	}
	public void set(int fromIndex, int toIndex, boolean value)
	{
		bitset.set(fromIndex, toIndex, value);
	}
	public void set(int index, boolean value)
	{
		bitset.set(index, value);
	}
}
