package usj.svit.architecture.individual.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;

public class IndividualTextEMF extends IndividualEMF implements IIndividualTextEMF {

	private String[] texts;
	private String[] tags;
	private HashMap<String,Integer> filteredTextsAndFrequencies;

	public IndividualTextEMF(Chromosome genes, EObjectFragmentable parent) {
		super(genes, parent);
		texts = new String[1];
		tags = new String[0];
		filteredTextsAndFrequencies=new HashMap<String,Integer>();
	}

	public IndividualTextEMF(EObjectFragmentable parent) {
		super(parent);
		texts = new String[0];
		tags = new String[0];
		filteredTextsAndFrequencies=new HashMap<String,Integer>();
	}

	public String toString() {

		StringBuilder sb = new StringBuilder("Individual EMF Text Binary\r\n");
		sb.append(genes.toString() + "\r\n");
		sb.append("Texts (" + texts.length + "): " + Arrays.toString(this.texts) + "\r\n");
		sb.append("Tags (" + tags.length + "): " + Arrays.toString(this.tags) + "\r\n");
		sb.append("Fitness: " + this.getFitness() + "\r\n");
		return sb.toString();

	}

	@Override
	public String[] getText() {
		return texts;
	}

	@Override
	public void setText(String[] texts) {
		this.texts = texts;
	}

	@Override
	public String[] getTags() {
		return tags;
	}

	@Override
	public void setTags(String[] texts) {
		this.tags = texts;

	}
	
	public HashMap<String,Integer> getFilteredTextsAndFrequencies()
	{
		return filteredTextsAndFrequencies;
	}

	@Override
	public void updateTextsAfterRecombinations() {

		ArrayList<String> textoNuevo = new ArrayList<String>();
		for (int i = 0; i < getParentModelSize(); i++) {

			if (getGen(i))
				textoNuevo.addAll(Arrays.asList(parent.getFilteredTextForPosition(i)));
		}
		texts = textoNuevo.toArray(new String[0]);
	}
	
	public void updateFilteredTextsAndFrequenciesAfterRecombinations() {

		ArrayList<String> textoNuevo = new ArrayList<String>();
		for (int i = 0; i < getParentModelSize(); i++) {

			if (getGen(i))
				textoNuevo.addAll(Arrays.asList(parent.getFilteredTextForPosition(i)));
		}
		texts = textoNuevo.toArray(new String[0]);
		filteredTextsAndFrequencies.clear();
		getTermsAndFrequencies(texts);
		
	}

	public String getOriginalText() {

		ArrayList<String> originalText = new ArrayList<String>();
		for (int i = 0; i < getParentModelSize(); i++) {

			if (getGen(i)) {
				originalText.addAll(Arrays.asList(parent.getTextForPosition(i)));
				originalText.addAll(Arrays.asList(parent.getMetamodelTextForPosition(i)));
			}
		}
		return originalText.toString();
	}

	@Override
	/**
	 * Generates a copy of itself independent that can be further modified
	 * fitness and texts are not copied so this call should be followed with updateTextAfterRecombination and assessment of fitness
	 */
	public IIndividualTextEMF createCopy() {
		return new IndividualTextEMF(getCopyOfGenes(), getParent());
	}

	private void getTermsAndFrequencies(String [] texts)
	{
		for(String term:texts)
		{
			if(filteredTextsAndFrequencies.containsKey(term)) filteredTextsAndFrequencies.replace(term, filteredTextsAndFrequencies.get(term)+1);
			else 
			{
				filteredTextsAndFrequencies.put(term,1);
			}
			
		
		}
	}
}
