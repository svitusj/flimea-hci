package usj.svit.architecture.individual.impl;

import java.util.Arrays;

import usj.svit.architecture.individual.IIndividualText;

public class IndividualText implements IIndividualText {

	String[] texts;
	String[] tags;
	private double fitness;

	public IndividualText(String text) {
		texts = new String[] { text };
	}

	@Override
	public String[] getText() {
		return texts;
	}

	@Override
	public void setText(String[] texts) {
		this.texts = texts;
	}

	@Override
	public String[] getTags() {
		return tags;
	}

	@Override
	public void setTags(String[] texts) {
		this.tags = texts;

	}

	@Override
	public double getFitness() {
		return this.fitness;
	}

	@Override
	public void setFitness(double fitnessValue) {
		this.fitness = fitnessValue;

	}

	public String toString() {

		StringBuilder sb = new StringBuilder("Text Individual\r\n");
		sb.append("\tTexts: " + Arrays.toString(this.texts) + "\r\n");
		sb.append("\tTags: " + Arrays.toString(this.tags) + "\r\n");
		sb.append("\tFitness: " + this.getFitness());

		return sb.toString();

	}

}
