package usj.svit.architecture.individual.impl;

import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;
import usj.svit.architecture.utils.MathUtils;

public class IndividualEMF implements IIndividualEMF {

	protected Chromosome genes;
	protected double fitness;
	protected EObjectFragmentable parent;

	public IndividualEMF(Chromosome genes, EObjectFragmentable parent) {
		this.genes = genes;
		this.fitness=-1;
		this.parent = parent;
	}
	
	public IndividualEMF(EObjectFragmentable parent) {
		this(new Chromosome(parent.getSize()),parent);
	}
	
	public Chromosome getCopyOfGenes() {
		return genes.clone();
	}

	public double getFitness() {
		return fitness;
	}

	public void setFitness(double fitness) {
		this.fitness = fitness;
	}

	public String toString() {

		StringBuilder sb = new StringBuilder("Individual EMF Text Binary\r\n");
		sb.append(genes.toString()+ "\r\n");
		sb.append("Fitness: " + this.getFitness()+"\r\n");
		return sb.toString();

	}
	
	public int getNumberOfPossitiveGenes() {
		return genes.cardinality();
	}
	
	public int getParentModelSize(){
		return parent.getSize();
	}
	
	@Override
	public EObjectFragmentable getParent() {
		return parent;
	}

	@Override
	public boolean getGen(int id) {
		
		return genes.get(id);
	}

	@Override
	public void flipGen(int id) {
		genes.flip(id);
		
	}

	@Override
	public void randomizeGenes() {
		genes = MathUtils.generateRandomCromosomeEquallyDistributed(genes.getSize());
	}

	@Override
	public Chromosome getGenes() {
		return genes;
	}
}
