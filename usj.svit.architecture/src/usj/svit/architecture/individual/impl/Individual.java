package usj.svit.architecture.individual.impl;

import usj.svit.architecture.individual.IIndividual;

public class Individual implements IIndividual{

	private double fitness=0;

	@Override
	public double getFitness() {
		return fitness;
	}

	@Override
	public void setFitness(double fitnessValue) {
		this.fitness = fitnessValue;
	}
}
