package usj.svit.architecture.individual;

public interface IIndividual {

	public double getFitness();
	public void setFitness(double fitnessValue);

	
}
