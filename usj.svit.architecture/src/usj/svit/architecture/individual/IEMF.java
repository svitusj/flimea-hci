package usj.svit.architecture.individual;

import usj.svit.architecture.individual.impl.Chromosome;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;

public interface IEMF {

	public Chromosome getGenes();
//	public void setGenes(Genes genes);
	public void randomizeGenes();
	public boolean getGen(int id);
	public void flipGen(int id);
	public Chromosome getCopyOfGenes();
	public int getNumberOfPossitiveGenes();
	public int getParentModelSize();
	public EObjectFragmentable getParent();
}
