package usj.svit.architecture.individual;

import java.util.HashMap;

public interface IIndividualTextEMF extends IIndividualText, IIndividualEMF {

	public void updateTextsAfterRecombinations();
	public void updateFilteredTextsAndFrequenciesAfterRecombinations();
	public HashMap<String,Integer> getFilteredTextsAndFrequencies();
	public IIndividualTextEMF createCopy();
}
