package usj.svit.architecture.utils;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import usj.svit.architecture.individual.impl.Chromosome;

public class MathUtils {

	static Random rng = new Random(System.currentTimeMillis());
	
	
	public static Chromosome generateRandomCromosomeEquallyDistributed(int size){
		
		BigInteger bi = new BigInteger(size, rng);
		
		return new Chromosome(bi,size);
			
	}
	
	/** 
	 * Generates a Chromosome of the given size where each gene is randomly set to true or false
	 * @param size Number of genes required
	 * @return a Chromosome object whose cardinality will be around size/2
	 */
	public static Chromosome generateRandomChromosomeMask(int size) {
		
		Chromosome mask = new Chromosome(size);
		
		for (int i = 0; i < mask.getSize(); i++) {
			if (rng.nextFloat() < 0.5d) {
				mask.flip(i);
			}
		}
		
		return mask;
	}
	
	/** 
	 * Generates a Genes object of the given size and the given cardinality, the elements set to true will be selected randomly among the space available
	 * @param size Number of genes required
	 * @param cardinality Number of genes that will be true
	 * @return a genes object whose cardinality will be around size/2
	 */
	public static Chromosome generateRandomChromosome(int size, int cardinality) {
		
		Chromosome mask = new Chromosome(size);
		mask.negate();
		
		Set<Integer> randomIndexes = getRandomIntegers(0,size-1,cardinality);
		
		for(Integer index : randomIndexes){
			mask.flip(index);
		}
		
		return mask;
	}
	/**
	 * Generates a set of random integers whose values are inside the parameters provided (included).
	 * @param from lower range
	 * @param to upper range
	 * @param amount number of integers to be generated
	 * @return the set of random integers generated
	 */
	public static Set<Integer> getRandomIntegers(int from, int to, int amount) {
		
		Set<Integer> set = new HashSet<Integer>();

		while (set.size()<amount) {
		    set.add(rng.nextInt(to+1));
		}
		
		return set;
	}

	public static double getRandomDouble() {
		return rng.nextDouble();
	}

	/**
	 * Random value from 0 (inclusive) to the size provided (exclusive)
	 * @param size
	 * @return
	 */
	public static int getRandomInt(int size) {
		return rng.nextInt(size);
	}

	public static float getRandomFloat() {
		return rng.nextFloat();
	}
}
