package usj.svit.architecture.oracle;

import java.util.List;

public interface IOracle<T extends ITestCase> {

	public List<T> getTestCases();

	public String getID();

	public String getDescription();
}
