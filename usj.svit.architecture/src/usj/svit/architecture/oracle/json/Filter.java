package usj.svit.architecture.oracle.json;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Filter <T> {
	
	public static enum Operation {EQUALS,GREATER,LESS,RANDOM};
	public static enum Attributes {id,reference,owner,creationDate,numHumanElements,numGeneratedElements,idQuery,idSearchSpace,idSolution};
	
	Class<?> T;
	Attributes attribute;
	Object value;
	Operation operation;
	
	
	public Filter (Class<?> TClass, Attributes attribute, Object value, Operation operation){
		T = TClass;
		this.attribute = attribute;
		this.value = value;
		this.operation = operation;
	}

	public Class<?> getFilteredClass()
	{
		return T;
	}
	
	public List<T> fulfillFilter(List<T> objects)
	{
		
		for (Field field: T.getDeclaredFields())
		{
			if(field.getName().equals(attribute.name()))
			{
				switch(operation){
				case EQUALS:
					return valueIsEqual(field, objects);
				case GREATER:
					return valueIsGreater(field,objects);
				case LESS:
					return valueIsLess(field,objects);
				case RANDOM:
					return selectRandomObjects(field,objects);
				}
				
			}
		}
		return new ArrayList<T>();
	}


	private List<T> selectRandomObjects(Field field, List<T> objects) 
	{
		List<T> filteredObjects = new ArrayList<T>();
		List<T> copyObjects = new ArrayList<T>(objects);
		
		if (value instanceof Integer)
		{
			Random r = new Random();
			int random;
			
			if ((int)value>objects.size()) return objects; 
			else {
				while (filteredObjects.size() < (int)value)
				{
					random = r.nextInt(copyObjects.size()-1) + 1;
					filteredObjects.add(copyObjects.get(random));
					copyObjects.remove(random);
		        }
			}
		}
		
		return filteredObjects;
	}

	private List<T> valueIsLess(Field field, List<T> objects) 
	{
		List<T> filteredObjects = new ArrayList<T>();
		
		if (value instanceof String)
		{
			value = Integer.parseInt((String) value);
		}
		for (T object: objects)
		{
			try {
				Object fieldValue = field.get(object);
				if(fieldValue instanceof String)
				{
					fieldValue = Integer.parseInt((String) fieldValue);
				}	
				if ((int)fieldValue < (int)value)
				{
					filteredObjects.add(object);
				}
			}  catch (NumberFormatException e) {
				//Do nothing!
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return filteredObjects;	
	}

	private List<T> valueIsGreater(Field field, List<T> objects) {
		List<T> filteredObjects = new ArrayList<T>();
		
		if (value instanceof String)
		{
			value = Integer.parseInt((String) value);
		}
		for (T object: objects)
		{
			try {
				Object fieldValue = field.get(object);
				if(fieldValue instanceof String)
				{
					fieldValue = Integer.parseInt((String) fieldValue);
				}	
				if ((int)fieldValue > (int)value)
				{
					filteredObjects.add(object);
				}
			}  catch (NumberFormatException e) {
				//Do nothing!
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return filteredObjects;	
	}

	private List<T> valueIsEqual(Field field, List<T> objects) 
	{
		List<T> filteredObjects = new ArrayList<T>();
		
		for (T object: objects)
		{
			try {
				if (field.get(object).equals(value))
				{
					filteredObjects.add(object);
				}
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return filteredObjects;		
		
	}
}
