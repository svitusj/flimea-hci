package usj.svit.architecture.oracle.json;

public class JsonSearchSpace {
	
	String id;
	String file;
	String fileIndex;
	String owner;
	String creationDate;
	int numHumanElements;
	int numGeneratedElements;
	
	public JsonSearchSpace(String id, String file, String fileIndex, String owner, String creationDate,
			int numHumanElements, int numRandomElements) {
		super();
		this.id = id;
		this.file = file;
		this.fileIndex = fileIndex;
		this.owner = owner;
		this.creationDate = creationDate;
		this.numHumanElements = numHumanElements;
		this.numGeneratedElements = numRandomElements;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public void setFileIndex(String fileIndex) {
		this.fileIndex = fileIndex;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public void setNumHumanElements(int numHumanElements) {
		this.numHumanElements = numHumanElements;
	}
	public void setNumRandomElements(int numRandomElements) {
		this.numGeneratedElements = numRandomElements;
	}
	public String getId() {
		return id;
	}
	public String getFile(){
		return file;
	}
	public String getFileIndex() {
		return fileIndex;
	}
	public String getOwner() {
		return owner;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public int getNumHumanElements() {
		return numHumanElements;
	}
	public int getNumRandomElements() {
		return numGeneratedElements;
	}
}
