package usj.svit.architecture.oracle.json;

public class JsonQuery {
	
	String id;
	String reference;
	String owner;
	String description;
	
	public JsonQuery(String id, String reference, String owner, String description) {
		this.id = id;
		this.reference = reference;
		this.owner = owner;
		this.description = description;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getId() {
		return id;
	}
	public String getReference() {
		return reference;
	}
	public String getOwner() {
		return owner;
	}
	public String getDescription() {
		return description;
	}
}
