package usj.svit.architecture.oracle.json;

import java.util.ArrayList;
import java.util.List;

public class JsonOracle {

	List<JsonQuery> queries;
	List<JsonSearchSpace> searchSpaces;
	List<JsonSolution> solutions;
	List<JsonTestCase> testCases;

	
	public JsonOracle(){
		queries = new ArrayList<JsonQuery>();
		searchSpaces = new ArrayList<JsonSearchSpace>();
		solutions = new ArrayList<JsonSolution>();
		testCases = new ArrayList<JsonTestCase>();
	}
	
	public void addQuery(JsonQuery query) {
		queries.add(query);
	}

	public void setQueries(ArrayList<JsonQuery> queries){
		this.queries = queries;
	}
	
	public void addSearchSpace(JsonSearchSpace searchSpace) {
		searchSpaces.add(searchSpace);
	}

	public void addSolution(JsonSolution solution) {
		solutions.add(solution);
	}

	public void addTestCase(JsonTestCase testCase) {
		testCases.add(testCase);
	}

	public List<JsonQuery> getQueries() {
		return queries;
	}

	public List<JsonSearchSpace> getSearchSpaces() {
		return searchSpaces;
	}

	public List<JsonSolution> getSolutions() {
		return solutions;
	}

	public List<JsonTestCase> getTestCases() {
		return testCases;
	}

	
	
	public JsonQuery findQuery(String idQuery) {
		for (JsonQuery query : queries) {
			if (query.getId().equals(idQuery)) {
				return query;
			}
		}
		return null;
	}

	public JsonSearchSpace findSearchSpace(String idSearchSpace) {
		for (JsonSearchSpace searchSpace : searchSpaces) {
			if (searchSpace.getId().equals(idSearchSpace)) {
				return searchSpace;
			}
		}
		return null;
	}

	public JsonSolution findSolution(String idSolution) {
		for (JsonSolution solution : solutions) {
			
			if (solution.getId().equals(idSolution)) {
				return solution;
			}
		}
		return null;
	}
	
	public void fulfillFilters(ArrayList<Filter> filters) {
		if(filters != null)
		{
			for(Filter filter : filters)
			{
				if(filter.getFilteredClass().equals(JsonQuery.class))
				{
					queries = filter.fulfillFilter(queries);
				}
				else if(filter.getFilteredClass().equals(JsonSearchSpace.class))
				{
					searchSpaces = filter.fulfillFilter((List<?>) searchSpaces);
				}
				else if(filter.getFilteredClass().equals(JsonSolution.class))
				{
					solutions = filter.fulfillFilter(solutions);
				}
				else if(filter.getFilteredClass().equals(JsonTestCase.class))
				{
					testCases = filter.fulfillFilter(testCases);
				}
			}
		}
	}

	/**
	 * Find all the testCases related with a specific searchSpace
	 * @param idSearchSpace
	 * @return
	 */
	public List<JsonTestCase>findTestCasesForSearchSpace(String idSearchSpace){
		ArrayList<JsonTestCase> tcs = new ArrayList<JsonTestCase>();
		
		for(JsonTestCase tc : testCases){
			if(tc.getIdSearchSpace().equals(idSearchSpace))
				tcs.add(tc);
		}
		return tcs;
	}
	
	/**
	 * Find all the testCases related with a specific Query
	 * @param idSearchSpace
	 * @return 
	 * @return
	 */
	public ArrayList<JsonTestCase> findTestCasesForQuery(String idQuery) {
		ArrayList<JsonTestCase> tcs = new ArrayList<JsonTestCase>();
		
		for(JsonTestCase tc : testCases){
			if(tc.getIdQuery().equals(idQuery))
				tcs.add(tc);
		}
		return tcs;
		
		
	}

	/**
	 * Find all the testCases related with a specific Query and SearchSpace
	 * @param idQuery
	 * @param idSearchSpace
	 * @return
	 */
	public ArrayList<JsonTestCase> findTestCasesForQueryAndSearchSpace(String idQuery, String idSearchSpace) {
		
		ArrayList<JsonTestCase> tcs = new ArrayList<JsonTestCase>();
		
		for(JsonTestCase tc : testCases){
			if(tc.getIdQuery().equals(idQuery) && tc.getIdSearchSpace().equals(idSearchSpace))
				tcs.add(tc);
		}
		return tcs;
	}
	
	public int getSolutionSize(JsonTestCase testCase){
		int size=0;
		
		for(int i=0;i<testCase.getSolutionIndex().length();i++){
			if(testCase.getSolutionIndex().charAt(i)=='1')
				size++;
		}
		return size;
	}
	
	
	/**
	 * Find all the TC with the same size of the provided query
	 * @param idQuery 
	 * @return 
	 */
	public ArrayList<JsonTestCase> findAlternativesForQuery(JsonTestCase test) {
		
		int lookingForSize = getSolutionSize(test);
		
		ArrayList<JsonTestCase> solution = new ArrayList<JsonTestCase>();
		
		for(JsonTestCase tc : findTestCasesForSearchSpace(test.getIdSearchSpace())){
			
			if(getSolutionSize(tc) == lookingForSize)
				solution.add(tc);
		}
		
		return solution;
	}
}
