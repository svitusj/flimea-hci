package usj.svit.architecture.oracle.json;

import com.google.gson.annotations.Expose;

public class JsonTestCase {
	
	@Expose
	String id;
	@Expose
	String idQuery;
	@Expose
	String idSearchSpace;
	@Expose
	String idSolution;
	@Expose
	String solutionIndex;
	
	public JsonTestCase(String id, String idQuery, String idSearchSpace, String idSolution, String solutionIndex) {
		this.id = id;
		this.idQuery = idQuery;
		this.idSearchSpace = idSearchSpace;
		this.idSolution = idSolution;
		this.solutionIndex = solutionIndex;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdQuery() {
		return idQuery;
	}
	public void setIdQuery(String idQuery) {
		this.idQuery = idQuery;
	}
	public String getIdSearchSpace() {
		return idSearchSpace;
	}
	public void setIdSearchSpace(String idSearchSpace) {
		this.idSearchSpace = idSearchSpace;
	}
	public String getIdSolution() {
		return idSolution;
	}
	public void setIdSolution(String idSolution) {
		this.idSolution = idSolution;
	}
	public String getSolutionIndex() {
		return solutionIndex;
	}
	public void setSolutionIndex(String solutionIndex) {
		this.solutionIndex = solutionIndex;
	}
	
	
	

	
}
