package usj.svit.architecture.oracle.json;

public class JsonSolution {
	
	String id;
	String owner;
	String creationDate;
	
	
	public JsonSolution(String id, String owner, String creationDate) {
		this.id = id;
		this.owner = owner;
		this.creationDate = creationDate;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getId() {
		return id;
	}
	public String getOwner() {
		return owner;
	}
	public String getCreationDate() {
		return creationDate;
	}
	
}
