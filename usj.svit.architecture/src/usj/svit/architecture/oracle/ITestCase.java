package usj.svit.architecture.oracle;

public interface ITestCase {

	public Object getID();
	public Object getQuestion();
	public Object getSearchSpace();
	public Object getAnswer();
	
}
