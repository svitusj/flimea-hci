package usj.svit.architecture.oracle;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import com.google.gson.Gson;

import usj.svit.architecture.individual.impl.Chromosome;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.individual.impl.eof.BaseNode;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;
import usj.svit.architecture.oracle.json.Filter;
import usj.svit.architecture.oracle.json.JsonOracle;
import usj.svit.architecture.oracle.json.JsonQuery;
import usj.svit.architecture.oracle.json.JsonSearchSpace;
import usj.svit.architecture.oracle.json.JsonSolution;
import usj.svit.architecture.oracle.json.JsonTestCase;
import usj.svit.helper.perfomance.log.Logger;
import usj.svit.helper.workspace.helper.PluginHelper;

public abstract class BaseOracleEMFText implements IOracle<TestCaseTextEMF> {

	public static enum Granularity {
		TOTAL, DropAttributes, DropReferencesAndAttributes
	};

	private String pathOracle;
	private String pathModels;
	private String pluginID;
	private JsonOracle oracle;

	private Granularity granularity;

	List<TestCaseTextEMF> testCases;
	List<Filter> filters;
	private PluginHelper pluginHelper;

	public BaseOracleEMFText(String pathOracle, String pathModels, String pluginID) {
		this.pathOracle = pathOracle;
		this.pathModels = pathModels;
		this.pluginID = pluginID;
		this.granularity = Granularity.TOTAL;
		pluginHelper = new PluginHelper(pluginID);
	}

	public BaseOracleEMFText(String pathOracle, String pathModels, String pluginID, Granularity granularity) {
		this(pathOracle, pathModels, pluginID);
		this.granularity = granularity;
	}

	public BaseOracleEMFText(String pathOracle, String pathModels, String pluginID, List<Filter> filters) {
		this(pathOracle, pathModels, pluginID);
		this.filters = filters;
	}

	public BaseOracleEMFText(String pathOracle, String pathModels, String pluginID, List<Filter> filters,
			Granularity granularity) {
		this(pathOracle, pathModels, pluginID);
		this.granularity = granularity;
		this.filters = filters;
		
	}

	public List<TestCaseTextEMF> getTestCases() {
		if (testCases == null) {
			if (Logger.ENABLED_ORACLE)
				Logger.log(Logger.WHO_ORACLE, getClass().getCanonicalName(), Logger.ACTION_START, Logger.WHAT_ORACLE_LOAD);
			
			testCases = loadTestCases();
			
			if (Logger.ENABLED_ORACLE) {
				Logger.log(Logger.WHO_ORACLE, getClass().getCanonicalName(), Logger.ACTION_FINISH, Logger.WHAT_ORACLE_LOAD+" : "+testCases.size()+" TC Loaded");
			}
		}
		return testCases;
	}

	public JsonOracle getJSONOracle(){
		
		if(oracle==null) {
			String json = pluginHelper.loadFile(pathOracle);

			Gson gson = new Gson();
			oracle = gson.fromJson(json, JsonOracle.class);
		}
		
		return oracle;
	}
	
	private ArrayList<TestCaseTextEMF> loadTestCases() {
		ArrayList<TestCaseTextEMF> testCases = new ArrayList<TestCaseTextEMF>();

		getJSONOracle();

		// Filter Test Cases
		oracle.fulfillFilters((ArrayList<Filter>) filters);
		

		for (JsonTestCase jsonTestCase : oracle.getTestCases()) {
			TestCaseTextEMF testCase = loadTestCase(oracle, jsonTestCase);
			//

			if (testCase != null) {
				//testCase.getAnswer().updateTextsAfterRecombinations();
				testCases.add(testCase);
			}
		}
		return testCases;

	}

	private TestCaseTextEMF loadTestCase(JsonOracle oracle, JsonTestCase jsonTestCase) {

		String id = jsonTestCase.getId();

		// QUERY
		JsonQuery jsonQuery = oracle.findQuery(jsonTestCase.getIdQuery());
		if (jsonQuery == null)
			return null;

		String queryDescription = jsonQuery.getDescription();

		// SEARCH SPACE
		JsonSearchSpace jsonSearchSpace = oracle.findSearchSpace(jsonTestCase.getIdSearchSpace());
		if (jsonSearchSpace == null)
			return null;
		EObjectFragmentable searchSpace = loadSearchSpace(jsonSearchSpace);

		// INDEX ANSWER
		JsonSolution solution = oracle.findSolution(jsonTestCase.getIdSolution());
		if (solution == null)
			return null;

		IndividualTextEMF answer = loadAnswer(jsonTestCase.getSolutionIndex(), searchSpace);

		return (new TestCaseTextEMF(id, queryDescription, searchSpace, answer));

	}

	private IndividualTextEMF loadAnswer(String solution, EObjectFragmentable searchSpace) {

		String narrowSolution = "";
		ArrayList<BaseNode> mapNarrowElements = searchSpace.getIndex().getFilteredElements();
		for (int i = 0; i < mapNarrowElements.size(); i++) {
			int position = Integer.parseInt(mapNarrowElements.get(i).getId()) - 1;
			narrowSolution += solution.charAt(position);
		}
		return new IndividualTextEMF(new Chromosome(narrowSolution), searchSpace);
	}
	/**
	 * 
	 * @param jsonSearchSpace
	 * @return
	 */
	public EObjectFragmentable loadSearchSpace(JsonSearchSpace jsonSearchSpace) {

		EObject eobject = pluginHelper.loadModel(pathModels + jsonSearchSpace.getFile());
		return EObjectFragmentable.createOrFindEOF(eobject, granularity);

	}

	public String getDescription() {
		StringBuilder sb = new StringBuilder();

		sb.append(getID() + "\r\n");
		sb.append("Test Cases #:;" + this.getTestCases().size() + "; MODE:;" + this.granularity);
		return sb.toString();

	}

}
