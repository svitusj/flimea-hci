package usj.svit.architecture.oracle;

import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;
 
public class TestCaseTextEMF implements ITestCase {
		
	private String id;
	private String query;
	private EObjectFragmentable searchSpace;
	private IndividualTextEMF answer;
	
	
	public TestCaseTextEMF(String id, String query, EObjectFragmentable searchSpace, IndividualTextEMF answer) {
		super();
		this.id = id;
		this.query = query;
		this.searchSpace = searchSpace;
		this.answer = answer;
	}

	@Override
	public String getID() {
		return id;
	}
	@Override
	public String getQuestion() {
		return query;
	}
	@Override
	public EObjectFragmentable getSearchSpace() {
		return searchSpace;
	}
	@Override
	public IndividualTextEMF getAnswer() {
		return answer;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		
		sb.append("Type: EMFTextTestCase      ID: "+this.id).append("\r\n");
		sb.append("Question: "+this.query).append("\r\n");
		sb.append("Search Space: "+this.searchSpace).append("\r\n");
		sb.append("Model Fragment Index: "+this.searchSpace.getIndex()).append("\r\n");
		sb.append("Index Answer: "+this.answer).append("\r\n\r\n");
		
		return sb.toString();
	}
	
}
