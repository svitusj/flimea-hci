package usj.svit.architecture.experiment;

import java.util.Arrays;
import java.util.List;

import usj.svit.architecture.approach.IApproachTextEMF;
import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.individual.impl.Query;
import usj.svit.architecture.oracle.BaseOracleEMFText;
import usj.svit.architecture.oracle.IOracle;
import usj.svit.architecture.oracle.TestCaseTextEMF;
import usj.svit.architecture.results.IEMFResult;
import usj.svit.architecture.results.TCPerformance;
import usj.svit.architecture.results.persist.BaseRow;
import usj.svit.architecture.results.persist.PerformanceMatrixBuilder;
import usj.svit.architecture.results.persist.PerformanceRow;
import usj.svit.architecture.results.persist.TextRow;
import usj.svit.helper.perfomance.log.Logger;
import usj.svit.helper.workspace.helper.ProjectHelper;

public class ExperimentEMFText {

	List<IApproachTextEMF> approaches;
	List<BaseOracleEMFText> oracles;
	ProjectHelper helper;
	String baseName;
	Config config;
	StringBuilder summary = new StringBuilder("");
	private StringBuilder sb;

	public ExperimentEMFText(List<BaseOracleEMFText> oracles, List<IApproachTextEMF> approaches, ProjectHelper helper) {
		this.approaches = approaches;
		this.oracles = oracles;
		this.helper = helper;
		sb = new StringBuilder();
		baseName = System.currentTimeMillis() + "";
		Logger.setLogFiles(helper, baseName);
	}
	
	
	public ExperimentEMFText(List<BaseOracleEMFText> oracles, List<IApproachTextEMF> approaches, ProjectHelper helper,
			Config config) {
		this(oracles,approaches,helper);
		this.config = config;
	}

	public void executeExperiment() {
		createSummaryHeader();
		
		for (IOracle<TestCaseTextEMF> oracle : oracles) {
			executeOracle(oracle);
		}
	}

	private void createSummaryHeader() {	
		String header = "";
		if(config.isSet(Config.PERSIST_MODE_BASIC))
			header += new BaseRow().getHeader();
		else if(config.isSet(Config.PERSIST_MODE_TEXTUAL))
			header += new TextRow().getHeader();
		else if(config.isSet(Config.PERSIST_MODE_PERFORMANCE))
			header += new PerformanceRow().getHeader();
		summary.append("Oracle;Approach"+header);	
	}


	protected void executeOracle(IOracle<TestCaseTextEMF> oracle) {

		// Para cada approach
		for (IApproachTextEMF app : approaches) {
			executeApproach(oracle, app);
		}
	}

	protected void executeApproach(IOracle<TestCaseTextEMF> oracle, IApproachTextEMF app) {
		
		// creo un constructor de excels
		PerformanceMatrixBuilder pmb = new PerformanceMatrixBuilder();

		for (TestCaseTextEMF tc : oracle.getTestCases()) {
			executeTestCase(tc, app, pmb);
		}
		persistResults(oracle, app, pmb);

	}

	protected void executeTestCase(TestCaseTextEMF tc, IApproachTextEMF app, PerformanceMatrixBuilder pmb) {

		long t0 = System.currentTimeMillis();
		// ejecuto el testcase
		IEMFResult<IIndividualEMF> result = app.run(tc);
		
		logWords((IIndividualTextEMF) result.getResults().get(0),tc);
		
		
		
		long t1 = System.currentTimeMillis();
		//System.out.println(tc.getID()+" - "+ (t1-t0));
		
		// a�ado los resultados al excel

		if (config.isSet(Config.PERSIST_MODE_BASIC)) {
			int lastGeneration = app.getLastGenerations();
			if (config.isSet(Config.PERSIST_CANDIDATES_ALL)) {
				for (IIndividualEMF candidate : result.getResults()) {
					pmb.compare(tc.getAnswer(), candidate,lastGeneration,tc.getID());
				}
			} else if (config.isSet(Config.PERSIST_CANDIDATES_BEST)) {
				pmb.compare(tc.getAnswer(), result.getResults().get(0),lastGeneration,tc.getID());
			}
		} else if (config.isSet(Config.PERSIST_MODE_TEXTUAL)) {
			Query query = app.getLastQuery();

			if (config.isSet(Config.PERSIST_CANDIDATES_ALL)) {
				for (IIndividualEMF candidate : result.getResults()) {
					pmb.compare(tc.getAnswer(), (IndividualTextEMF) candidate, query,tc.getID());
				}
			} else if (config.isSet(Config.PERSIST_CANDIDATES_BEST)) {
				pmb.compare(tc.getAnswer(), (IndividualTextEMF) result.getResults().get(0), query,tc.getID());
			}
		} else if(config.isSet(Config.PERSIST_MODE_PERFORMANCE)) {
			int lastGeneration = app.getLastGenerations();
			TCPerformance performance = app.getTCPerformance();
			performance.timeElapsed = t1-t0;
			pmb.compare(tc.getAnswer(), result.getResults().get(0),lastGeneration, performance,tc.getID());
			
		}
	}

	private void logWords(IIndividualTextEMF result, TestCaseTextEMF tc) {
		
		sb = new StringBuilder();
		
		sb.append("Query: "+tc.getQuestion());
		
		sb.append("Candidato: "+result.getFitness()+"\r\n");
		
		for(int i=0;i<result.getParentModelSize();i++){
			sb.append("Gen "+i+" - "+result.getGen(i)+" - "+Arrays.toString(result.getParent().getFilteredTextForPosition(i))+"\r\n");
		}
		
		sb.append("Oracle \r\n");
		
		for(int i=0;i<result.getParentModelSize();i++){
			sb.append("Gen "+i+" - "+tc.getAnswer().getGen(i)+" - "+Arrays.toString(tc.getAnswer().getParent().getFilteredTextForPosition(i))+"\r\n");
		}
		
	}


	public void postExperiment() {

		persistDescriptions();
		createAndPersistSummary();
		
		for(IApproachTextEMF app : approaches){
			app.persistWathever(helper,baseName);
		}
		
		System.out.println(sb.toString());
		helper.createFile(baseName+"/words.txt", sb.toString());
		//TimedElement times = Logger.processLogAndReport();

	}

	private void createAndPersistSummary() {

		helper.createFile(baseName + "/summary/summary_"+baseName+".csv", summary.toString());
	}

//	private String getMeanForFile(String oracle, String approach) {
//		return helper.readLastLine(baseName + "/results/" + oracle + "-" + approach + ".csv");
//	}

	private void persistDescriptions() {

		for (BaseOracleEMFText oracle : oracles) {
			helper.createFile(baseName + "/descriptions/O_" + oracle.getID(), oracle.getDescription());
		}

		for (IApproachTextEMF approach : approaches) {
			helper.createFile(baseName + "/descriptions/A_" + approach.getID(), approach.getDescription());
		}
	}

	protected void persistResults(IOracle<TestCaseTextEMF> oracle, IApproachTextEMF app, PerformanceMatrixBuilder pmb) {
		String csv = pmb.getContentForCSV();
		String[] lines = csv.split("\r\n");
		summary.append(oracle.getID() + ";" + app.getID());
		summary.append(lines[lines.length-1]+"\r\n");
		
		helper.createFile(baseName + "/results/" + oracle.getID() + "-" + app.getID() + ".csv", pmb.getContentForCSV());
	}
}
