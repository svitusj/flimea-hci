package usj.svit.architecture.experiment;

import java.util.List;

import usj.svit.architecture.approach.IApproachTextEMF;
import usj.svit.architecture.oracle.BaseOracleEMFText;
import usj.svit.architecture.oracle.IOracle;
import usj.svit.architecture.oracle.TestCaseTextEMF;
import usj.svit.architecture.results.persist.PerformanceMatrixBuilder;
import usj.svit.helper.perfomance.log.Logger;
import usj.svit.helper.workspace.helper.ProjectHelper;

public class ExperimentEMFTextTimed extends ExperimentEMFText {

	public ExperimentEMFTextTimed(List<BaseOracleEMFText> oracles, List<IApproachTextEMF> approaches,
			ProjectHelper helper,Config config) {
		super(oracles, approaches, helper, config);
	}
	
	public ExperimentEMFTextTimed(List<BaseOracleEMFText> oracles, List<IApproachTextEMF> approaches,
			ProjectHelper helper) {
		super(oracles, approaches, helper);
	}
	
	@Override
	public void executeExperiment() {
		if (Logger.ENABLED_EXPERIMENT)
			Logger.log(Logger.WHO_EXPERIMENT, getClass().getCanonicalName(), Logger.ACTION_START,
					Logger.WHAT_EXPERIMENT_EXECUTE);
		super.executeExperiment();
		if (Logger.ENABLED_EXPERIMENT)
			Logger.log(Logger.WHO_EXPERIMENT, getClass().getCanonicalName(), Logger.ACTION_FINISH,
					Logger.WHAT_EXPERIMENT_EXECUTE);
	
	}
	
	@Override
	public void executeOracle(IOracle<TestCaseTextEMF> oracle) {
		
		if (Logger.ENABLED_ORACLE)
			Logger.log(Logger.WHO_ORACLE, getClass().getCanonicalName(), Logger.ACTION_START,
					Logger.WHAT_ORACLE_EXECUTE);
		
		super.executeOracle(oracle);
		
		if (Logger.ENABLED_ORACLE)
			Logger.log(Logger.WHO_ORACLE, getClass().getCanonicalName(), Logger.ACTION_FINISH,
					Logger.WHAT_ORACLE_EXECUTE);
	
	}
	
	@Override
	protected void executeApproach(IOracle<TestCaseTextEMF> oracle, IApproachTextEMF app) {
		
		if (Logger.ENABLED_APPROACH)
			Logger.log(Logger.WHO_APPROACH, getClass().getCanonicalName(), Logger.ACTION_START,
					Logger.WHAT_APPROACH_EXECUTE);
		
		super.executeApproach(oracle, app);
		
		if (Logger.ENABLED_APPROACH)
			Logger.log(Logger.WHO_APPROACH, getClass().getCanonicalName(), Logger.ACTION_FINISH,
					Logger.WHAT_APPROACH_EXECUTE);
	}
	
	@Override
	protected void executeTestCase(TestCaseTextEMF tc, IApproachTextEMF app, PerformanceMatrixBuilder pmb) {
		
		if (Logger.ENABLED_TC)
			Logger.log(Logger.WHO_TC, getClass().getCanonicalName(), Logger.ACTION_START,
					Logger.WHAT_TC_EXECUTE);
		
		super.executeTestCase(tc, app, pmb);
		
		if (Logger.ENABLED_TC)
			Logger.log(Logger.WHO_TC, getClass().getCanonicalName(), Logger.ACTION_FINISH,
					Logger.WHAT_TC_EXECUTE);
	}

	@Override
	protected void persistResults(IOracle<TestCaseTextEMF> oracle, IApproachTextEMF app, PerformanceMatrixBuilder pmb) {
		
		if (Logger.ENABLED_APPROACH)
			Logger.log(Logger.WHO_APPROACH, getClass().getCanonicalName(), Logger.ACTION_START,
					Logger.WHAT_APPROACH_PERSIST);
		
		super.persistResults(oracle, app, pmb);
		
		if (Logger.ENABLED_APPROACH)
			Logger.log(Logger.WHO_APPROACH, getClass().getCanonicalName(), Logger.ACTION_FINISH,
					Logger.WHAT_APPROACH_PERSIST);
	}
}
