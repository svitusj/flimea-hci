package usj.svit.architecture.experiment;

public class Config {
	
	public static final int PERSIST_MODE_BASIC = 1;
	public static final int PERSIST_MODE_TEXTUAL = 2;
	public static final int PERSIST_MODE_PERFORMANCE = 3;
	
	public static final int PERSIST_CANDIDATES_BEST = 4;
	public static final int PERSIST_CANDIDATES_ALL = 5;

	int[] config;
	
	public Config(int... config) {
		this.config = config;
	}
	
	public boolean isSet(int confOption){
		for(int i : config){
			if(i == confOption)
				return true;
		}
		return false;
	}
	
}
