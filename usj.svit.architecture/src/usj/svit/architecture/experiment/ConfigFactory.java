package usj.svit.architecture.experiment;

public class ConfigFactory {

	public static Config getBaseConfig(){
		return new Config(Config.PERSIST_CANDIDATES_BEST,Config.PERSIST_MODE_BASIC);
	}
	
	public static Config getPerformanceConfig(){
		return new Config(Config.PERSIST_CANDIDATES_BEST,Config.PERSIST_MODE_PERFORMANCE);
	}
}
