package usj.svit.architecture.results;

import java.util.ArrayList;
import java.util.List;

import usj.svit.architecture.individual.IIndividualEMF;

public class EMFResultCompletePopulation<T extends IIndividualEMF> implements IEMFResult<T>{

	List<T> individual;
	public EMFResultCompletePopulation(List<? extends T> iPopulation) {
		individual = new ArrayList<T>();
		individual.addAll(iPopulation);
	}
	
	@Override
	public List<T> getResults() {
		return individual;
	}
}
