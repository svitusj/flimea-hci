package usj.svit.architecture.results;

import java.util.List;

import usj.svit.architecture.individual.IIndividualEMF;

public interface IEMFResult<T extends IIndividualEMF> {
	public List<T> getResults();
}
