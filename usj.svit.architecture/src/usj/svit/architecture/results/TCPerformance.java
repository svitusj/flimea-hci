package usj.svit.architecture.results;

public class TCPerformance {

	public long timeElapsed;
	public long populatorCalls;
	public long populatorAffected;
	public long fitnessCalls;
	public long fitnessAffected;
	public long selectionCalls;
	public long selectionAffected;
	public long mutationCalls;
	public long mutationAffected;
	public long crossoverCalls;
	public long crossoverAffected;
	public long replaceCalls;
	public long replaceAffected;
	public long populationCalls;
	public long populationAffected;

	public static String toHeader(){
		return "Time elapsed;"
				+ "Populator Calls;Populator affected;"
				+ "Fitness Calls;Fitness Affected;"
				+ "Selection Calls;Selection Affected;"
				+ "Mutation calls;Mutation Affected;"
				+ "Crossover Calls;Crossover Affected;"
				+ "Replace Calls;Replace Affected;"
				+ "Population Calls;Population Affected";
	}
	
	public String toCSVString(){
		StringBuilder sb = new StringBuilder();
		sb.append(timeElapsed+";");
		sb.append(populationCalls+";"+populationAffected+";");
		sb.append(fitnessCalls+";"+fitnessAffected+";");
		sb.append(selectionCalls+";"+selectionAffected+";");
		sb.append(mutationCalls+";"+mutationAffected+";");
		sb.append(crossoverCalls+";"+crossoverAffected+";");
		sb.append(replaceCalls+";"+replaceAffected+";");
		sb.append(populationCalls+";"+populationAffected);
		return sb.toString();
	}
	
	public void add(TCPerformance other){
		timeElapsed += other.timeElapsed;
		populatorCalls += other.populatorCalls;
		populatorAffected += other.populationAffected;
		fitnessCalls += other.fitnessCalls;
		fitnessAffected += other.fitnessAffected;
		selectionCalls += other.selectionCalls;
		selectionAffected += other.selectionAffected;
		mutationCalls += other.mutationCalls;
		mutationAffected += other.mutationAffected;
		crossoverCalls += other.crossoverCalls;
		crossoverAffected += other.crossoverAffected;
		replaceCalls += other.replaceCalls;
		replaceAffected += other.replaceAffected;
		populationCalls += other.populationCalls;
		populationAffected += other.populationAffected;
	}
	
	public void divide(float amount){
		timeElapsed /= amount;
		populatorCalls /= amount;
		populatorAffected /= amount;
		fitnessCalls /= amount;
		fitnessAffected /= amount;
		selectionCalls /= amount;
		selectionAffected /= amount;
		mutationCalls /= amount;
		mutationAffected /= amount;
		crossoverCalls /= amount;
		crossoverAffected /= amount;
		replaceCalls /= amount;
		replaceAffected /= amount;
		populationCalls /= amount;
		populationAffected /= amount;
	}
}
