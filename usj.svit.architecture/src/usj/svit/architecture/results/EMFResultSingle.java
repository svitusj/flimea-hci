package usj.svit.architecture.results;

import java.util.ArrayList;
import java.util.List;

import usj.svit.architecture.individual.IIndividualEMF;

public class EMFResultSingle<T extends IIndividualEMF> implements IEMFResult<T>{

	List<T> individual;
	public EMFResultSingle(T ind) {
		individual = new ArrayList<T>(1);
		individual.add(ind);
	}
	
	@Override
	public List<T> getResults() {
		// TODO Auto-generated method stub
		return individual;
	}
}
