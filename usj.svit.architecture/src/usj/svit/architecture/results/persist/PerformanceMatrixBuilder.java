package usj.svit.architecture.results.persist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.individual.impl.Query;
import usj.svit.architecture.results.TCPerformance;

public class PerformanceMatrixBuilder {

	List<BaseRow> results;

	public PerformanceMatrixBuilder() {
		results = new ArrayList<BaseRow>();
	}

	/**
	 * Compare two elements and add the result to the list of Rows
	 * performs a basic comparison with MS,FS,TP,FP,FN,TN
	 * @param solution
	 * @param candidate
	 */
	private void basicComparison(BaseRow row, IIndividualEMF solution, IIndividualEMF candidate,String tcID){
		
		//((IIndividualTextEMF)solution).updateTextsAfterRecombinations();
		row.tcID = tcID;
		
		row.modelSize = solution.getParentModelSize();
		row.featureSize = solution.getNumberOfPossitiveGenes();
		for (int i = 0; i < solution.getParentModelSize(); i++) {

			if (solution.getGen(i) && candidate.getGen(i))
				row.TP++;
			else if (!solution.getGen(i) && candidate.getGen(i))
				row.FP++;
			else if (solution.getGen(i) && !candidate.getGen(i))
				row.FN++;
			else if (!solution.getGen(i) && !candidate.getGen(i))
				row.TN++;

		}
		row.calculateDerivateMeasures();
		row.fitness = candidate.getFitness();
	}
	
	
	public void compare(IIndividualEMF solution, IIndividualEMF candidate,int generation,String tcID) {

		BaseRow row = new BaseRow();

		basicComparison(row, solution, candidate,tcID);
		
		row.generation = generation;
		
		results.add(row);
	}

	/**
	 * Compare two elements and add the result to the list of Rows followed by
	 * the query and the fitness value obtained
	 * 
	 * @param solution
	 * @param candidate
	 * @param query
	 */
	public void compare(IIndividualEMF solution, IndividualTextEMF candidate, Query query,String tcID) {

		TextRow row = new TextRow();

		basicComparison(row, solution, candidate,tcID);

		row.originalQuery = query.getOriginalText();
		row.processedQuery = Arrays.toString(query.getText());

		row.originalText = candidate.getOriginalText();
		row.processedText = Arrays.toString(candidate.getText());

		row.fitness = candidate.getFitness();

		results.add(row);
	}
	
	public void compare(IIndividualEMF solution, IIndividualEMF iIndividualEMF,int generation, TCPerformance performance,String tcID){
		
		PerformanceRow row = new PerformanceRow();
		
		basicComparison(row, solution, iIndividualEMF,tcID);
		
		row.generation = generation;
		row.perfomance = performance;
		
		results.add(row);
	}
	
	public List<BaseRow> getResults() {
		return results;
	}

	
	public String getContentForCSV() {
		String content = "";
		
		if(results.size() <= 0)
			return content;
		
		BaseRow mean = results.get(0).cloneForMean();
		content = mean.getHeader();

		for (int i = 0; i < results.size(); i++) {
			content += "F-" + results.get(i).tcID + ";" + results.get(i).toString();
			mean.add(results.get(i));
		}

		mean.divide(results.size());

		content += "MEAN;" + mean.toRoundedString();

		return content;
	}

}
