package usj.svit.architecture.results.persist;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class BaseRow {
	
	public String tcID;

	public int modelSize;
	public int featureSize;
	public double TP;
	public double FN;
	public double FP;
	public double TN;
	public double precision;
	public double recall;
	public double fmeasure;
	public double mcc;
	public double fitness;
	public int generation;
	
	public BaseRow cloneForMean(){
		return new BaseRow();
	}
	
	public String getHeader(){
		return " ;MS;FS;TP;FN;FP;TN;Precision;Recall;F-Measure;MCC;Fitness;Gen\r\n";
		
	}
	
	@Override
	public String toString() {
		
		String s = modelSize + ";" + featureSize + ";" + TP + ";" + FN + ";" + FP + ";" + TN + ";" + precision + ";"
				+ recall + ";" + fmeasure + ";" + mcc + ";"+fitness+";"+generation+"\r\n";
		return s.replace(".", ",");
	}
	
	public String toRoundedString(){
		
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.CEILING);
		
		String s = df.format(modelSize) + ";" + df.format(featureSize) + ";" + df.format(TP) + ";" + df.format(FN) + ";" + df.format(FP) + ";" + df.format(TN) + ";" + df.format(precision) + ";"
				+ df.format(recall) + ";" + df.format(fmeasure) + ";" + df.format(mcc) + ";"+df.format(fitness)+";"+generation+"\r\n";
		
		return s.replace(".", ",");
		
	}

	/**
	 * Calculate
	 */
	public void calculateDerivateMeasures() {

		if (TP == 0.0 && FP == 0.0)
			precision = 0.0;
		else
			precision = TP / (TP + FP);

		if (TP == 0.0 && FN == 0.0)
			recall = 0.0;
		else
			recall = TP / (TP + FN);

		if (precision == 0.0 && recall == 0.0)
			fmeasure = 0.0;
		else
			fmeasure = 2 * ((precision * recall) / (precision + recall));

		if (TP == 0.0 && TN == 0.0 && FN == 0.0 && FP == 0.0)
			mcc = 0.0;
		else
			mcc = ((TP * TN) - (FP * FN)) / Math.sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN));

	}
	
	public void add(BaseRow other){
		this.modelSize += other.modelSize;
		this.featureSize += other.featureSize;
		this.TP += other.TP;
		this.FN += other.FN;
		this.FP += other.FP;
		this.TN += other.TN;
		this.precision += other.precision;
		this.recall += other.recall;
		this.fmeasure += other.fmeasure;
		this.mcc += other.mcc;
		this.fitness += other.fitness;
		this.generation += other.generation;
	}
	
	public void divide(float size){
		this.modelSize /= size;
		this.featureSize /= size;
		this.TP /= size;
		this.FN /= size;
		this.FP /= size;
		this.TN /= size;
		this.precision /= size;
		this.recall /= size;
		this.fmeasure /= size;
		this.mcc /= size;
		this.fitness /= size;
		this.generation /= size;
	}
}