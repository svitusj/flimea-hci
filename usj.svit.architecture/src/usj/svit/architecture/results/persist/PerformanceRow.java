package usj.svit.architecture.results.persist;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import usj.svit.architecture.results.TCPerformance;

public class PerformanceRow extends BaseRow {

	TCPerformance perfomance;
	
	public PerformanceRow() {
		perfomance = new TCPerformance();
	}
	
	public PerformanceRow cloneForMean(){
		return new PerformanceRow();
	}
	
	public String getHeader(){
		return " ;MS;FS;TP;FN;FP;TN;Precision;Recall;F-Measure;MCC;Fitness;Gen;"
				+ TCPerformance.toHeader() 
				+ "\r\n";	
	}
	
	@Override
	public String toString() {
		
		String s = modelSize + ";" + featureSize + ";" + TP + ";" + FN + ";" + FP + ";" + TN + ";" + precision + ";"
				+ recall + ";" + fmeasure + ";" + mcc + ";"+fitness+";"+generation
				+ ";"+ perfomance.toCSVString()+"\r\n";
		return s.replace(".", ",");
	}
	
	public String toRoundedString(){
		
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.CEILING);
		
		String s = df.format(modelSize) + ";" + df.format(featureSize) + ";" + df.format(TP) + ";" + df.format(FN) + ";" + df.format(FP) + ";" + df.format(TN) + ";" + df.format(precision) + ";"
				+ df.format(recall) + ";" + df.format(fmeasure) + ";" + df.format(mcc) + ";"+df.format(fitness)+";"+generation
				+ ";"+ perfomance.toCSVString() +"\r\n";
		
		return s.replace(".", ",");
		
	}
	
	public void add(BaseRow other){
		super.add(other);
		if(other instanceof PerformanceRow)
			perfomance.add(((PerformanceRow)other).perfomance);
	}
	
	public void divide(float size){
		super.divide(size);
		perfomance.divide(size);
	}
}
