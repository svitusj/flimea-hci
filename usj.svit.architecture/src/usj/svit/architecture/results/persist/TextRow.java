package usj.svit.architecture.results.persist;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class TextRow extends BaseRow {

	public String originalQuery;
	public String processedQuery;
	public String originalText;
	public String processedText;

	public TextRow cloneForMean(){
		return new TextRow();
	}
	
	public String getHeader(){
		return " ;MS;FS;TP;FN;FP;TN;Precision;Recall;F-Measure;MCC;Fitness;Original Query;Processed Query;Candidate original text;Candidate processed Text\r\n";	
	}
	
	@Override
	public String toString() {
		String s = modelSize + ";" + featureSize + ";" + TP + ";" + FN + ";" + FP + ";" + TN + ";" + precision + ";"
				+ recall + ";" + fmeasure + ";" + mcc + ";" + fitness + ";" + originalQuery + ";" + processedQuery
				+ ";" + originalText + ";" + processedText + "\r\n";
		return s.replace(".", ",");
	}

	public String toRoundedString() {
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.CEILING);

		String s = df.format(modelSize) + ";" + df.format(featureSize) + ";" + df.format(TP) + ";" + df.format(FN)
				+ ";" + df.format(FP) + ";" + df.format(TN) + ";" + df.format(precision) + ";" + df.format(recall)
				+ ";" + df.format(fmeasure) + ";" + df.format(mcc) + ";" + fitness + ";" + originalQuery + ";"
				+ processedQuery + ";" + originalText + ";" + processedText + "\r\n";

		return s.replace(".", ",");
	}
}