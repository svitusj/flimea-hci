package usj.svit.helper.workspace.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.osgi.framework.Bundle;

public class PluginHelper {

	private Bundle bundle;
	
	
	public PluginHelper(String plugin_name) {
		bundle = Platform.getBundle(plugin_name);
	}
	
	public File findFileForResourcePath(String name)
	{
		URL url = FileLocator.find(bundle, new Path(name), null);
		File file = null;
		try {
			url = FileLocator.toFileURL(url);
			file = URIUtil.toFile(URIUtil.toURI(url));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file;
	}
	
	/**
	 * Load a Model which is stored in a plugin.
	 * @param bundle Plugin which contains the searched resource.
	 * @param path_model Directory where is stored the model(e.g. "models/model.tcs")
	 * @return The Model 
	 */
	public EObject loadModel(String path_model) 
	{
		File file = findFileForResourcePath(path_model);
		if (file.exists())
		{
			URI uri = URI.createFileURI(file.getAbsolutePath());
			return IOHelper.loadModel(uri);
		}
		return null;

	}
	
	public String loadFile(String path){
		File f = findFileForResourcePath(path);
			return IOHelper.readFile(f);
	}
	
	public void createFile(String name, String content) {
		
		BufferedWriter output = null;
		try {
			String url = FileLocator.getBundleFile(bundle).getAbsolutePath();
			
			File file = new File(url+"/"+name);
	    	if (!file.exists()) {		    		
	    		file.createNewFile();
	    		output = new BufferedWriter(new FileWriter(file));
	            output.write(content);
	    	}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
	        try {
	        	if(output!=null)
	        	{
	        		output.close();
	        	}
	        } catch (IOException e) {
	            //ignore
	        }
	    }
	}

	public void copyResourcesFromPluginToProject(String resources_directory, ProjectHelper project)
	{
		File file = findFileForResourcePath(resources_directory);
		if (file.exists())
		{
			copyFileToProject("",file, project);
		}
	}
		
	private void copyFileToProject(String path, File file, ProjectHelper project)
	{
		if (file.isDirectory())
		{
			for (File file_child: file.listFiles())
			{
				copyFileToProject(path+"/"+file.getName(),file_child, project);
			}
		}
		else if (file.isFile())
		{			
			String content;
			try {
				content = new String(Files.readAllBytes(file.toPath()));
				project.createFile(path+"/"+file.getName(),content);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
	}
}
