package usj.svit.helper.workspace.helper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ProjectHelper {

	private IProject project;

	public ProjectHelper(String name) {
		checkProject(name);
	}

	private void checkProject(String name) {

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		project = root.getProject(name);
		try {
			if (!project.exists()) {
				// project.delete(true, new NullProgressMonitor());
				project.create(null);
			}
			project.open(null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void refresh(){
		try {
			project.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
		} catch (CoreException e) {
			
			e.printStackTrace();
		}
	}

	public File getFile(String name){
		IFile file = project.getFile(name);
		// gets URI for EFS.
		java.net.URI uri = file.getLocationURI();

		// what if file is a link, resolve it.
		if(file.isLinked()){
		   uri = file.getRawLocationURI();
		}

		// Gets native File using EFS
		File javaFile = new File(uri);
		refresh();
		return javaFile;
	}
	
	public String readLastLine(String path){
		
		File f = getFile(path);
		String lastLine = null;
		BufferedReader br = null;
		
		try {
			String currentLine = "";
			br = new BufferedReader(new FileReader(f));

			while ((currentLine = br.readLine()) != null) {
				lastLine = currentLine;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 
		return lastLine;
		
	}
	
	public void createFile(String name, String content) {
		
		InputStream input = new ByteArrayInputStream(content.getBytes());;
	    IFile file = project.getFile(name);
	    try {
	    	if (!file.exists()) {
	    		checkPath(file.getParent());
	    		
	    		file.create(input, true, null);
	    	} else {
	    		file.setContents(input, IResource.FORCE, null);
	    	}
	    } catch (CoreException e) {
			e.printStackTrace();
		} finally {
	        try {
	            input.close();
	        } catch (IOException e) {
	            //ignore
	        }
	    }
	}
	
	private void checkPath(IContainer parent) {
		if (!parent.exists())
		{
			checkPath(parent.getParent());
			IFolder newFolder = project.getFolder(parent.getProjectRelativePath());
            try {
                  if (!newFolder.exists()) {
                      newFolder.create(true, true, null);
                  }
              } catch (CoreException e) {
                  e.printStackTrace();
              }
		}
	}

	public EObject loadModel(String name) {
		return loadModel(project.getFile(name));

	}

	public EObject loadModel(IFile f) {

		if (f.exists()) {

			String filename = f.getFullPath().toString();
			URI uri = URI.createFileURI(filename);
			return IOHelper.loadModel(uri);
		}

		return null;

	}

	public void createFile(String name, EObject content) {
		IFile f = project.getFile(name);
		if (!f.exists()) {
			ResourceSet resSet = new ResourceSetImpl();
			Resource resource = resSet.createResource(URI.createPlatformResourceURI(f.getFullPath().toString(), true));
			resource.getContents().add(content);
			try {
				resource.save(Collections.EMPTY_MAP);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<EObject> loadModels() {

		try {
			ArrayList<EObject> objects = new ArrayList<EObject>();

			IFolder modelsfolder = project.getFolder("models/");

			for (IResource modelFolder : modelsfolder.members()) {

				if (modelFolder instanceof IFolder) {
					IFolder folder = (IFolder) modelFolder;

					for (IResource res : folder.members()) {

						if (res instanceof IFile) {
							IFile file = (IFile) res;

							if (file.getFileExtension().equals("ih62")) {
								objects.add(loadModel(file));
							}
						}

					}

				}

			}
			return objects;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

//	public Genes readOracle(String name) {
//
//		BufferedReader reader;
//		Genes result=null;
//		try {
//
//			IFile f = project.getFile(name);
//			File file = f.getRawLocation().makeAbsolute().toFile();
//
//			if (f.exists()) {
//
//				project.refreshLocal(IFile.DEPTH_INFINITE, null);
//				reader = new BufferedReader(new FileReader(file));
//				int size = Integer.parseInt(reader.readLine());
//				result = new Genes(size);
//				String nextLine;
//				int i=0;
//				while((nextLine = reader.readLine()) != null) {
//					
//					if(Integer.parseInt(nextLine)==1)
//						result.set(i);
//					i++;
//				}
//				reader.close();
//				
//			}
//			
//		} catch (IOException | CoreException e) {
//
//			e.printStackTrace();
//		}
//		return result;
//	}
//
//	public void writeOracle(String name, Genes oracle) {
//
//		try {
//			IFile f = project.getFile(name);
//			project.refreshLocal(IFile.DEPTH_INFINITE, null);
//			if (!f.exists()) {
//
//				
//				f.create(new ByteArrayInputStream("".getBytes()), true, null);
//				File file = f.getRawLocation().makeAbsolute().toFile();
//
//				FileWriter writer = new FileWriter(file);
//				writer.write(oracle.size()+"\r\n");
//				for(int i=0;i<oracle.size();i++) {
//					if(oracle.get(i))
//						writer.write("1\r\n");
//					else
//						writer.write("0\r\n");
//				}
//				writer.flush();
//				writer.close();
//			}
//		} catch (CoreException | IOException e) {
//			e.printStackTrace();
//		}
//
//	}

	public String readFile(String name) {
		IFile f = project.getFile(name);
		try {
			project.refreshLocal(IFile.DEPTH_INFINITE, null);
		} catch (CoreException e1) {
			e1.printStackTrace();
		}
		BufferedReader bufferedReader = null;
		if (f.exists()) {

			try {
				bufferedReader = new BufferedReader(new InputStreamReader(f.getContents()),
						4 * (int) Math.pow(1024, 2));

				// File file = f.getRawLocation().makeAbsolute().toFile();
				String inputLine;
				String text = "";

				while ((inputLine = bufferedReader.readLine()) != null) {
					//if(Logger.enabled) Logger.log(getClass().getCanonicalName(), inputLine);
					text += inputLine;
				}

				return text;

			} catch (CoreException | IOException e) {
				e.printStackTrace();
			}

		}
		return "";
	}
	
	public void createJSON(String name, Object jsonObject) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
	    String json = gson.toJson(jsonObject);
		InputStream input = new ByteArrayInputStream(json.getBytes());
	    IFile file = project.getFile(name);
	    
	    try {
	    	if (!file.exists()) {
	    		checkPath(file.getParent());
	    		
	    		file.create(input, true, null);
	    	} else {
	    		file.setContents(input, IResource.FORCE, null);
	    	}
	    } catch (CoreException e) {
			e.printStackTrace();
		} finally {
	        try {
	            input.close();
	        } catch (IOException e) {
	            //ignore
	        }
	    }
	}
	
	public ArrayList<String> loadJSONs(String path) {
		BufferedReader bufferedReader = null;
		ArrayList<String> lstConfigs = new ArrayList<String>();
		
		try {
			IFolder configsFolder = project.getFolder(path);
			
			checkPath(configsFolder);

			for (IResource config : configsFolder.members()) {
				if (config instanceof IFile) {
					IFile json = (IFile) config;

					if (json.getFileExtension().equals("json")) {
						bufferedReader = new BufferedReader(new InputStreamReader(json.getContents()));

						String inputLine;
						String content = "";

						while ((inputLine = bufferedReader.readLine()) != null) {
							content += inputLine;
						}
						
						lstConfigs.add(content);
					}
				}
			}

			return lstConfigs;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void deleteJSON(String name) {
		IFile file = project.getFile(name + ".json");
	    
	    try {
	    	if (file.exists()) {
	    		file.delete(true, null);
	    	}
	    } catch (CoreException e) {
			e.printStackTrace();
		}
	}
}
