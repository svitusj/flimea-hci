package usj.svit.helper.workspace.helper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class IOHelper {

	public static HashMap<URI,EObject> cacheEObjects = new HashMap<URI,EObject>();
	
	public static EObject loadModel(URI uri){
		if(cacheEObjects.containsKey(uri)) {
			return cacheEObjects.get(uri);
		}
		
		ResourceSet resSet = new ResourceSetImpl();
		Resource resource = resSet.getResource(uri, true);
		EObject myModel = (EObject) resource.getContents().get(0);
		cacheEObjects.put(uri, myModel);
		return myModel;
	}

	public static String readFile(File f) {
		try {
			return new String(Files.readAllBytes(f.toPath()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
